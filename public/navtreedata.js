var NAVTREE =
[
  [ "Packuru", "index.html", [
    [ "Introduction", "index.html", [
      [ "Architecture", "index.html#architecture", null ],
      [ "Controller System", "index.html#controllersystem", [
        [ "Advantages", "index.html#cs_advantages", null ]
      ] ],
      [ "Frontend API", "index.html#api", null ]
    ] ],
    [ "Modules", "modules.html", [
      [ "Controller System Qt Quick Controls", "group__ControllerSystemQQControls.html", [
        [ "Controller System QML", "group__ControllerSystemQML.html", [
          [ "GeneratedPage", "classGeneratedPage.html", [
            [ "rowMapper", "classGeneratedPage.html#a8c62445461f31386b574d241547b2358", null ]
          ] ],
          [ "PageStack", "classPageStack.html", [
            [ "setCurrentPage", "classPageStack.html#ab9eac3ff7a171f180fd073e5f303dfc3", null ]
          ] ]
        ] ],
        [ "qqc", "namespaceqcs_1_1qqc.html", null ]
      ] ],
      [ "Controller System Qt Widgets", "group__ControllerSystemQtWidgets.html", [
        [ "qtw", "namespaceqcs_1_1qtw.html", null ]
      ] ],
      [ "Packuru Core", "group__PackuruCore.html", [
        [ "Core", "namespacePackuru_1_1Core.html", null ]
      ] ],
      [ "Packuru Core Browser", "group__PackuruCoreBrowser.html", [
        [ "Browser", "namespacePackuru_1_1Core_1_1Browser.html", null ]
      ] ],
      [ "Packuru Core Queue", "group__PackuruCoreQueue.html", [
        [ "Queue", "namespacePackuru_1_1Core_1_1Queue.html", null ]
      ] ],
      [ "Packuru Utils", "group__PackuruUtils.html", [
        [ "Utils", "namespacePackuru_1_1Utils.html", null ]
      ] ]
    ] ],
    [ "Namespaces", "namespaces.html", [
      [ "Namespace List", "namespaces.html", [
        [ "Packuru", null, [
          [ "Core", "namespacePackuru_1_1Core.html", [
            [ "Browser", "namespacePackuru_1_1Core_1_1Browser.html", null ],
            [ "Queue", "namespacePackuru_1_1Core_1_1Queue.html", null ]
          ] ],
          [ "Utils", "namespacePackuru_1_1Utils.html", null ]
        ] ],
        [ "qcs", null, [
          [ "qqc", "namespaceqcs_1_1qqc.html", null ],
          [ "qtw", "namespaceqcs_1_1qtw.html", null ]
        ] ]
      ] ],
      [ "Namespace Members", "namespacemembers.html", [
        [ "All", "namespacemembers.html", null ],
        [ "Functions", "namespacemembers_func.html", null ],
        [ "Variables", "namespacemembers_vars.html", null ]
      ] ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", [
        [ "Packuru", null, [
          [ "Core", "namespacePackuru_1_1Core.html", [
            [ "Browser", "namespacePackuru_1_1Core_1_1Browser.html", [
              [ "ArchiveBrowserCore", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html", [
                [ "BrowserTaskState", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#a276d67ed03566f1cec934a98a78d23dd", [
                  [ "ReadyToRun", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#a276d67ed03566f1cec934a98a78d23dda506db5c85cf0fd10f93e5478013650b6", null ],
                  [ "StartingUp", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#a276d67ed03566f1cec934a98a78d23dda2454c0e7d149123452f860fcb976b65c", null ],
                  [ "InProgress", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#a276d67ed03566f1cec934a98a78d23dda12d868c18cb29bf58f02b504be9033fd", null ],
                  [ "WaitingForInput", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#a276d67ed03566f1cec934a98a78d23dda76f1e38106648a32dbed02d8898fe21c", null ],
                  [ "WaitingForPassword", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#a276d67ed03566f1cec934a98a78d23dda5fab9daa3c378d3f65ff689a5b950e76", null ],
                  [ "Paused", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#a276d67ed03566f1cec934a98a78d23ddae99180abf47a8b3a856e0bcb2656990a", null ],
                  [ "Aborted", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#a276d67ed03566f1cec934a98a78d23dda721c28f4c74928cc9e0bb3fef345e408", null ],
                  [ "Success", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#a276d67ed03566f1cec934a98a78d23dda505a83f220c02df2f85c3810cd9ceb38", null ],
                  [ "Warnings", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#a276d67ed03566f1cec934a98a78d23ddae17fdb615452f440c793b5ddad90dd5e", null ],
                  [ "Errors", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#a276d67ed03566f1cec934a98a78d23dda5ef0c737746fae2ca90e66c39333f8f6", null ],
                  [ "___INVALID", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#a276d67ed03566f1cec934a98a78d23dda6e545caba6d4293f6c360bc2e183bd64", null ]
                ] ],
                [ "UIState", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#a46982b6b3247b002a74dde6ca26b2344", [
                  [ "Navigation", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#a46982b6b3247b002a74dde6ca26b2344a846495f9ceed11accf8879f555936a7d", null ],
                  [ "StatusPage", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#a46982b6b3247b002a74dde6ca26b2344a903fb4f260e8fa3179d3a278430d0836", null ]
                ] ],
                [ "UserString", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#aae24373b6c0bbb21fcd8a1cf051e29af", [
                  [ "___INVALID", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa6e545caba6d4293f6c360bc2e183bd64", null ],
                  [ "ActionAdd", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#aae24373b6c0bbb21fcd8a1cf051e29afaded30500200846a9cdb7d18c51671196", null ],
                  [ "ActionDelete", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa36b4a85d379d8e72426e1a59a167b6af", null ],
                  [ "ActionExtract", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa7f2a064ae66cff8649053b333b33de7b", null ],
                  [ "ActionFilter", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa710ef25b897ef78bedcd99442ed4518a", null ],
                  [ "ActionPreview", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#aae24373b6c0bbb21fcd8a1cf051e29afaaba6df4a348123cf8adcf7694da06113", null ],
                  [ "ActionProperties", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa8fcdc6f191114223f0690bae910dc4a8", null ],
                  [ "ActionReload", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#aae24373b6c0bbb21fcd8a1cf051e29afaf2730324df06d72982c8528374ef4cf4", null ],
                  [ "ActionTest", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa107f1c4fc93e51f8d693fa4c9eabac77", null ]
                ] ],
                [ "ArchiveBrowserCore", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#ab0855321499dde496f660f0866fce24a", null ],
                [ "~ArchiveBrowserCore", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#a798951178a20923c69f47b60e0758a62", null ],
                [ "addAvailableChanged", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#abbe8f54545f276d88181b17183e6fd7b", null ],
                [ "archivePropertiesAvailable", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#a39d3dc43a324ab2837b0ad7a4e2dcf88", null ],
                [ "availableActionsChanged", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#a6b38fa62503821e2fcdee137d113b73e", null ],
                [ "browserReset", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#af457caa9b318d69811d47414df3a6c0b", null ],
                [ "createArchivingDialogCore", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#ab3a1074f83e0e613e8c8fa784bc0af34", null ],
                [ "createDeletionDialogCore", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#a96c10c2e3e745113bd900d51b4e5eda2", null ],
                [ "createExtractionDialogCore", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#a2d3d85a4f68cf01fc7973fc042b4e2e8", null ],
                [ "createTestDialogCore", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#ac762aa21c5f6755b5d6e2bc9e9ec0c50", null ],
                [ "deleteAvailableChanged", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#a0673efce544fcd65c98461e9e23493d5", null ],
                [ "destroyLater", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#aa5f97b4f6309ac39ce270d0ac0f92d50", null ],
                [ "extractAvailableChanged", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#a233f721e58fedee0040a3e3b27fdf3ab", null ],
                [ "filesReadyForPreview", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#a5436e66ddf284c411a4420d17831d4ed", null ],
                [ "filterAvailableChanged", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#aaa8b43335673fb4ee1464828a1b840fc", null ],
                [ "getArchiveComment", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#a64e6a165c5412dd8e8759a35d4eb0470", null ],
                [ "getArchiveName", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#ac021d52a3bc4b9c507ffad1345b91526", null ],
                [ "getNavigator", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#aa788ab29c8f807dcfc7ec870502e6b21", null ],
                [ "getPropertiesModel", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#a668ed7bf905a84ee2f48ae3c666278c4", null ],
                [ "getStatusPageCore", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#a9ea4f2793a9f97a608488a8ed74c152f", null ],
                [ "getTaskState", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#a2d15aad1047131f45c1c8239f2fde121", null ],
                [ "getTitle", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#a9c99cda566ec653b87bfc34b0d76d999", null ],
                [ "getUIState", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#a7947d79cf8e8f7dea95a76089d1b6884", null ],
                [ "isAddAvailable", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#af874935e44c7482f7aacd6db322a48fc", null ],
                [ "isDeleteAvailable", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#ad84fb9bb10574895982ee0bc02617947", null ],
                [ "isExtractAvailable", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#a5712c1b578d156352419b21d4f701d7e", null ],
                [ "isFilterAvailable", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#a56a255cd59a905dd29627d0772cdf01c", null ],
                [ "isPreviewAvailable", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#ac9c78f19a63440fb87f758f3eada7f20", null ],
                [ "isReloadAvailable", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#a14d7f859896bc364fd29da0782e354dd", null ],
                [ "isTaskBusy", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#aa7367792d8ac5d62812c4ebf7659cdde", null ],
                [ "isTaskFinished", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#a98309a2ecca96501e5687223c9b35664", null ],
                [ "needsActivation", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#a64811fcfee1f0127433bf06df1cf22d8", null ],
                [ "openExtractionDestinationFolder", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#aafe344417df8b86062073f2bba7d32d5", null ],
                [ "previewAvailableChanged", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#a7ce563866f9b015b9a22d9760c5fe5c4", null ],
                [ "previewSelectedFiles", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#a10126b1c3ffe3458761a0623fe9008c9", null ],
                [ "propertiesAvailableChanged", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#ac64148204ccf75c55d26870387dbe566", null ],
                [ "reloadArchive", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#a2a5622ae4776ea7b63f134af94bc8f52", null ],
                [ "reloadAvailableChanged", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#ae1334f5b218dded4af222585d97a1716", null ],
                [ "taskBusyChanged", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#a47a03038ef90eebecbd80e4aa3470cc0", null ],
                [ "taskStateChanged", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#a1e898a1b9e36e125dba17103fbb3ed4b", null ],
                [ "titleChanged", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#ab32e5e797b9f3e81016d6b703d7a4dd5", null ],
                [ "uiStateChanged", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#a3b847815abda8981901aa0e241350c66", null ],
                [ "addAvailable", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#ae19cc643d473717db458b8242ea9bd29", null ],
                [ "archiveName", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#a896346e04ece5b9dde4808844c334979", null ],
                [ "deleteAvailable", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#a9a85f454a6f9f07ce9de30cb482412af", null ],
                [ "extractAvailable", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#ac2a6bb3a1325c27b063cf106600820e0", null ],
                [ "filterAvailable", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#a6b70e0b50554fbd0ca0c6d017c1a9948", null ],
                [ "navigator", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#a7da562c93d0a03e3bf67f3c0a8566c2f", null ],
                [ "previewAvailable", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#a3ca912ab848e18f7ecf0efa747eab7bd", null ],
                [ "propertiesAvailable", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#a0e13a66bb8d668e611bb912498e2efef", null ],
                [ "reloadAvailable", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#ae46b9b270db5c73bd9dc9dc32331d4ac", null ],
                [ "statusPageCore", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#a4c1963a115247ed627d75904e713c127", null ],
                [ "taskBusy", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#a03f485fcaa8e97262e2a84ac94bb4176", null ],
                [ "taskState", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#a12fd4dedcbae89ca6bddda34d9a1a7e5", null ],
                [ "title", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#aa93d37c468bf6aea9c9823eb74bd1a48", null ],
                [ "uiState", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#a316e948d552200cb583868f3717fc940", null ]
              ] ],
              [ "ArchiveNavigator", "classPackuru_1_1Core_1_1Browser_1_1ArchiveNavigator.html", [
                [ "PreviousPathIndexSelection", "classPackuru_1_1Core_1_1Browser_1_1ArchiveNavigator.html#a2bd4c31e8c3edcef188925e74455bcf0", [
                  [ "___INVALID", "classPackuru_1_1Core_1_1Browser_1_1ArchiveNavigator.html#a2bd4c31e8c3edcef188925e74455bcf0a6e545caba6d4293f6c360bc2e183bd64", null ],
                  [ "NoSelection", "classPackuru_1_1Core_1_1Browser_1_1ArchiveNavigator.html#a2bd4c31e8c3edcef188925e74455bcf0a5230c6f2cc0017a06cb7f3afd8f535cf", null ],
                  [ "SelectFirstIndex", "classPackuru_1_1Core_1_1Browser_1_1ArchiveNavigator.html#a2bd4c31e8c3edcef188925e74455bcf0a2d88e0eae49b226677a77f4844169414", null ],
                  [ "SelectRow", "classPackuru_1_1Core_1_1Browser_1_1ArchiveNavigator.html#a2bd4c31e8c3edcef188925e74455bcf0a5357abf3f205f710f010aa2be41cb224", null ]
                ] ],
                [ "UserString", "classPackuru_1_1Core_1_1Browser_1_1ArchiveNavigator.html#aae24373b6c0bbb21fcd8a1cf051e29af", [
                  [ "___INVALID", "classPackuru_1_1Core_1_1Browser_1_1ArchiveNavigator.html#aae24373b6c0bbb21fcd8a1cf051e29afa6e545caba6d4293f6c360bc2e183bd64", null ],
                  [ "Close", "classPackuru_1_1Core_1_1Browser_1_1ArchiveNavigator.html#aae24373b6c0bbb21fcd8a1cf051e29afad3d2e617335f08df83599665eef8a418", null ]
                ] ],
                [ "ArchiveNavigator", "classPackuru_1_1Core_1_1Browser_1_1ArchiveNavigator.html#ad875f733274a0fcd3c625dba926c9030", null ],
                [ "~ArchiveNavigator", "classPackuru_1_1Core_1_1Browser_1_1ArchiveNavigator.html#aeef7f6b60e13c4c18c5247005c289c77", null ],
                [ "canGoUpChanged", "classPackuru_1_1Core_1_1Browser_1_1ArchiveNavigator.html#ae0a26930608c6a7010f7a91550e95b1b", null ],
                [ "changeDir", "classPackuru_1_1Core_1_1Browser_1_1ArchiveNavigator.html#aef103219effad1780c7d8b762ae9a8eb", null ],
                [ "currentPathChanged", "classPackuru_1_1Core_1_1Browser_1_1ArchiveNavigator.html#ab7b83febf474b0a65ef1e7b3bf652c48", null ],
                [ "currentPathIndexChanged", "classPackuru_1_1Core_1_1Browser_1_1ArchiveNavigator.html#aca4d7ad884680505f1f06876515b25ec", null ],
                [ "getCanGoUp", "classPackuru_1_1Core_1_1Browser_1_1ArchiveNavigator.html#ac78e2bf71afb9ec69c8861acb5747164", null ],
                [ "getCurrentPath", "classPackuru_1_1Core_1_1Browser_1_1ArchiveNavigator.html#a06afa8f380f7576e971c9d66469d5ad7", null ],
                [ "getCurrentPathIndex", "classPackuru_1_1Core_1_1Browser_1_1ArchiveNavigator.html#a603a61670514c435aa7ea9dfcae294fd", null ],
                [ "getFilterControllerEngine", "classPackuru_1_1Core_1_1Browser_1_1ArchiveNavigator.html#a9e6aee41e12d73a8ca6b86f68ec9969a", null ],
                [ "getModel", "classPackuru_1_1Core_1_1Browser_1_1ArchiveNavigator.html#aebd364d75e9eac692769dd30679abb9e", null ],
                [ "getModelDefaultSections", "classPackuru_1_1Core_1_1Browser_1_1ArchiveNavigator.html#a2174bdc6e3c4008ed25aefc0307c1575", null ],
                [ "getPreviousPathIndexSelection", "classPackuru_1_1Core_1_1Browser_1_1ArchiveNavigator.html#a76431072975ab981b94dd84d53521e6f", null ],
                [ "getSelectionModel", "classPackuru_1_1Core_1_1Browser_1_1ArchiveNavigator.html#a2f00a733f2ef3d5cd8a2aeba08daaf1e", null ],
                [ "getString", "classPackuru_1_1Core_1_1Browser_1_1ArchiveNavigator.html#a572e58bd9d208f4fd37e800a18d11de0", null ],
                [ "goUp", "classPackuru_1_1Core_1_1Browser_1_1ArchiveNavigator.html#a5a17d4b22bebd99951af4c2422022844", null ],
                [ "openItem", "classPackuru_1_1Core_1_1Browser_1_1ArchiveNavigator.html#ab81a9ad4707dfc791a2f39befb9cc81d", null ],
                [ "previousPathIndexSelectionChanged", "classPackuru_1_1Core_1_1Browser_1_1ArchiveNavigator.html#ad93fed6f71e48515b25665dd6eb8fc9d", null ],
                [ "setPreviousPathIndexSelection", "classPackuru_1_1Core_1_1Browser_1_1ArchiveNavigator.html#aec1663f05a38958212435ad61dd3e7dc", null ],
                [ "sortModel", "classPackuru_1_1Core_1_1Browser_1_1ArchiveNavigator.html#a961fe28fc060ed5cdc1e20524714e294", null ],
                [ "canGoUp", "classPackuru_1_1Core_1_1Browser_1_1ArchiveNavigator.html#a988b3f61f0cc56085cf07e0f6c477f4c", null ],
                [ "currentPath", "classPackuru_1_1Core_1_1Browser_1_1ArchiveNavigator.html#ab6189b2f022d5a2ecfcde6857c5b9bf8", null ],
                [ "currentPathIndex", "classPackuru_1_1Core_1_1Browser_1_1ArchiveNavigator.html#aae9263b173ce7331b7218e844db5b1bc", null ],
                [ "model", "classPackuru_1_1Core_1_1Browser_1_1ArchiveNavigator.html#ab565f00c2e1a51ba5511d0225bd1bab8", null ],
                [ "previousPathIndexSelection", "classPackuru_1_1Core_1_1Browser_1_1ArchiveNavigator.html#a2874aa8d4ea5c846429d66dfd19e7865", null ],
                [ "selectionModel", "classPackuru_1_1Core_1_1Browser_1_1ArchiveNavigator.html#add429d891c9324f479e3f8613048810a", null ]
              ] ],
              [ "BasicControl", "classPackuru_1_1Core_1_1Browser_1_1BasicControl.html", [
                [ "DefaultButton", "classPackuru_1_1Core_1_1Browser_1_1BasicControl.html#a24a75c2caa7249dc6132ca82060f3ee8", [
                  [ "___INVALID", "classPackuru_1_1Core_1_1Browser_1_1BasicControl.html#a24a75c2caa7249dc6132ca82060f3ee8a6e545caba6d4293f6c360bc2e183bd64", null ],
                  [ "Abort", "classPackuru_1_1Core_1_1Browser_1_1BasicControl.html#a24a75c2caa7249dc6132ca82060f3ee8a727b63583e01fa2b3952dab580c84dc2", null ],
                  [ "BrowseArchive", "classPackuru_1_1Core_1_1Browser_1_1BasicControl.html#a24a75c2caa7249dc6132ca82060f3ee8aab49bb4d9d59db86b051a5428504492c", null ],
                  [ "Retry", "classPackuru_1_1Core_1_1Browser_1_1BasicControl.html#a24a75c2caa7249dc6132ca82060f3ee8a6327b4e59f58137083214a1fec358855", null ],
                  [ "ViewLog", "classPackuru_1_1Core_1_1Browser_1_1BasicControl.html#a24a75c2caa7249dc6132ca82060f3ee8a28dae093f6dd5798e896c2fbfe16836e", null ]
                ] ],
                [ "BasicControl", "classPackuru_1_1Core_1_1Browser_1_1BasicControl.html#a73a6429d8e8683abcaf307edce68a409", null ],
                [ "~BasicControl", "classPackuru_1_1Core_1_1Browser_1_1BasicControl.html#acad4fbc56cb600ecbf4e7564f7d0a0c2", null ],
                [ "abort", "classPackuru_1_1Core_1_1Browser_1_1BasicControl.html#a220fbc82bcea18cde1d84500470b38d1", null ],
                [ "browse", "classPackuru_1_1Core_1_1Browser_1_1BasicControl.html#ad02d74b0df11febdc696172cf6a22b83", null ],
                [ "defaultButtonChanged", "classPackuru_1_1Core_1_1Browser_1_1BasicControl.html#a052cb602fff0f71b514a0f8eb9285d4a", null ],
                [ "getControllerEngine", "classPackuru_1_1Core_1_1Browser_1_1BasicControl.html#a72d2d5dda34983ff5afcb6039a09bdbb", null ],
                [ "getDefaultButton", "classPackuru_1_1Core_1_1Browser_1_1BasicControl.html#aa33cbcbcb61969da84acde9c785a2cb2", null ],
                [ "getLog", "classPackuru_1_1Core_1_1Browser_1_1BasicControl.html#a9a878fed611a5ca9e06a6e5974d11387", null ],
                [ "retry", "classPackuru_1_1Core_1_1Browser_1_1BasicControl.html#a5619747c1e923b27c794f221811ef88a", null ],
                [ "defaultButton", "classPackuru_1_1Core_1_1Browser_1_1BasicControl.html#aaba7d939f104fde296c49db2f3be9c91", null ]
              ] ],
              [ "BasicControlControllerType", "classPackuru_1_1Core_1_1Browser_1_1BasicControlControllerType.html", [
                [ "Type", "classPackuru_1_1Core_1_1Browser_1_1BasicControlControllerType.html#a1d1cfd8ffb84e947f82999c682b666a7", [
                  [ "AbortButton", "classPackuru_1_1Core_1_1Browser_1_1BasicControlControllerType.html#a1d1cfd8ffb84e947f82999c682b666a7a41096bc836b5e9230aa0ccc09ab5b6ea", null ],
                  [ "BrowserButton", "classPackuru_1_1Core_1_1Browser_1_1BasicControlControllerType.html#a1d1cfd8ffb84e947f82999c682b666a7ae0a571867df971422ade5d99985da31d", null ],
                  [ "ErrorsWarningsLabel", "classPackuru_1_1Core_1_1Browser_1_1BasicControlControllerType.html#a1d1cfd8ffb84e947f82999c682b666a7a58e9643867b2fae9213efe075c304794", null ],
                  [ "RetryButton", "classPackuru_1_1Core_1_1Browser_1_1BasicControlControllerType.html#a1d1cfd8ffb84e947f82999c682b666a7a8dec4d6f30ae94ff4f300af9013b2351", null ],
                  [ "ViewLogButton", "classPackuru_1_1Core_1_1Browser_1_1BasicControlControllerType.html#a1d1cfd8ffb84e947f82999c682b666a7ab013339acd6f8385dc10652aafe4699d", null ]
                ] ]
              ] ],
              [ "DeletionDialogControllerType", "classPackuru_1_1Core_1_1Browser_1_1DeletionDialogControllerType.html", [
                [ "Type", "classPackuru_1_1Core_1_1Browser_1_1DeletionDialogControllerType.html#a1d1cfd8ffb84e947f82999c682b666a7", [
                  [ "Password", "classPackuru_1_1Core_1_1Browser_1_1DeletionDialogControllerType.html#a1d1cfd8ffb84e947f82999c682b666a7adc647eb65e6711e155375218212b3964", null ],
                  [ "RunInQueue", "classPackuru_1_1Core_1_1Browser_1_1DeletionDialogControllerType.html#a1d1cfd8ffb84e947f82999c682b666a7a3e2ffba79e6d032b1c075e396fc70e1b", null ]
                ] ]
              ] ],
              [ "DeletionDialogCore", "classPackuru_1_1Core_1_1Browser_1_1DeletionDialogCore.html", [
                [ "UserString", "classPackuru_1_1Core_1_1Browser_1_1DeletionDialogCore.html#aae24373b6c0bbb21fcd8a1cf051e29af", [
                  [ "___INVALID", "classPackuru_1_1Core_1_1Browser_1_1DeletionDialogCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa6e545caba6d4293f6c360bc2e183bd64", null ],
                  [ "Cancel", "classPackuru_1_1Core_1_1Browser_1_1DeletionDialogCore.html#aae24373b6c0bbb21fcd8a1cf051e29afaea4788705e6873b424c65e91c2846b19", null ],
                  [ "DialogQuestion", "classPackuru_1_1Core_1_1Browser_1_1DeletionDialogCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa999d248c759f533003da12d4d8a7a5db", null ],
                  [ "DialogTitle", "classPackuru_1_1Core_1_1Browser_1_1DeletionDialogCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa59e6edcf06044686b74fc871f4098d0e", null ]
                ] ],
                [ "DeletionDialogCore", "classPackuru_1_1Core_1_1Browser_1_1DeletionDialogCore.html#a76ff4a3c669bdffb94074d3c67ac3d8f", null ],
                [ "~DeletionDialogCore", "classPackuru_1_1Core_1_1Browser_1_1DeletionDialogCore.html#a9683652e1738277ded62c17bd6d8cf11", null ],
                [ "accept", "classPackuru_1_1Core_1_1Browser_1_1DeletionDialogCore.html#a74ddbe2ce21e10cdd5e4586e2e9f0607", null ],
                [ "destroyLater", "classPackuru_1_1Core_1_1Browser_1_1DeletionDialogCore.html#aa5f97b4f6309ac39ce270d0ac0f92d50", null ],
                [ "getControllerEngine", "classPackuru_1_1Core_1_1Browser_1_1DeletionDialogCore.html#a72d2d5dda34983ff5afcb6039a09bdbb", null ],
                [ "getFileModel", "classPackuru_1_1Core_1_1Browser_1_1DeletionDialogCore.html#ac009248b1e2c5fd0df6ac227f4aa45d8", null ]
              ] ],
              [ "Init", "classPackuru_1_1Core_1_1Browser_1_1Init.html", [
                [ "Init", "classPackuru_1_1Core_1_1Browser_1_1Init.html#ab7ab6b6595f2fe7563a2484e7e9a92f1", null ],
                [ "~Init", "classPackuru_1_1Core_1_1Browser_1_1Init.html#a451d43761b3b3eeae21dd5559eeb8fb5", null ],
                [ "getMainWindowCore", "classPackuru_1_1Core_1_1Browser_1_1Init.html#abfa4078ed385965e314a1e0ae61e34be", null ],
                [ "messagePrimaryInstance", "classPackuru_1_1Core_1_1Browser_1_1Init.html#aea87a1bde153a87fcac831856fc448d8", null ],
                [ "processArguments", "classPackuru_1_1Core_1_1Browser_1_1Init.html#af1694f156f54fda65cf39d1a62a14bb5", null ]
              ] ],
              [ "MainWindowCore", "classPackuru_1_1Core_1_1Browser_1_1MainWindowCore.html", [
                [ "UserString", "classPackuru_1_1Core_1_1Browser_1_1MainWindowCore.html#aae24373b6c0bbb21fcd8a1cf051e29af", [
                  [ "___INVALID", "classPackuru_1_1Core_1_1Browser_1_1MainWindowCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa6e545caba6d4293f6c360bc2e183bd64", null ],
                  [ "ActionAbout", "classPackuru_1_1Core_1_1Browser_1_1MainWindowCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa223138955c9c0ef19f7b9eecd2f9bfc4", null ],
                  [ "ActionClose", "classPackuru_1_1Core_1_1Browser_1_1MainWindowCore.html#aae24373b6c0bbb21fcd8a1cf051e29afabcbb21e925d5eb7d17a949db6a852b5d", null ],
                  [ "ActionNew", "classPackuru_1_1Core_1_1Browser_1_1MainWindowCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa98ff5ebe2ae053cf1e008e791a4cf56e", null ],
                  [ "ActionOpen", "classPackuru_1_1Core_1_1Browser_1_1MainWindowCore.html#aae24373b6c0bbb21fcd8a1cf051e29afafc311944a280b5ce38182b7fb4237f4c", null ],
                  [ "ActionQuit", "classPackuru_1_1Core_1_1Browser_1_1MainWindowCore.html#aae24373b6c0bbb21fcd8a1cf051e29afae2edcf3a4e74ed1dbd7dab13ee58c7ab", null ],
                  [ "ActionSettings", "classPackuru_1_1Core_1_1Browser_1_1MainWindowCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa8a2f945561854bddbd40ca2b4b07cb33", null ],
                  [ "WindowTitle", "classPackuru_1_1Core_1_1Browser_1_1MainWindowCore.html#aae24373b6c0bbb21fcd8a1cf051e29afabfeec2b56d1c63a77acc8b2af0ef248e", null ]
                ] ],
                [ "MainWindowCore", "classPackuru_1_1Core_1_1Browser_1_1MainWindowCore.html#a8820fceb4da44fc9e86d7237025e3dac", null ],
                [ "~MainWindowCore", "classPackuru_1_1Core_1_1Browser_1_1MainWindowCore.html#a86f5b1c3c4a89d19f46b58a7af224106", null ],
                [ "commandLineError", "classPackuru_1_1Core_1_1Browser_1_1MainWindowCore.html#a7ecc760fd11ca5ea8fda293fc82b9c0c", null ],
                [ "commandLineHelpRequested", "classPackuru_1_1Core_1_1Browser_1_1MainWindowCore.html#aa206621758a6fd0fb7954e27d00ed3f8", null ],
                [ "createAppSettingsDialogCore", "classPackuru_1_1Core_1_1Browser_1_1MainWindowCore.html#a429ed735e31b974d11632492c54ff600", null ],
                [ "createArchive", "classPackuru_1_1Core_1_1Browser_1_1MainWindowCore.html#adf462c90bda9e42b34e92b032bb46847", null ],
                [ "createBrowserSettingsDialogCore", "classPackuru_1_1Core_1_1Browser_1_1MainWindowCore.html#a7a6edb296afed0b25a4778c4cb947d53", null ],
                [ "getBusyBrowserCount", "classPackuru_1_1Core_1_1Browser_1_1MainWindowCore.html#aac2c1c0581dac85c3ed8e5de17625af8", null ],
                [ "getQueueMessenger", "classPackuru_1_1Core_1_1Browser_1_1MainWindowCore.html#aa2039b6bb36fbf538b6a3197d3a53bc1", null ],
                [ "getString", "classPackuru_1_1Core_1_1Browser_1_1MainWindowCore.html#a803c0e47fdbaebbcca9c0f4e5f4ddd27", null ],
                [ "newBrowserCoreCreated", "classPackuru_1_1Core_1_1Browser_1_1MainWindowCore.html#ad55277600e8f642959f528faf4ce474d", null ],
                [ "newMessage", "classPackuru_1_1Core_1_1Browser_1_1MainWindowCore.html#a4d147bec8de4813d3aeb0d71bf159f68", null ],
                [ "openArchives", "classPackuru_1_1Core_1_1Browser_1_1MainWindowCore.html#a89ce54a606ebad8f59e5f9ce54ea94d4", null ],
                [ "openArchives", "classPackuru_1_1Core_1_1Browser_1_1MainWindowCore.html#a8285679a8a0fcc6210ec5c1010c0a45b", null ],
                [ "openArchives", "classPackuru_1_1Core_1_1Browser_1_1MainWindowCore.html#a437875e971d239b7f0bc0c0365a63179", null ]
              ] ],
              [ "QueueMessenger", "classPackuru_1_1Core_1_1Browser_1_1QueueMessenger.html", [
                [ "UserString", "classPackuru_1_1Core_1_1Browser_1_1QueueMessenger.html#aae24373b6c0bbb21fcd8a1cf051e29af", [
                  [ "___INVALID", "classPackuru_1_1Core_1_1Browser_1_1QueueMessenger.html#aae24373b6c0bbb21fcd8a1cf051e29afa6e545caba6d4293f6c360bc2e183bd64", null ],
                  [ "Abort", "classPackuru_1_1Core_1_1Browser_1_1QueueMessenger.html#aae24373b6c0bbb21fcd8a1cf051e29afa727b63583e01fa2b3952dab580c84dc2", null ],
                  [ "DialogTitle", "classPackuru_1_1Core_1_1Browser_1_1QueueMessenger.html#aae24373b6c0bbb21fcd8a1cf051e29afa59e6edcf06044686b74fc871f4098d0e", null ],
                  [ "RunInBrowser", "classPackuru_1_1Core_1_1Browser_1_1QueueMessenger.html#aae24373b6c0bbb21fcd8a1cf051e29afa1d989eaf8f6b9c606fa44d787f5deca5", null ]
                ] ],
                [ "QueueMessenger", "classPackuru_1_1Core_1_1Browser_1_1QueueMessenger.html#a4c02b54a2f1460394146a27abf0e05a0", null ],
                [ "~QueueMessenger", "classPackuru_1_1Core_1_1Browser_1_1QueueMessenger.html#a44eb1db37279ede50e382b5c3bb0e83c", null ],
                [ "abort", "classPackuru_1_1Core_1_1Browser_1_1QueueMessenger.html#a220fbc82bcea18cde1d84500470b38d1", null ],
                [ "activated", "classPackuru_1_1Core_1_1Browser_1_1QueueMessenger.html#a244e631e4637625aaca7abf94c04d4d1", null ],
                [ "activeChanged", "classPackuru_1_1Core_1_1Browser_1_1QueueMessenger.html#a56ce57451f645b6167547a0c8420fc54", null ],
                [ "canRunInBrowserChanged", "classPackuru_1_1Core_1_1Browser_1_1QueueMessenger.html#a982a6a4359613f78f54ee60b46009acf", null ],
                [ "deactivated", "classPackuru_1_1Core_1_1Browser_1_1QueueMessenger.html#acfd4ec43396d90d144a6b27bfe409465", null ],
                [ "getCanRunInBrowser", "classPackuru_1_1Core_1_1Browser_1_1QueueMessenger.html#aea636c48fba10f150bad4ab78b9b4164", null ],
                [ "getStatus", "classPackuru_1_1Core_1_1Browser_1_1QueueMessenger.html#ad0d914f1624c77ed8eb149f1ee698a40", null ],
                [ "isActive", "classPackuru_1_1Core_1_1Browser_1_1QueueMessenger.html#adb80df6eeafd900dfefcf90c56efad51", null ],
                [ "runInBrowser", "classPackuru_1_1Core_1_1Browser_1_1QueueMessenger.html#a0b21e846c084fc48ff34e24625fcab3b", null ],
                [ "statusChanged", "classPackuru_1_1Core_1_1Browser_1_1QueueMessenger.html#ad6ae348da15f16797bd854667b67ceee", null ],
                [ "active", "classPackuru_1_1Core_1_1Browser_1_1QueueMessenger.html#a03c996f9fcf0e10baeb3e700be0c409a", null ],
                [ "canRunInBrowser", "classPackuru_1_1Core_1_1Browser_1_1QueueMessenger.html#a61caedf8a282ceffb57f96f7768d9f79", null ],
                [ "status", "classPackuru_1_1Core_1_1Browser_1_1QueueMessenger.html#ac239e35c6fdaead7d9011c035268dadd", null ]
              ] ],
              [ "SettingsControllerType", "classPackuru_1_1Core_1_1Browser_1_1SettingsControllerType.html", [
                [ "Type", "classPackuru_1_1Core_1_1Browser_1_1SettingsControllerType.html#a1d1cfd8ffb84e947f82999c682b666a7", [
                  [ "ForceSingleInstance", "classPackuru_1_1Core_1_1Browser_1_1SettingsControllerType.html#a1d1cfd8ffb84e947f82999c682b666a7ae64517427e61b70bd8aa5c6b3708d09a", null ],
                  [ "ShowSummaryOnTaskCompletion", "classPackuru_1_1Core_1_1Browser_1_1SettingsControllerType.html#a1d1cfd8ffb84e947f82999c682b666a7a134e324704f9dc8589617e54b75baccd", null ]
                ] ]
              ] ],
              [ "SettingsDialogCore", "classPackuru_1_1Core_1_1Browser_1_1SettingsDialogCore.html", [
                [ "UserString", "classPackuru_1_1Core_1_1Browser_1_1SettingsDialogCore.html#aae24373b6c0bbb21fcd8a1cf051e29af", [
                  [ "___INVALID", "classPackuru_1_1Core_1_1Browser_1_1SettingsDialogCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa6e545caba6d4293f6c360bc2e183bd64", null ],
                  [ "PageTitle", "classPackuru_1_1Core_1_1Browser_1_1SettingsDialogCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa57a08c88b9aff49612ca6b19fd8bea90", null ]
                ] ],
                [ "SettingsDialogCore", "classPackuru_1_1Core_1_1Browser_1_1SettingsDialogCore.html#ac5701d5ae1efc89d382b324e4ea2d3b8", null ],
                [ "~SettingsDialogCore", "classPackuru_1_1Core_1_1Browser_1_1SettingsDialogCore.html#a0f841e3d6a6cdaf9837a251777d66af6", null ],
                [ "accept", "classPackuru_1_1Core_1_1Browser_1_1SettingsDialogCore.html#a74ddbe2ce21e10cdd5e4586e2e9f0607", null ],
                [ "destroyLater", "classPackuru_1_1Core_1_1Browser_1_1SettingsDialogCore.html#aa5f97b4f6309ac39ce270d0ac0f92d50", null ],
                [ "getControllerEngine", "classPackuru_1_1Core_1_1Browser_1_1SettingsDialogCore.html#a72d2d5dda34983ff5afcb6039a09bdbb", null ]
              ] ],
              [ "StatusPageCore", "classPackuru_1_1Core_1_1Browser_1_1StatusPageCore.html", [
                [ "StatusPageCore", "classPackuru_1_1Core_1_1Browser_1_1StatusPageCore.html#a40c513de3307d120d1aecdbfdd573b73", null ],
                [ "~StatusPageCore", "classPackuru_1_1Core_1_1Browser_1_1StatusPageCore.html#af965d740caeb5995b2c73011d8904020", null ],
                [ "getBasicControl", "classPackuru_1_1Core_1_1Browser_1_1StatusPageCore.html#a8a36ef4e747f1875b5a0787a831fb925", null ],
                [ "getProgressBarVisible", "classPackuru_1_1Core_1_1Browser_1_1StatusPageCore.html#a40bcf1e12ce7db5587c08fdd87bb0ab2", null ],
                [ "getTaskDescription", "classPackuru_1_1Core_1_1Browser_1_1StatusPageCore.html#a494c42a825c2ffd504449424d09cbc4b", null ],
                [ "getTaskProgress", "classPackuru_1_1Core_1_1Browser_1_1StatusPageCore.html#a9a257b3532387654746c8f810e3bcdb9", null ],
                [ "getTaskState", "classPackuru_1_1Core_1_1Browser_1_1StatusPageCore.html#ac5a1dc77e1abafd36428775d3dae8c36", null ],
                [ "progressBarVisibleChanged", "classPackuru_1_1Core_1_1Browser_1_1StatusPageCore.html#a2974bc1f2b81bd41ebbe933a18c095ce", null ],
                [ "showBasicControl", "classPackuru_1_1Core_1_1Browser_1_1StatusPageCore.html#aab7dda5ee791d12d2a371120ef66de68", null ],
                [ "showFileExistsPrompt", "classPackuru_1_1Core_1_1Browser_1_1StatusPageCore.html#a10044f78671170cc06fcefb11a183089", null ],
                [ "showFolderExistsPrompt", "classPackuru_1_1Core_1_1Browser_1_1StatusPageCore.html#a157adb894d6ff84e6b7c10a5bee55893", null ],
                [ "showItemTypeMismatchPrompt", "classPackuru_1_1Core_1_1Browser_1_1StatusPageCore.html#a474c9e79b435e2cca3b96362dd417cf8", null ],
                [ "showPasswordPrompt", "classPackuru_1_1Core_1_1Browser_1_1StatusPageCore.html#ab24dd3ded9c5283ea9fc6701dfa7633e", null ],
                [ "taskDescriptionChanged", "classPackuru_1_1Core_1_1Browser_1_1StatusPageCore.html#a1aee599ef614703adaaba2c9e23233df", null ],
                [ "taskProgressChanged", "classPackuru_1_1Core_1_1Browser_1_1StatusPageCore.html#aff8ed851abc98cad04e8c5143fd4c0f4", null ],
                [ "taskStateChanged", "classPackuru_1_1Core_1_1Browser_1_1StatusPageCore.html#a9c210d2ce4ca42a7c0ff5c064a2b1574", null ],
                [ "basicControl", "classPackuru_1_1Core_1_1Browser_1_1StatusPageCore.html#af0eed66356a41be25205e3fb32d60a28", null ],
                [ "progressBarVisible", "classPackuru_1_1Core_1_1Browser_1_1StatusPageCore.html#ad39364f25c67a631b9e814fa92b31c2a", null ],
                [ "taskDescription", "classPackuru_1_1Core_1_1Browser_1_1StatusPageCore.html#a7755def137e650ae374fba38ae0558ed", null ],
                [ "taskProgress", "classPackuru_1_1Core_1_1Browser_1_1StatusPageCore.html#a1456590ec5da6a8cdd21a6e5085c27e2", null ],
                [ "taskState", "classPackuru_1_1Core_1_1Browser_1_1StatusPageCore.html#adb9b324dde3d0328af7af7481da25176", null ]
              ] ],
              [ "TabClosingDialogCore", "classPackuru_1_1Core_1_1Browser_1_1TabClosingDialogCore.html", [
                [ "UserString", "classPackuru_1_1Core_1_1Browser_1_1TabClosingDialogCore.html#aae24373b6c0bbb21fcd8a1cf051e29af", [
                  [ "___INVALID", "classPackuru_1_1Core_1_1Browser_1_1TabClosingDialogCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa6e545caba6d4293f6c360bc2e183bd64", null ],
                  [ "Cancel", "classPackuru_1_1Core_1_1Browser_1_1TabClosingDialogCore.html#aae24373b6c0bbb21fcd8a1cf051e29afaea4788705e6873b424c65e91c2846b19", null ],
                  [ "Close", "classPackuru_1_1Core_1_1Browser_1_1TabClosingDialogCore.html#aae24373b6c0bbb21fcd8a1cf051e29afad3d2e617335f08df83599665eef8a418", null ],
                  [ "DialogTitle", "classPackuru_1_1Core_1_1Browser_1_1TabClosingDialogCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa59e6edcf06044686b74fc871f4098d0e", null ],
                  [ "DoYouWantToCloseIt", "classPackuru_1_1Core_1_1Browser_1_1TabClosingDialogCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa6c2f589a66f5d1badac5ae0170cb0a49", null ],
                  [ "ThisTabIsBusy", "classPackuru_1_1Core_1_1Browser_1_1TabClosingDialogCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa996758d9a293210523bd070a48c476cb", null ]
                ] ],
                [ "TabClosingDialogCore", "classPackuru_1_1Core_1_1Browser_1_1TabClosingDialogCore.html#a96cfd2325d7f154bc5683ab602c08e69", null ]
              ] ],
              [ "ViewFilterControllerType", "classPackuru_1_1Core_1_1Browser_1_1ViewFilterControllerType.html", [
                [ "Type", "classPackuru_1_1Core_1_1Browser_1_1ViewFilterControllerType.html#a1d1cfd8ffb84e947f82999c682b666a7", [
                  [ "FilterFiles", "classPackuru_1_1Core_1_1Browser_1_1ViewFilterControllerType.html#a1d1cfd8ffb84e947f82999c682b666a7a2981599b9df1f02d9e113e1c90d06eef", null ],
                  [ "FilterFolders", "classPackuru_1_1Core_1_1Browser_1_1ViewFilterControllerType.html#a1d1cfd8ffb84e947f82999c682b666a7a3b41c5b816c09fad2f5ed0eb29bf7875", null ],
                  [ "FilterScope", "classPackuru_1_1Core_1_1Browser_1_1ViewFilterControllerType.html#a1d1cfd8ffb84e947f82999c682b666a7a806fef84c314e95fdaf4cfce28f3ef5b", null ],
                  [ "FilterText", "classPackuru_1_1Core_1_1Browser_1_1ViewFilterControllerType.html#a1d1cfd8ffb84e947f82999c682b666a7aadc15cb731ea0e6ec61cd2a1f933dca5", null ]
                ] ]
              ] ]
            ] ],
            [ "Queue", "namespacePackuru_1_1Core_1_1Queue.html", [
              [ "Init", "classPackuru_1_1Core_1_1Queue_1_1Init.html", [
                [ "Init", "classPackuru_1_1Core_1_1Queue_1_1Init.html#a1bdcf02cbfc67a99f726eec8c57fed44", null ],
                [ "~Init", "classPackuru_1_1Core_1_1Queue_1_1Init.html#a970c1ac01b53341336e950772d76669d", null ],
                [ "getQueueCore", "classPackuru_1_1Core_1_1Queue_1_1Init.html#a5de0743cb1f27466f8c63d5fa7dea147", null ],
                [ "messagePrimaryInstance", "classPackuru_1_1Core_1_1Queue_1_1Init.html#aea87a1bde153a87fcac831856fc448d8", null ],
                [ "processArguments", "classPackuru_1_1Core_1_1Queue_1_1Init.html#af1694f156f54fda65cf39d1a62a14bb5", null ]
              ] ],
              [ "MainWindowCore", "classPackuru_1_1Core_1_1Queue_1_1MainWindowCore.html", [
                [ "UserString", "classPackuru_1_1Core_1_1Queue_1_1MainWindowCore.html#aae24373b6c0bbb21fcd8a1cf051e29af", [
                  [ "___INVALID", "classPackuru_1_1Core_1_1Queue_1_1MainWindowCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa6e545caba6d4293f6c360bc2e183bd64", null ],
                  [ "ActionAbout", "classPackuru_1_1Core_1_1Queue_1_1MainWindowCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa223138955c9c0ef19f7b9eecd2f9bfc4", null ],
                  [ "ActionExtract", "classPackuru_1_1Core_1_1Queue_1_1MainWindowCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa7f2a064ae66cff8649053b333b33de7b", null ],
                  [ "ActionNew", "classPackuru_1_1Core_1_1Queue_1_1MainWindowCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa98ff5ebe2ae053cf1e008e791a4cf56e", null ],
                  [ "ActionQuit", "classPackuru_1_1Core_1_1Queue_1_1MainWindowCore.html#aae24373b6c0bbb21fcd8a1cf051e29afae2edcf3a4e74ed1dbd7dab13ee58c7ab", null ],
                  [ "ActionSettings", "classPackuru_1_1Core_1_1Queue_1_1MainWindowCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa8a2f945561854bddbd40ca2b4b07cb33", null ],
                  [ "ActionTest", "classPackuru_1_1Core_1_1Queue_1_1MainWindowCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa107f1c4fc93e51f8d693fa4c9eabac77", null ],
                  [ "WindowTitle", "classPackuru_1_1Core_1_1Queue_1_1MainWindowCore.html#aae24373b6c0bbb21fcd8a1cf051e29afabfeec2b56d1c63a77acc8b2af0ef248e", null ]
                ] ],
                [ "MainWindowCore", "classPackuru_1_1Core_1_1Queue_1_1MainWindowCore.html#a1a864a61662d070daa97da45ccd35449", null ],
                [ "~MainWindowCore", "classPackuru_1_1Core_1_1Queue_1_1MainWindowCore.html#a86f5b1c3c4a89d19f46b58a7af224106", null ],
                [ "allTasksCompletedReadyToClose", "classPackuru_1_1Core_1_1Queue_1_1MainWindowCore.html#a284880af28e1c7081dca91e15eddbb47", null ],
                [ "getTaskQueueModel", "classPackuru_1_1Core_1_1Queue_1_1MainWindowCore.html#a8879298f727d96d8f3d58ef568b43dc7", null ]
              ] ],
              [ "QueueCore", "classPackuru_1_1Core_1_1Queue_1_1QueueCore.html", [
                [ "QueueCore", "classPackuru_1_1Core_1_1Queue_1_1QueueCore.html#a496e0a03e027358308d8e036ebfaf4b2", null ],
                [ "~QueueCore", "classPackuru_1_1Core_1_1Queue_1_1QueueCore.html#ad0485dfd874c059db6b7eca001ae76b6", null ],
                [ "archivingDialogCoreCreated", "classPackuru_1_1Core_1_1Queue_1_1QueueCore.html#ac56b60c0e2ffaca0a966f44e11e8f777", null ],
                [ "commandLineError", "classPackuru_1_1Core_1_1Queue_1_1QueueCore.html#a90aa69260286b0d6341eb32a4a0273ce", null ],
                [ "commandLineHelpRequested", "classPackuru_1_1Core_1_1Queue_1_1QueueCore.html#aa206621758a6fd0fb7954e27d00ed3f8", null ],
                [ "createAppSettingsDialogCore", "classPackuru_1_1Core_1_1Queue_1_1QueueCore.html#a429ed735e31b974d11632492c54ff600", null ],
                [ "createArchivingDialogCore", "classPackuru_1_1Core_1_1Queue_1_1QueueCore.html#aa469eb7c512584e02e802c6bb21e80b5", null ],
                [ "createExtractionDialogCore", "classPackuru_1_1Core_1_1Queue_1_1QueueCore.html#a700149f85c2a574366744f64084192d4", null ],
                [ "createQueueSettingsDialogCore", "classPackuru_1_1Core_1_1Queue_1_1QueueCore.html#a9452a1449ace31158d5815422cfd2995", null ],
                [ "createTestDialogCore", "classPackuru_1_1Core_1_1Queue_1_1QueueCore.html#a1826e9d2b6810e1e466b7a11f2fc5f27", null ],
                [ "defaultStartUp", "classPackuru_1_1Core_1_1Queue_1_1QueueCore.html#a84a3f5dcac1e9ba0e946b25363bb86cb", null ],
                [ "extractionDialogCoreCreated", "classPackuru_1_1Core_1_1Queue_1_1QueueCore.html#a74fa4e7bd02b29ea2088927456badb16", null ],
                [ "getMainWindowCore", "classPackuru_1_1Core_1_1Queue_1_1QueueCore.html#a3bc3780dfb5183815bd888fa90ed50c7", null ],
                [ "newTasks", "classPackuru_1_1Core_1_1Queue_1_1QueueCore.html#aa8d0ddf089947ded76e04e74b3446167", null ],
                [ "testDialogCoreCreated", "classPackuru_1_1Core_1_1Queue_1_1QueueCore.html#a48f6ea6c856f8ad345691276cff76633", null ]
              ] ],
              [ "SettingsControllerType", "classPackuru_1_1Core_1_1Queue_1_1SettingsControllerType.html", [
                [ "Type", "classPackuru_1_1Core_1_1Queue_1_1SettingsControllerType.html#a1d1cfd8ffb84e947f82999c682b666a7", [
                  [ "AbortTasksRequiringPassword", "classPackuru_1_1Core_1_1Queue_1_1SettingsControllerType.html#a1d1cfd8ffb84e947f82999c682b666a7ab58f0d8290bec3b07316713d44c1477f", null ],
                  [ "AutoCloseOnAllTasksCompletion", "classPackuru_1_1Core_1_1Queue_1_1SettingsControllerType.html#a1d1cfd8ffb84e947f82999c682b666a7a8c25d0f5d9093f5325ddd52aea073e84", null ],
                  [ "AutoRemoveCompletedTasks", "classPackuru_1_1Core_1_1Queue_1_1SettingsControllerType.html#a1d1cfd8ffb84e947f82999c682b666a7a85f73d8d8a6f269f8e7ebfb4d51af6ff", null ],
                  [ "MaxErrorCount", "classPackuru_1_1Core_1_1Queue_1_1SettingsControllerType.html#a1d1cfd8ffb84e947f82999c682b666a7a36111345248439ea9576d1b46c8a84b9", null ]
                ] ]
              ] ],
              [ "SettingsDialogCore", "classPackuru_1_1Core_1_1Queue_1_1SettingsDialogCore.html", [
                [ "UserString", "classPackuru_1_1Core_1_1Queue_1_1SettingsDialogCore.html#aae24373b6c0bbb21fcd8a1cf051e29af", [
                  [ "___INVALID", "classPackuru_1_1Core_1_1Queue_1_1SettingsDialogCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa6e545caba6d4293f6c360bc2e183bd64", null ],
                  [ "PageTitle", "classPackuru_1_1Core_1_1Queue_1_1SettingsDialogCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa57a08c88b9aff49612ca6b19fd8bea90", null ]
                ] ],
                [ "SettingsDialogCore", "classPackuru_1_1Core_1_1Queue_1_1SettingsDialogCore.html#ac5701d5ae1efc89d382b324e4ea2d3b8", null ],
                [ "~SettingsDialogCore", "classPackuru_1_1Core_1_1Queue_1_1SettingsDialogCore.html#a0f841e3d6a6cdaf9837a251777d66af6", null ],
                [ "accept", "classPackuru_1_1Core_1_1Queue_1_1SettingsDialogCore.html#a74ddbe2ce21e10cdd5e4586e2e9f0607", null ],
                [ "destroyLater", "classPackuru_1_1Core_1_1Queue_1_1SettingsDialogCore.html#aa5f97b4f6309ac39ce270d0ac0f92d50", null ],
                [ "getControllerEngine", "classPackuru_1_1Core_1_1Queue_1_1SettingsDialogCore.html#a72d2d5dda34983ff5afcb6039a09bdbb", null ]
              ] ],
              [ "TaskQueueModel", "classPackuru_1_1Core_1_1Queue_1_1TaskQueueModel.html", [
                [ "UserString", "classPackuru_1_1Core_1_1Queue_1_1TaskQueueModel.html#aae24373b6c0bbb21fcd8a1cf051e29af", [
                  [ "___INVALID", "classPackuru_1_1Core_1_1Queue_1_1TaskQueueModel.html#aae24373b6c0bbb21fcd8a1cf051e29afa6e545caba6d4293f6c360bc2e183bd64", null ],
                  [ "ActionErrorLog", "classPackuru_1_1Core_1_1Queue_1_1TaskQueueModel.html#aae24373b6c0bbb21fcd8a1cf051e29afac0ea3d943f017ec563e153d87959ba59", null ],
                  [ "ActionRemoveCompleted", "classPackuru_1_1Core_1_1Queue_1_1TaskQueueModel.html#aae24373b6c0bbb21fcd8a1cf051e29afa604693d2bfbde294601d627ecce28186", null ],
                  [ "ActionRemoveTask", "classPackuru_1_1Core_1_1Queue_1_1TaskQueueModel.html#aae24373b6c0bbb21fcd8a1cf051e29afade6f403f3df6e66b540c8dd2983ceb64", null ],
                  [ "ActionResetTask", "classPackuru_1_1Core_1_1Queue_1_1TaskQueueModel.html#aae24373b6c0bbb21fcd8a1cf051e29afac1440498d690395b7ea2e4fbf6a9bd43", null ],
                  [ "ActionStartQueue", "classPackuru_1_1Core_1_1Queue_1_1TaskQueueModel.html#aae24373b6c0bbb21fcd8a1cf051e29afaf6999cfbb106d13e44f98bbb851b7fb9", null ],
                  [ "ActionStopHere", "classPackuru_1_1Core_1_1Queue_1_1TaskQueueModel.html#aae24373b6c0bbb21fcd8a1cf051e29afa2ce8f75eddbe22fd608894e631aa3ae6", null ],
                  [ "ActionStopQueue", "classPackuru_1_1Core_1_1Queue_1_1TaskQueueModel.html#aae24373b6c0bbb21fcd8a1cf051e29afa01825dd0ef7a527e038d89819f78942e", null ]
                ] ],
                [ "TaskQueueModel", "classPackuru_1_1Core_1_1Queue_1_1TaskQueueModel.html#a354c2271332e2e92dd77aba7a465df40", null ],
                [ "~TaskQueueModel", "classPackuru_1_1Core_1_1Queue_1_1TaskQueueModel.html#a4502f11885f3d4b4a9dad35bc982f143", null ],
                [ "allTasksCompleted", "classPackuru_1_1Core_1_1Queue_1_1TaskQueueModel.html#a1bdf4ce4342340d77d93d3db15c97408", null ],
                [ "allTasksFinished", "classPackuru_1_1Core_1_1Queue_1_1TaskQueueModel.html#a64dc75a3b1ebd406468eafc21d701b2e", null ],
                [ "archiveCreationNameError", "classPackuru_1_1Core_1_1Queue_1_1TaskQueueModel.html#a12e37d843735d202965d5426b6d31f71", null ],
                [ "areAllTasksCompleted", "classPackuru_1_1Core_1_1Queue_1_1TaskQueueModel.html#a1a0cc0f1db5491875233788f08ea3e98", null ],
                [ "columnCount", "classPackuru_1_1Core_1_1Queue_1_1TaskQueueModel.html#a772927c4f845c5d0efe36b0c51245ba0", null ],
                [ "completedTaskCountChanged", "classPackuru_1_1Core_1_1Queue_1_1TaskQueueModel.html#a7e70fdaa174e829df0b2bfd051ced57c", null ],
                [ "data", "classPackuru_1_1Core_1_1Queue_1_1TaskQueueModel.html#ae93b915598cdbe5ae59dddf41d917a53", null ],
                [ "fileExists", "classPackuru_1_1Core_1_1Queue_1_1TaskQueueModel.html#a453edcd20ca0bf273682814dbaca10d1", null ],
                [ "folderExists", "classPackuru_1_1Core_1_1Queue_1_1TaskQueueModel.html#ae45f8c8676c16fa2ebc15c5962c41d20", null ],
                [ "getCompletedTaskCount", "classPackuru_1_1Core_1_1Queue_1_1TaskQueueModel.html#af4aa80740f7ad19613bf928323e8e95e", null ],
                [ "getErrorLog", "classPackuru_1_1Core_1_1Queue_1_1TaskQueueModel.html#af6cd4da32460a6716bd7e2a374ce4080", null ],
                [ "getQueueDescription", "classPackuru_1_1Core_1_1Queue_1_1TaskQueueModel.html#aed2bb6103938865d3ccdf9fa5563a021", null ],
                [ "hasErrorLog", "classPackuru_1_1Core_1_1Queue_1_1TaskQueueModel.html#ada3aeaeecc6242fea5f606ba4f526541", null ],
                [ "headerData", "classPackuru_1_1Core_1_1Queue_1_1TaskQueueModel.html#a0b82c5f78bc6bc150160c0b3f73e22ba", null ],
                [ "isRunning", "classPackuru_1_1Core_1_1Queue_1_1TaskQueueModel.html#ad619b8b51adc7874204abd71080cd90e", null ],
                [ "itemTypeMismatch", "classPackuru_1_1Core_1_1Queue_1_1TaskQueueModel.html#a09f61504055a608f95d3eba98725adfb", null ],
                [ "openDestinationFolder", "classPackuru_1_1Core_1_1Queue_1_1TaskQueueModel.html#a21fc7c78ec174d02892c06b8c6b0450f", null ],
                [ "openNewArchive", "classPackuru_1_1Core_1_1Queue_1_1TaskQueueModel.html#a08d3aae1a4ddf606dfcc8943c0a73aef", null ],
                [ "passwordNeeded", "classPackuru_1_1Core_1_1Queue_1_1TaskQueueModel.html#ae0ffe92903ecc8a43f46f2e371e29930", null ],
                [ "progressColumn", "classPackuru_1_1Core_1_1Queue_1_1TaskQueueModel.html#ad4e3df7b726eb543e20641a1ffa2c807", null ],
                [ "queueDescriptionChanged", "classPackuru_1_1Core_1_1Queue_1_1TaskQueueModel.html#af7b06e142539f144b8ba3ba1e189d7bb", null ],
                [ "removeCompletedTasks", "classPackuru_1_1Core_1_1Queue_1_1TaskQueueModel.html#a68a22aa4b60e3c8af137eb2fb174cba0", null ],
                [ "removeRows", "classPackuru_1_1Core_1_1Queue_1_1TaskQueueModel.html#a52044751b750cfaf51b877a7970911c9", null ],
                [ "resetTasks", "classPackuru_1_1Core_1_1Queue_1_1TaskQueueModel.html#a0ceed59a3c749f1a2e8e2b6ec4c91281", null ],
                [ "roleNames", "classPackuru_1_1Core_1_1Queue_1_1TaskQueueModel.html#ad4bfae6b95ad33dca637414040872cc2", null ],
                [ "rowCount", "classPackuru_1_1Core_1_1Queue_1_1TaskQueueModel.html#a63ff48c076d848a157ed82c970805ef7", null ],
                [ "runningChanged", "classPackuru_1_1Core_1_1Queue_1_1TaskQueueModel.html#a9fce3135084d066c916bba956287061b", null ],
                [ "startQueue", "classPackuru_1_1Core_1_1Queue_1_1TaskQueueModel.html#a74cae7abc2b49d92f6d2f82ffc8b3217", null ],
                [ "stopAfterTask", "classPackuru_1_1Core_1_1Queue_1_1TaskQueueModel.html#a910eb219460c811016bfacfd693d3d14", null ],
                [ "stopQueue", "classPackuru_1_1Core_1_1Queue_1_1TaskQueueModel.html#a9778c72d81d7f5aa8456e9e4379f406a", null ],
                [ "taskStateChanged", "classPackuru_1_1Core_1_1Queue_1_1TaskQueueModel.html#a163bae033ae25d9104116cf8e1190557", null ],
                [ "unfinishedTaskCount", "classPackuru_1_1Core_1_1Queue_1_1TaskQueueModel.html#a68f7c4af5c3a3853221e4f7a07695e08", null ],
                [ "completedTaskCount", "classPackuru_1_1Core_1_1Queue_1_1TaskQueueModel.html#ae7327933071159f823999a049d9f6e04", null ],
                [ "description", "classPackuru_1_1Core_1_1Queue_1_1TaskQueueModel.html#a867c710287b9ea1a0ab27c30886fb5c3", null ],
                [ "running", "classPackuru_1_1Core_1_1Queue_1_1TaskQueueModel.html#a36f7b6be7108281af77939ceaec42fd6", null ]
              ] ]
            ] ],
            [ "AboutDialogCore", "classPackuru_1_1Core_1_1AboutDialogCore.html", [
              [ "UserString", "classPackuru_1_1Core_1_1AboutDialogCore.html#aae24373b6c0bbb21fcd8a1cf051e29af", [
                [ "___INVALID", "classPackuru_1_1Core_1_1AboutDialogCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa6e545caba6d4293f6c360bc2e183bd64", null ],
                [ "AppInfo", "classPackuru_1_1Core_1_1AboutDialogCore.html#aae24373b6c0bbb21fcd8a1cf051e29afad6dfc8be95fcca215d2e5cd132f9780a", null ],
                [ "DialogTitle", "classPackuru_1_1Core_1_1AboutDialogCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa59e6edcf06044686b74fc871f4098d0e", null ]
              ] ],
              [ "AboutDialogCore", "classPackuru_1_1Core_1_1AboutDialogCore.html#a2414d4cdb873d32202412795e3d4cc42", null ]
            ] ],
            [ "AppClosingDialogCore", "classPackuru_1_1Core_1_1AppClosingDialogCore.html", [
              [ "UserString", "classPackuru_1_1Core_1_1AppClosingDialogCore.html#aae24373b6c0bbb21fcd8a1cf051e29af", [
                [ "___INVALID", "classPackuru_1_1Core_1_1AppClosingDialogCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa6e545caba6d4293f6c360bc2e183bd64", null ],
                [ "BrowserBusyBrowsersInfo", "classPackuru_1_1Core_1_1AppClosingDialogCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa2618e2ddfa9c945c73f76b08b48c8944", null ],
                [ "Cancel", "classPackuru_1_1Core_1_1AppClosingDialogCore.html#aae24373b6c0bbb21fcd8a1cf051e29afaea4788705e6873b424c65e91c2846b19", null ],
                [ "DialogTitle", "classPackuru_1_1Core_1_1AppClosingDialogCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa59e6edcf06044686b74fc871f4098d0e", null ],
                [ "QueueOpenedDialogsInfo", "classPackuru_1_1Core_1_1AppClosingDialogCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa2d73907177fa0010c69448417b49bea5", null ],
                [ "QueueUnfinishedTasksInfo", "classPackuru_1_1Core_1_1AppClosingDialogCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa9b2fabf6db9d0458429eeca5ffd11968", null ],
                [ "Quit", "classPackuru_1_1Core_1_1AppClosingDialogCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa0d82790b0612935992bd564a17ce37d6", null ],
                [ "QuitQuestion", "classPackuru_1_1Core_1_1AppClosingDialogCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa66a140e4aab9ef25be947ef056087be6", null ]
              ] ],
              [ "AppClosingDialogCore", "classPackuru_1_1Core_1_1AppClosingDialogCore.html#acf12ecc20178dda6894af8d721156220", null ]
            ] ],
            [ "AppInfo", "structPackuru_1_1Core_1_1AppInfo.html", null ],
            [ "AppSettingsControllerType", "classPackuru_1_1Core_1_1AppSettingsControllerType.html", [
              [ "Type", "classPackuru_1_1Core_1_1AppSettingsControllerType.html#a1d1cfd8ffb84e947f82999c682b666a7", [
                [ "CompressionRatioDisplayMode", "classPackuru_1_1Core_1_1AppSettingsControllerType.html#a1d1cfd8ffb84e947f82999c682b666a7a425b858601f3fb84e14a684d4e73a802", null ],
                [ "SizeUnits", "classPackuru_1_1Core_1_1AppSettingsControllerType.html#a1d1cfd8ffb84e947f82999c682b666a7aee2f01136cdca8e061e3cb95e1b2442e", null ],
                [ "SizeUnitsExample", "classPackuru_1_1Core_1_1AppSettingsControllerType.html#a1d1cfd8ffb84e947f82999c682b666a7af6e0ffe2e9ac82969bd684ab3b717971", null ]
              ] ]
            ] ],
            [ "AppSettingsDialogCore", "classPackuru_1_1Core_1_1AppSettingsDialogCore.html", [
              [ "UserString", "classPackuru_1_1Core_1_1AppSettingsDialogCore.html#aae24373b6c0bbb21fcd8a1cf051e29af", [
                [ "___INVALID", "classPackuru_1_1Core_1_1AppSettingsDialogCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa6e545caba6d4293f6c360bc2e183bd64", null ],
                [ "CategoryArchiveTypes", "classPackuru_1_1Core_1_1AppSettingsDialogCore.html#aae24373b6c0bbb21fcd8a1cf051e29afadf4b7587e5ef246b5502df92d685f563", null ],
                [ "CategoryGeneral", "classPackuru_1_1Core_1_1AppSettingsDialogCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa0e9f3de0f873ad14438e6cd7006124e0", null ],
                [ "CategoryPlugins", "classPackuru_1_1Core_1_1AppSettingsDialogCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa208b653ed3fd4c7333188decb8a17040", null ],
                [ "DialogTitle", "classPackuru_1_1Core_1_1AppSettingsDialogCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa59e6edcf06044686b74fc871f4098d0e", null ],
                [ "FilterPlaceholderText", "classPackuru_1_1Core_1_1AppSettingsDialogCore.html#aae24373b6c0bbb21fcd8a1cf051e29afab0d6242fbc8ca925f8024534f5a2f2e7", null ],
                [ "PluginBackendHomepage", "classPackuru_1_1Core_1_1AppSettingsDialogCore.html#aae24373b6c0bbb21fcd8a1cf051e29afaec5edb50017c5c2a601da10ca00ac0d8", null ],
                [ "PluginConfigChangeWarning", "classPackuru_1_1Core_1_1AppSettingsDialogCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa08191a0762d5e1e9be9b91a2d55866eb", null ],
                [ "PluginDescription", "classPackuru_1_1Core_1_1AppSettingsDialogCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa05eae5de8a43c7c4e806524d2ea424d9", null ],
                [ "PluginFilePath", "classPackuru_1_1Core_1_1AppSettingsDialogCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa27c9b09928edc09f21ea4101db5a83d0", null ],
                [ "PluginReads", "classPackuru_1_1Core_1_1AppSettingsDialogCore.html#aae24373b6c0bbb21fcd8a1cf051e29afae8857a137f2f4684f0e5b53742ade128", null ],
                [ "PluginWrites", "classPackuru_1_1Core_1_1AppSettingsDialogCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa18b6758caf4ad5b0431a7ebe6afdbf6f", null ],
                [ "ReadWarning", "classPackuru_1_1Core_1_1AppSettingsDialogCore.html#aae24373b6c0bbb21fcd8a1cf051e29afaf07cc975ba4be3bbb150155023a5e650", null ]
              ] ],
              [ "AppSettingsDialogCore", "classPackuru_1_1Core_1_1AppSettingsDialogCore.html#a3293f393676798622a56615503c861bd", null ],
              [ "~AppSettingsDialogCore", "classPackuru_1_1Core_1_1AppSettingsDialogCore.html#ad62415d8b3a1960dab35527f18339b95", null ],
              [ "accept", "classPackuru_1_1Core_1_1AppSettingsDialogCore.html#a74ddbe2ce21e10cdd5e4586e2e9f0607", null ],
              [ "destroyLater", "classPackuru_1_1Core_1_1AppSettingsDialogCore.html#aa5f97b4f6309ac39ce270d0ac0f92d50", null ],
              [ "getArchiveTypeModel", "classPackuru_1_1Core_1_1AppSettingsDialogCore.html#a4ef499b8165e1e6c3ba35ca23f093d26", null ],
              [ "getControllerEngine", "classPackuru_1_1Core_1_1AppSettingsDialogCore.html#a72d2d5dda34983ff5afcb6039a09bdbb", null ],
              [ "getPluginModel", "classPackuru_1_1Core_1_1AppSettingsDialogCore.html#a1c305c9f96d17444573d082595e10a15", null ]
            ] ],
            [ "ArchiveTypeFilterModel", "classPackuru_1_1Core_1_1ArchiveTypeFilterModel.html", [
              [ "ArchiveTypeFilterModel", "classPackuru_1_1Core_1_1ArchiveTypeFilterModel.html#a9ed1b0dedfb7a1ba46c1e9ce7931d343", null ],
              [ "~ArchiveTypeFilterModel", "classPackuru_1_1Core_1_1ArchiveTypeFilterModel.html#a48cb6920e55f8c37a7cd5483a4918aff", null ],
              [ "filterAcceptsRow", "classPackuru_1_1Core_1_1ArchiveTypeFilterModel.html#a5bdf08bac5a30438e2fa64e66b7fd6f5", null ],
              [ "getFilterFixedString", "classPackuru_1_1Core_1_1ArchiveTypeFilterModel.html#acc2dd347af2cebf5c70f2435b130811a", null ],
              [ "filterFixedString", "classPackuru_1_1Core_1_1ArchiveTypeFilterModel.html#a64e1407a3ce0ca5bd556677359352081", null ]
            ] ],
            [ "ArchiveTypeModel", "classPackuru_1_1Core_1_1ArchiveTypeModel.html", [
              [ "ArchiveTypeModel", "classPackuru_1_1Core_1_1ArchiveTypeModel.html#a6d13596be27c85b5af4b7b95d22ba4c7", null ],
              [ "~ArchiveTypeModel", "classPackuru_1_1Core_1_1ArchiveTypeModel.html#abeb879485beaabd0f1c3eb5500fa0ffe", null ],
              [ "columnCount", "classPackuru_1_1Core_1_1ArchiveTypeModel.html#a65c9695e4a2778f358da46947cac09d0", null ],
              [ "data", "classPackuru_1_1Core_1_1ArchiveTypeModel.html#aa06564528ca791fce5d559b298edbb18", null ],
              [ "flags", "classPackuru_1_1Core_1_1ArchiveTypeModel.html#a58221c89c3d72d2c781fa6a57c483eba", null ],
              [ "getCurrentReadPlugin", "classPackuru_1_1Core_1_1ArchiveTypeModel.html#a4a393024b1e4c164832510e8d8b8732b", null ],
              [ "getCurrentWritePlugin", "classPackuru_1_1Core_1_1ArchiveTypeModel.html#ab0f2344dd69c7206ba8f927ab3db5c5e", null ],
              [ "getReadPlugins", "classPackuru_1_1Core_1_1ArchiveTypeModel.html#aaf8a722ee0dec747c55c51fa484911f2", null ],
              [ "getValueSet", "classPackuru_1_1Core_1_1ArchiveTypeModel.html#a2fde80eaf8172522531d4da720bbaf59", null ],
              [ "getWritePlugins", "classPackuru_1_1Core_1_1ArchiveTypeModel.html#ae3b622b2e46f875731096cca72c57677", null ],
              [ "headerData", "classPackuru_1_1Core_1_1ArchiveTypeModel.html#a3dcf81ea724475d324f9fdd1481492ea", null ],
              [ "rowCount", "classPackuru_1_1Core_1_1ArchiveTypeModel.html#abb3de2795579c97b3ceea401e9c9677d", null ],
              [ "setData", "classPackuru_1_1Core_1_1ArchiveTypeModel.html#a893d1b7c0d8fc61472f7dcd4d3efa055", null ],
              [ "setReadPlugin", "classPackuru_1_1Core_1_1ArchiveTypeModel.html#a166916df15734a476f1c34237707887b", null ],
              [ "setWritePlugin", "classPackuru_1_1Core_1_1ArchiveTypeModel.html#a76e91012f02e9b545692d342bdc01cdd", null ]
            ] ],
            [ "ArchivingDialogControllerType", "classPackuru_1_1Core_1_1ArchivingDialogControllerType.html", [
              [ "Type", "classPackuru_1_1Core_1_1ArchivingDialogControllerType.html#a1d1cfd8ffb84e947f82999c682b666a7", [
                [ "AcceptButton", "classPackuru_1_1Core_1_1ArchivingDialogControllerType.html#a1d1cfd8ffb84e947f82999c682b666a7a54d40f47d348115168307bd4c1cdada0", null ],
                [ "ArchiveName", "classPackuru_1_1Core_1_1ArchivingDialogControllerType.html#a1d1cfd8ffb84e947f82999c682b666a7aa11d8253c0a8329965f7d7e1a86d48e3", null ],
                [ "ArchiveNameError", "classPackuru_1_1Core_1_1ArchivingDialogControllerType.html#a1d1cfd8ffb84e947f82999c682b666a7a405cb0a3e8ad6138b049cea9c59a8d6f", null ],
                [ "ArchiveType", "classPackuru_1_1Core_1_1ArchivingDialogControllerType.html#a1d1cfd8ffb84e947f82999c682b666a7ad77f4250386da13d83f709b54bac6012", null ],
                [ "ArchivingMode", "classPackuru_1_1Core_1_1ArchivingDialogControllerType.html#a1d1cfd8ffb84e947f82999c682b666a7a0ab41235d375ac2f1f4c30c3f11c2424", null ],
                [ "CompressionLevel", "classPackuru_1_1Core_1_1ArchivingDialogControllerType.html#a1d1cfd8ffb84e947f82999c682b666a7adc2e159f049625277b8a33d51074e5bf", null ],
                [ "CompressionState", "classPackuru_1_1Core_1_1ArchivingDialogControllerType.html#a1d1cfd8ffb84e947f82999c682b666a7ae8d3cee45bdbd55c37e9be5119368151", null ],
                [ "DestinationMode", "classPackuru_1_1Core_1_1ArchivingDialogControllerType.html#a1d1cfd8ffb84e947f82999c682b666a7a74e856f5a8e178cf38ae4e7c58bc838d", null ],
                [ "DestinationPath", "classPackuru_1_1Core_1_1ArchivingDialogControllerType.html#a1d1cfd8ffb84e947f82999c682b666a7aed14a1948af1b4acdadbdaefd50722a6", null ],
                [ "DialogErrors", "classPackuru_1_1Core_1_1ArchivingDialogControllerType.html#a1d1cfd8ffb84e947f82999c682b666a7a8445140d1b04381ede8375dc9e157ed1", null ],
                [ "EncryptionState", "classPackuru_1_1Core_1_1ArchivingDialogControllerType.html#a1d1cfd8ffb84e947f82999c682b666a7a28893619a707715e34dcdee9df69e59d", null ],
                [ "DirectoryError", "classPackuru_1_1Core_1_1ArchivingDialogControllerType.html#a1d1cfd8ffb84e947f82999c682b666a7a6b4b1a499a48685f442efc9e5c21b5d8", null ],
                [ "EncryptionMode", "classPackuru_1_1Core_1_1ArchivingDialogControllerType.html#a1d1cfd8ffb84e947f82999c682b666a7a4b438bc1796e5bde74cd10b3895f265d", null ],
                [ "FilePaths", "classPackuru_1_1Core_1_1ArchivingDialogControllerType.html#a1d1cfd8ffb84e947f82999c682b666a7a98b2f3064b727468dedc00c5b68aca6e", null ],
                [ "InternalPath", "classPackuru_1_1Core_1_1ArchivingDialogControllerType.html#a1d1cfd8ffb84e947f82999c682b666a7a2fdb8a5feb2bdffba4f35a5b601670e7", null ],
                [ "OpenDestinationPath", "classPackuru_1_1Core_1_1ArchivingDialogControllerType.html#a1d1cfd8ffb84e947f82999c682b666a7aea14bd4df8a5944d84b7a791925ba657", null ],
                [ "OpenNewArchive", "classPackuru_1_1Core_1_1ArchivingDialogControllerType.html#a1d1cfd8ffb84e947f82999c682b666a7a2de74dbd1043cb443f61be98d4f68738", null ],
                [ "Password", "classPackuru_1_1Core_1_1ArchivingDialogControllerType.html#a1d1cfd8ffb84e947f82999c682b666a7adc647eb65e6711e155375218212b3964", null ],
                [ "PasswordRepeat", "classPackuru_1_1Core_1_1ArchivingDialogControllerType.html#a1d1cfd8ffb84e947f82999c682b666a7a093f1396f280ae5793bc81532f825091", null ],
                [ "PasswordMatch", "classPackuru_1_1Core_1_1ArchivingDialogControllerType.html#a1d1cfd8ffb84e947f82999c682b666a7af5110490888aa1dff5e89a6769fdb47d", null ],
                [ "RunInQueue", "classPackuru_1_1Core_1_1ArchivingDialogControllerType.html#a1d1cfd8ffb84e947f82999c682b666a7a3e2ffba79e6d032b1c075e396fc70e1b", null ],
                [ "SplitPreset", "classPackuru_1_1Core_1_1ArchivingDialogControllerType.html#a1d1cfd8ffb84e947f82999c682b666a7af0eaac979794a786afcf830dbeb4f086", null ],
                [ "SplitSize", "classPackuru_1_1Core_1_1ArchivingDialogControllerType.html#a1d1cfd8ffb84e947f82999c682b666a7af1ac652d05668b4a383f400f16b5eaa3", null ]
              ] ]
            ] ],
            [ "ArchivingDialogCore", "classPackuru_1_1Core_1_1ArchivingDialogCore.html", [
              [ "DialogMode", "classPackuru_1_1Core_1_1ArchivingDialogCore.html#a8417b7c0317799c868a2a7408772a902", [
                [ "AddToArchive", "classPackuru_1_1Core_1_1ArchivingDialogCore.html#a8417b7c0317799c868a2a7408772a902abd948d4b5876db7c52d0c20f223066c4", null ],
                [ "CreateArchive", "classPackuru_1_1Core_1_1ArchivingDialogCore.html#a8417b7c0317799c868a2a7408772a902ab29548b720c9f8e251b601e5ccd410e0", null ]
              ] ],
              [ "UserString", "classPackuru_1_1Core_1_1ArchivingDialogCore.html#aae24373b6c0bbb21fcd8a1cf051e29af", [
                [ "___INVALID", "classPackuru_1_1Core_1_1ArchivingDialogCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa6e545caba6d4293f6c360bc2e183bd64", null ],
                [ "AddFiles", "classPackuru_1_1Core_1_1ArchivingDialogCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa0131f74030ee83e5eb048bc98acf9766", null ],
                [ "AddFolder", "classPackuru_1_1Core_1_1ArchivingDialogCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa95dd7e29ef3db1c28d8243a3e162aa05", null ],
                [ "Cancel", "classPackuru_1_1Core_1_1ArchivingDialogCore.html#aae24373b6c0bbb21fcd8a1cf051e29afaea4788705e6873b424c65e91c2846b19", null ],
                [ "Compression", "classPackuru_1_1Core_1_1ArchivingDialogCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa82af841589057aa8922b1ac3bb4a28a4", null ],
                [ "DialogTitleAdd", "classPackuru_1_1Core_1_1ArchivingDialogCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa05f50e62b13812a19e1f48b24ff04477", null ],
                [ "DialogTitleCreate", "classPackuru_1_1Core_1_1ArchivingDialogCore.html#aae24373b6c0bbb21fcd8a1cf051e29afaab8d9182f5d50b1563e6a38add044fb4", null ],
                [ "Encryption", "classPackuru_1_1Core_1_1ArchivingDialogCore.html#aae24373b6c0bbb21fcd8a1cf051e29afad7f2615c71a1567cc13cf3a7f7de0aea", null ],
                [ "FilePaths", "classPackuru_1_1Core_1_1ArchivingDialogCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa98b2f3064b727468dedc00c5b68aca6e", null ],
                [ "Miscellaneous", "classPackuru_1_1Core_1_1ArchivingDialogCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa387baf0199e7c9cc944fae94e96448fa", null ],
                [ "OnCompletion", "classPackuru_1_1Core_1_1ArchivingDialogCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa36471358c6eea276df595fadd18bfd73", null ],
                [ "PluginSettings", "classPackuru_1_1Core_1_1ArchivingDialogCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa5760bbc99f5c7588181e204c9c048bca", null ],
                [ "RemoveItems", "classPackuru_1_1Core_1_1ArchivingDialogCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa8379408817affe864aa65a4d49abe17a", null ],
                [ "TabAdvanced", "classPackuru_1_1Core_1_1ArchivingDialogCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa8d8d56a5b410e2157c6807a3ce614268", null ],
                [ "TabArchive", "classPackuru_1_1Core_1_1ArchivingDialogCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa6833c6998e53b722728c4ae3093ce44c", null ],
                [ "TabFiles", "classPackuru_1_1Core_1_1ArchivingDialogCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa018d63b95f1dea27eb8fc788bf598c28", null ],
                [ "TabOptions", "classPackuru_1_1Core_1_1ArchivingDialogCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa1fd2d79c4a6ec415bed13956dcddd7f1", null ]
              ] ],
              [ "ArchivingDialogCore", "classPackuru_1_1Core_1_1ArchivingDialogCore.html#a4979ee466d3da022c93e11f47097b2a1", null ],
              [ "ArchivingDialogCore", "classPackuru_1_1Core_1_1ArchivingDialogCore.html#a460a9fd0c2a4ed030194589742a1a52c", null ],
              [ "~ArchivingDialogCore", "classPackuru_1_1Core_1_1ArchivingDialogCore.html#a7e8b848e74a089601d8a80d5a35c1247", null ],
              [ "accept", "classPackuru_1_1Core_1_1ArchivingDialogCore.html#a74ddbe2ce21e10cdd5e4586e2e9f0607", null ],
              [ "addItems", "classPackuru_1_1Core_1_1ArchivingDialogCore.html#aa269edce7d5c65601ea9bd10a5fc7780", null ],
              [ "destroyLater", "classPackuru_1_1Core_1_1ArchivingDialogCore.html#aa5f97b4f6309ac39ce270d0ac0f92d50", null ],
              [ "getControllerEngine", "classPackuru_1_1Core_1_1ArchivingDialogCore.html#a5bf5d17149c5959300fb3105bb540ef2", null ],
              [ "getFileModel", "classPackuru_1_1Core_1_1ArchivingDialogCore.html#a10b8d378b527f8fdc06e4398728cabe0", null ],
              [ "getMode", "classPackuru_1_1Core_1_1ArchivingDialogCore.html#aa3e80e215041cf91f33eb042be7e3d64", null ],
              [ "getPluginRowMapper", "classPackuru_1_1Core_1_1ArchivingDialogCore.html#a9f1f14e291131acf28efa5585916cd01", null ],
              [ "pluginRowMapperChanged", "classPackuru_1_1Core_1_1ArchivingDialogCore.html#aa80e5ab2fac436340c88844ebdb1077e", null ],
              [ "removeItems", "classPackuru_1_1Core_1_1ArchivingDialogCore.html#ac88c91d05f2085b82deb3d551cbc119a", null ]
            ] ],
            [ "CommonStrings", "classPackuru_1_1Core_1_1CommonStrings.html", [
              [ "UserString", "classPackuru_1_1Core_1_1CommonStrings.html#aae24373b6c0bbb21fcd8a1cf051e29af", [
                [ "___INVALID", "classPackuru_1_1Core_1_1CommonStrings.html#aae24373b6c0bbb21fcd8a1cf051e29afa6e545caba6d4293f6c360bc2e183bd64", null ],
                [ "Add", "classPackuru_1_1Core_1_1CommonStrings.html#aae24373b6c0bbb21fcd8a1cf051e29afaec211f7c20af43e742bf2570c3cb84f9", null ],
                [ "ArchivePropertiesDialogTitle", "classPackuru_1_1Core_1_1CommonStrings.html#aae24373b6c0bbb21fcd8a1cf051e29afa46d98332f48f582d19701549cf715132", null ],
                [ "Back", "classPackuru_1_1Core_1_1CommonStrings.html#aae24373b6c0bbb21fcd8a1cf051e29afa0557fa923dcee4d0f86b1409f5c2167f", null ],
                [ "Cancel", "classPackuru_1_1Core_1_1CommonStrings.html#aae24373b6c0bbb21fcd8a1cf051e29afaea4788705e6873b424c65e91c2846b19", null ],
                [ "Close", "classPackuru_1_1Core_1_1CommonStrings.html#aae24373b6c0bbb21fcd8a1cf051e29afad3d2e617335f08df83599665eef8a418", null ],
                [ "CommandLineErrorDialogTitle", "classPackuru_1_1Core_1_1CommonStrings.html#aae24373b6c0bbb21fcd8a1cf051e29afae875c1781065da1c75741f84510b8f25", null ],
                [ "CommandLineHelpDialogTitle", "classPackuru_1_1Core_1_1CommonStrings.html#aae24373b6c0bbb21fcd8a1cf051e29afa81a693742e6ac80f0bb7c7b58f04042e", null ],
                [ "Comment", "classPackuru_1_1Core_1_1CommonStrings.html#aae24373b6c0bbb21fcd8a1cf051e29afa0be8406951cdfda82f00f79328cf4efc", null ],
                [ "ErrorLogDialogTitle", "classPackuru_1_1Core_1_1CommonStrings.html#aae24373b6c0bbb21fcd8a1cf051e29afa2cd7ba6a0f23a822c4d22bacd7c72f14", null ],
                [ "Remove", "classPackuru_1_1Core_1_1CommonStrings.html#aae24373b6c0bbb21fcd8a1cf051e29afa1063e38cb53d94d386f21227fcd84717", null ]
              ] ],
              [ "CommonStrings", "classPackuru_1_1Core_1_1CommonStrings.html#aaf16f2e31ccf3d0f2dd9c21550020394", null ]
            ] ],
            [ "CreationNameErrorPromptCore", "classPackuru_1_1Core_1_1CreationNameErrorPromptCore.html", [
              [ "UserString", "classPackuru_1_1Core_1_1CreationNameErrorPromptCore.html#aae24373b6c0bbb21fcd8a1cf051e29af", [
                [ "___INVALID", "classPackuru_1_1Core_1_1CreationNameErrorPromptCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa6e545caba6d4293f6c360bc2e183bd64", null ],
                [ "Abort", "classPackuru_1_1Core_1_1CreationNameErrorPromptCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa727b63583e01fa2b3952dab580c84dc2", null ],
                [ "CannotCreateArchive", "classPackuru_1_1Core_1_1CreationNameErrorPromptCore.html#aae24373b6c0bbb21fcd8a1cf051e29afad05ec4ff2ec6fd1e990da25ae935fb14", null ],
                [ "DialogTitle", "classPackuru_1_1Core_1_1CreationNameErrorPromptCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa59e6edcf06044686b74fc871f4098d0e", null ],
                [ "Name", "classPackuru_1_1Core_1_1CreationNameErrorPromptCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa49ee3087348e8d44e1feda1917443987", null ],
                [ "Retry", "classPackuru_1_1Core_1_1CreationNameErrorPromptCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa6327b4e59f58137083214a1fec358855", null ],
                [ "YouCanChangeArchiveName", "classPackuru_1_1Core_1_1CreationNameErrorPromptCore.html#aae24373b6c0bbb21fcd8a1cf051e29afad5a52b8ec2eb9a3abc6214ab1c28f6e6", null ]
              ] ],
              [ "CreationNameErrorPromptCore", "classPackuru_1_1Core_1_1CreationNameErrorPromptCore.html#a499b8da30e79b8dcf0cccff0da2e870f", null ],
              [ "~CreationNameErrorPromptCore", "classPackuru_1_1Core_1_1CreationNameErrorPromptCore.html#af45337ab9fe72d2d9595e5b6c3f89371", null ],
              [ "abort", "classPackuru_1_1Core_1_1CreationNameErrorPromptCore.html#a220fbc82bcea18cde1d84500470b38d1", null ],
              [ "archiveName", "classPackuru_1_1Core_1_1CreationNameErrorPromptCore.html#a0bbca4ecbfc6610cb12632e7dc10af6f", null ],
              [ "deleted", "classPackuru_1_1Core_1_1CreationNameErrorPromptCore.html#ad98350fa0941ca1e625dac12428429fa", null ],
              [ "errorInfo", "classPackuru_1_1Core_1_1CreationNameErrorPromptCore.html#ad63200b3e696d3a2fb7d4c8724e5299b", null ],
              [ "retry", "classPackuru_1_1Core_1_1CreationNameErrorPromptCore.html#aaed64d49cf1893b4d15f4011a498e99d", null ]
            ] ],
            [ "ExtractionDialogControllerType", "classPackuru_1_1Core_1_1ExtractionDialogControllerType.html", [
              [ "Type", "classPackuru_1_1Core_1_1ExtractionDialogControllerType.html#a1d1cfd8ffb84e947f82999c682b666a7", [
                [ "AcceptButton", "classPackuru_1_1Core_1_1ExtractionDialogControllerType.html#a1d1cfd8ffb84e947f82999c682b666a7a54d40f47d348115168307bd4c1cdada0", null ],
                [ "DestinationError", "classPackuru_1_1Core_1_1ExtractionDialogControllerType.html#a1d1cfd8ffb84e947f82999c682b666a7ad1c9d421338dc8177762b1c34d180543", null ],
                [ "DestinationMode", "classPackuru_1_1Core_1_1ExtractionDialogControllerType.html#a1d1cfd8ffb84e947f82999c682b666a7a74e856f5a8e178cf38ae4e7c58bc838d", null ],
                [ "DestinationPath", "classPackuru_1_1Core_1_1ExtractionDialogControllerType.html#a1d1cfd8ffb84e947f82999c682b666a7aed14a1948af1b4acdadbdaefd50722a6", null ],
                [ "DialogErrors", "classPackuru_1_1Core_1_1ExtractionDialogControllerType.html#a1d1cfd8ffb84e947f82999c682b666a7a8445140d1b04381ede8375dc9e157ed1", null ],
                [ "FileExtractionMode", "classPackuru_1_1Core_1_1ExtractionDialogControllerType.html#a1d1cfd8ffb84e947f82999c682b666a7a8e39c58b8fbfa7bf0b4a43068a48629c", null ],
                [ "FileOverwriteMode", "classPackuru_1_1Core_1_1ExtractionDialogControllerType.html#a1d1cfd8ffb84e947f82999c682b666a7a69fce6d24ca64a795dbdbc6045618d2b", null ],
                [ "FolderOverwriteMode", "classPackuru_1_1Core_1_1ExtractionDialogControllerType.html#a1d1cfd8ffb84e947f82999c682b666a7a7e06140e991100abfa0b4d1cb3805466", null ],
                [ "OpenDestinationOnCompletion", "classPackuru_1_1Core_1_1ExtractionDialogControllerType.html#a1d1cfd8ffb84e947f82999c682b666a7a1d522bbe436073c08e2febff71551110", null ],
                [ "Password", "classPackuru_1_1Core_1_1ExtractionDialogControllerType.html#a1d1cfd8ffb84e947f82999c682b666a7adc647eb65e6711e155375218212b3964", null ],
                [ "RunInQueue", "classPackuru_1_1Core_1_1ExtractionDialogControllerType.html#a1d1cfd8ffb84e947f82999c682b666a7a3e2ffba79e6d032b1c075e396fc70e1b", null ],
                [ "TopFolderMode", "classPackuru_1_1Core_1_1ExtractionDialogControllerType.html#a1d1cfd8ffb84e947f82999c682b666a7a584a7f7e54fbbcd91de2e8e5b74f65ed", null ]
              ] ]
            ] ],
            [ "ExtractionDialogCore", "classPackuru_1_1Core_1_1ExtractionDialogCore.html", [
              [ "DialogMode", "classPackuru_1_1Core_1_1ExtractionDialogCore.html#a8417b7c0317799c868a2a7408772a902", [
                [ "SingleArchive", "classPackuru_1_1Core_1_1ExtractionDialogCore.html#a8417b7c0317799c868a2a7408772a902a2e030f3064798bd2bb442ee4bb0fe350", null ],
                [ "MultiArchive", "classPackuru_1_1Core_1_1ExtractionDialogCore.html#a8417b7c0317799c868a2a7408772a902a1320a01fdbb9d966b73060c49796e01c", null ]
              ] ],
              [ "UserString", "classPackuru_1_1Core_1_1ExtractionDialogCore.html#aae24373b6c0bbb21fcd8a1cf051e29af", [
                [ "___INVALID", "classPackuru_1_1Core_1_1ExtractionDialogCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa6e545caba6d4293f6c360bc2e183bd64", null ],
                [ "Cancel", "classPackuru_1_1Core_1_1ExtractionDialogCore.html#aae24373b6c0bbb21fcd8a1cf051e29afaea4788705e6873b424c65e91c2846b19", null ],
                [ "DialogTitleMulti", "classPackuru_1_1Core_1_1ExtractionDialogCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa16506c7dcca0f1f0eea20e2cf5f85ad8", null ],
                [ "DialogTitleSingle", "classPackuru_1_1Core_1_1ExtractionDialogCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa540a44eb092d329e7d257535929567cb", null ],
                [ "TabArchives", "classPackuru_1_1Core_1_1ExtractionDialogCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa20aaffe14760557d9f0a961fafbf63fc", null ],
                [ "TabOptions", "classPackuru_1_1Core_1_1ExtractionDialogCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa1fd2d79c4a6ec415bed13956dcddd7f1", null ]
              ] ],
              [ "ExtractionDialogCore", "classPackuru_1_1Core_1_1ExtractionDialogCore.html#a4aaac96c705525ec330f6daa3e5b92c0", null ],
              [ "ExtractionDialogCore", "classPackuru_1_1Core_1_1ExtractionDialogCore.html#a7cb16aed93f0e340cbdcb0909f04570b", null ],
              [ "~ExtractionDialogCore", "classPackuru_1_1Core_1_1ExtractionDialogCore.html#a0273454246008ee64c540bae1e421b29", null ],
              [ "accept", "classPackuru_1_1Core_1_1ExtractionDialogCore.html#a74ddbe2ce21e10cdd5e4586e2e9f0607", null ],
              [ "addArchives", "classPackuru_1_1Core_1_1ExtractionDialogCore.html#a756dcb1779d10a07294b22743d02aa99", null ],
              [ "destroyLater", "classPackuru_1_1Core_1_1ExtractionDialogCore.html#aa5f97b4f6309ac39ce270d0ac0f92d50", null ],
              [ "getArchivesModel", "classPackuru_1_1Core_1_1ExtractionDialogCore.html#aeb9784dda34852ba332ca3f01244dda5", null ],
              [ "getControllerEngine", "classPackuru_1_1Core_1_1ExtractionDialogCore.html#a72d2d5dda34983ff5afcb6039a09bdbb", null ],
              [ "getMode", "classPackuru_1_1Core_1_1ExtractionDialogCore.html#aa3e80e215041cf91f33eb042be7e3d64", null ],
              [ "removeArchives", "classPackuru_1_1Core_1_1ExtractionDialogCore.html#a91e9284022ca279baa68d5784dbffee8", null ]
            ] ],
            [ "ExtractionFileOverwritePromptCore", "classPackuru_1_1Core_1_1ExtractionFileOverwritePromptCore.html", [
              [ "FileType", "classPackuru_1_1Core_1_1ExtractionFileOverwritePromptCore.html#a2c794c5c13ab4dd7e65bad031dbe41c3", [
                [ "File", "classPackuru_1_1Core_1_1ExtractionFileOverwritePromptCore.html#a2c794c5c13ab4dd7e65bad031dbe41c3a0b27918290ff5323bea1e3b78a9cf04e", null ],
                [ "Link", "classPackuru_1_1Core_1_1ExtractionFileOverwritePromptCore.html#a2c794c5c13ab4dd7e65bad031dbe41c3a97e7c9a7d06eac006a28bf05467fcc8b", null ]
              ] ],
              [ "UserString", "classPackuru_1_1Core_1_1ExtractionFileOverwritePromptCore.html#aae24373b6c0bbb21fcd8a1cf051e29af", [
                [ "___INVALID", "classPackuru_1_1Core_1_1ExtractionFileOverwritePromptCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa6e545caba6d4293f6c360bc2e183bd64", null ],
                [ "Abort", "classPackuru_1_1Core_1_1ExtractionFileOverwritePromptCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa727b63583e01fa2b3952dab580c84dc2", null ],
                [ "ApplyToAll", "classPackuru_1_1Core_1_1ExtractionFileOverwritePromptCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa39d3ee11834e3e8833582218907a9cfe", null ],
                [ "Destination", "classPackuru_1_1Core_1_1ExtractionFileOverwritePromptCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa12007e1d59f4d09c87dbe2c438256244", null ],
                [ "File", "classPackuru_1_1Core_1_1ExtractionFileOverwritePromptCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa0b27918290ff5323bea1e3b78a9cf04e", null ],
                [ "FileAlreadyExists", "classPackuru_1_1Core_1_1ExtractionFileOverwritePromptCore.html#aae24373b6c0bbb21fcd8a1cf051e29afacf25d5cc7db64ddc7b38564b7704cca2", null ],
                [ "Location", "classPackuru_1_1Core_1_1ExtractionFileOverwritePromptCore.html#aae24373b6c0bbb21fcd8a1cf051e29aface5bf551379459c1c61d2a204061c455", null ],
                [ "Modified", "classPackuru_1_1Core_1_1ExtractionFileOverwritePromptCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa35e0c8c0b180c95d4e122e55ed62cc64", null ],
                [ "Overwrite", "classPackuru_1_1Core_1_1ExtractionFileOverwritePromptCore.html#aae24373b6c0bbb21fcd8a1cf051e29afada364eb37e143f6b2b5559aa03f5913a", null ],
                [ "Retry", "classPackuru_1_1Core_1_1ExtractionFileOverwritePromptCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa6327b4e59f58137083214a1fec358855", null ],
                [ "Size", "classPackuru_1_1Core_1_1ExtractionFileOverwritePromptCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa6f6cb72d544962fa333e2e34ce64f719", null ],
                [ "Skip", "classPackuru_1_1Core_1_1ExtractionFileOverwritePromptCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa72ef2b9b6965d078e3c7f95487a82d1c", null ],
                [ "Source", "classPackuru_1_1Core_1_1ExtractionFileOverwritePromptCore.html#aae24373b6c0bbb21fcd8a1cf051e29afaf31bbdd1b3e85bccd652680e16935819", null ]
              ] ],
              [ "ExtractionFileOverwritePromptCore", "classPackuru_1_1Core_1_1ExtractionFileOverwritePromptCore.html#a1790e7849ed188369f6b05de3a062e54", null ],
              [ "~ExtractionFileOverwritePromptCore", "classPackuru_1_1Core_1_1ExtractionFileOverwritePromptCore.html#af151c2366dd6a539ca9b4178d5cc94d8", null ],
              [ "abort", "classPackuru_1_1Core_1_1ExtractionFileOverwritePromptCore.html#a220fbc82bcea18cde1d84500470b38d1", null ],
              [ "deleted", "classPackuru_1_1Core_1_1ExtractionFileOverwritePromptCore.html#ad98350fa0941ca1e625dac12428429fa", null ],
              [ "destinationFilePath", "classPackuru_1_1Core_1_1ExtractionFileOverwritePromptCore.html#a7139775b1887bc6be55fd737d3c1bd14", null ],
              [ "destinationLocation", "classPackuru_1_1Core_1_1ExtractionFileOverwritePromptCore.html#a8483b0cda34e6bde49ab3cf55d6d3d29", null ],
              [ "destinationModifiedDate", "classPackuru_1_1Core_1_1ExtractionFileOverwritePromptCore.html#ada58e924774230265164e8db55301f3e", null ],
              [ "destinationName", "classPackuru_1_1Core_1_1ExtractionFileOverwritePromptCore.html#afdb5c8bb11e7b8b05c5a2c78504b51cc", null ],
              [ "destinationSize", "classPackuru_1_1Core_1_1ExtractionFileOverwritePromptCore.html#ad1e0c444698b4f47f6a96610985ea7f2", null ],
              [ "getFileType", "classPackuru_1_1Core_1_1ExtractionFileOverwritePromptCore.html#aa08b9aefd026655793c9a03bd71a0168", null ],
              [ "overwrite", "classPackuru_1_1Core_1_1ExtractionFileOverwritePromptCore.html#ab6407f13a2e84ea02d58eb2f994b43be", null ],
              [ "retry", "classPackuru_1_1Core_1_1ExtractionFileOverwritePromptCore.html#a5619747c1e923b27c794f221811ef88a", null ],
              [ "skip", "classPackuru_1_1Core_1_1ExtractionFileOverwritePromptCore.html#a57aea69559b0ab43d059bedd678ed26a", null ],
              [ "sourceFilePath", "classPackuru_1_1Core_1_1ExtractionFileOverwritePromptCore.html#a3e6b6370313cc08e202e103bba9357a2", null ],
              [ "sourceLocation", "classPackuru_1_1Core_1_1ExtractionFileOverwritePromptCore.html#a38e20f4a8dd200f913e18e817cfd2055", null ],
              [ "sourceModifiedDate", "classPackuru_1_1Core_1_1ExtractionFileOverwritePromptCore.html#a65b3c5873b1d423a8693309718001652", null ],
              [ "sourceName", "classPackuru_1_1Core_1_1ExtractionFileOverwritePromptCore.html#a288a4c87c7f419c4ea75b628c99889b6", null ],
              [ "sourceSize", "classPackuru_1_1Core_1_1ExtractionFileOverwritePromptCore.html#ad4a75a8748312944d1651ffffaab9c6f", null ]
            ] ],
            [ "ExtractionFolderOverwritePromptCore", "classPackuru_1_1Core_1_1ExtractionFolderOverwritePromptCore.html", [
              [ "UserString", "classPackuru_1_1Core_1_1ExtractionFolderOverwritePromptCore.html#aae24373b6c0bbb21fcd8a1cf051e29af", [
                [ "___INVALID", "classPackuru_1_1Core_1_1ExtractionFolderOverwritePromptCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa6e545caba6d4293f6c360bc2e183bd64", null ],
                [ "Abort", "classPackuru_1_1Core_1_1ExtractionFolderOverwritePromptCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa727b63583e01fa2b3952dab580c84dc2", null ],
                [ "ApplyToAll", "classPackuru_1_1Core_1_1ExtractionFolderOverwritePromptCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa39d3ee11834e3e8833582218907a9cfe", null ],
                [ "Content", "classPackuru_1_1Core_1_1ExtractionFolderOverwritePromptCore.html#aae24373b6c0bbb21fcd8a1cf051e29afaf15c1cae7882448b3fb0404682e17e61", null ],
                [ "Destination", "classPackuru_1_1Core_1_1ExtractionFolderOverwritePromptCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa12007e1d59f4d09c87dbe2c438256244", null ],
                [ "Folder", "classPackuru_1_1Core_1_1ExtractionFolderOverwritePromptCore.html#aae24373b6c0bbb21fcd8a1cf051e29afab0f2b97dc5d2b76b26e040408bb1d8af", null ],
                [ "FolderAlreadyExists", "classPackuru_1_1Core_1_1ExtractionFolderOverwritePromptCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa91cede70800e35c65360d0013e073a07", null ],
                [ "Modified", "classPackuru_1_1Core_1_1ExtractionFolderOverwritePromptCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa35e0c8c0b180c95d4e122e55ed62cc64", null ],
                [ "Retry", "classPackuru_1_1Core_1_1ExtractionFolderOverwritePromptCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa6327b4e59f58137083214a1fec358855", null ],
                [ "Skip", "classPackuru_1_1Core_1_1ExtractionFolderOverwritePromptCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa72ef2b9b6965d078e3c7f95487a82d1c", null ],
                [ "Source", "classPackuru_1_1Core_1_1ExtractionFolderOverwritePromptCore.html#aae24373b6c0bbb21fcd8a1cf051e29afaf31bbdd1b3e85bccd652680e16935819", null ],
                [ "WriteInto", "classPackuru_1_1Core_1_1ExtractionFolderOverwritePromptCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa3ca35dd9911b3ef691b5b34c0b8c769e", null ]
              ] ],
              [ "ExtractionFolderOverwritePromptCore", "classPackuru_1_1Core_1_1ExtractionFolderOverwritePromptCore.html#aa0308cc82612b72972e5cada4ed1d1e0", null ],
              [ "~ExtractionFolderOverwritePromptCore", "classPackuru_1_1Core_1_1ExtractionFolderOverwritePromptCore.html#aaaf4fb049d41d417ba21231a7206061a", null ],
              [ "abort", "classPackuru_1_1Core_1_1ExtractionFolderOverwritePromptCore.html#a220fbc82bcea18cde1d84500470b38d1", null ],
              [ "deleted", "classPackuru_1_1Core_1_1ExtractionFolderOverwritePromptCore.html#ad98350fa0941ca1e625dac12428429fa", null ],
              [ "destinationContent", "classPackuru_1_1Core_1_1ExtractionFolderOverwritePromptCore.html#af1c33facd36b5a14027d7e5487762850", null ],
              [ "destinationModifiedDate", "classPackuru_1_1Core_1_1ExtractionFolderOverwritePromptCore.html#ada58e924774230265164e8db55301f3e", null ],
              [ "destinationPath", "classPackuru_1_1Core_1_1ExtractionFolderOverwritePromptCore.html#a4faa3a09310d6a7702574b55f0eb67ec", null ],
              [ "overwrite", "classPackuru_1_1Core_1_1ExtractionFolderOverwritePromptCore.html#ab6407f13a2e84ea02d58eb2f994b43be", null ],
              [ "retry", "classPackuru_1_1Core_1_1ExtractionFolderOverwritePromptCore.html#a5619747c1e923b27c794f221811ef88a", null ],
              [ "skip", "classPackuru_1_1Core_1_1ExtractionFolderOverwritePromptCore.html#a57aea69559b0ab43d059bedd678ed26a", null ],
              [ "sourceContent", "classPackuru_1_1Core_1_1ExtractionFolderOverwritePromptCore.html#af11be36673bc61d1bfa5fbdd42a85178", null ],
              [ "sourceModifiedDate", "classPackuru_1_1Core_1_1ExtractionFolderOverwritePromptCore.html#a65b3c5873b1d423a8693309718001652", null ],
              [ "sourcePath", "classPackuru_1_1Core_1_1ExtractionFolderOverwritePromptCore.html#a5e714656f589fd04db6f11530d2a17d2", null ]
            ] ],
            [ "ExtractionTypeMismatchPromptCore", "classPackuru_1_1Core_1_1ExtractionTypeMismatchPromptCore.html", [
              [ "FileType", "classPackuru_1_1Core_1_1ExtractionTypeMismatchPromptCore.html#a2c794c5c13ab4dd7e65bad031dbe41c3", [
                [ "Dir", "classPackuru_1_1Core_1_1ExtractionTypeMismatchPromptCore.html#a2c794c5c13ab4dd7e65bad031dbe41c3a3c4e6dbf3e1df93a734d9959bac9ee94", null ],
                [ "File", "classPackuru_1_1Core_1_1ExtractionTypeMismatchPromptCore.html#a2c794c5c13ab4dd7e65bad031dbe41c3a0b27918290ff5323bea1e3b78a9cf04e", null ],
                [ "Link", "classPackuru_1_1Core_1_1ExtractionTypeMismatchPromptCore.html#a2c794c5c13ab4dd7e65bad031dbe41c3a97e7c9a7d06eac006a28bf05467fcc8b", null ],
                [ "___INVALID", "classPackuru_1_1Core_1_1ExtractionTypeMismatchPromptCore.html#a2c794c5c13ab4dd7e65bad031dbe41c3a6e545caba6d4293f6c360bc2e183bd64", null ]
              ] ],
              [ "UserString", "classPackuru_1_1Core_1_1ExtractionTypeMismatchPromptCore.html#aae24373b6c0bbb21fcd8a1cf051e29af", [
                [ "___INVALID", "classPackuru_1_1Core_1_1ExtractionTypeMismatchPromptCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa6e545caba6d4293f6c360bc2e183bd64", null ],
                [ "Abort", "classPackuru_1_1Core_1_1ExtractionTypeMismatchPromptCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa727b63583e01fa2b3952dab580c84dc2", null ],
                [ "ApplyToAll", "classPackuru_1_1Core_1_1ExtractionTypeMismatchPromptCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa39d3ee11834e3e8833582218907a9cfe", null ],
                [ "Destination", "classPackuru_1_1Core_1_1ExtractionTypeMismatchPromptCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa12007e1d59f4d09c87dbe2c438256244", null ],
                [ "File", "classPackuru_1_1Core_1_1ExtractionTypeMismatchPromptCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa0b27918290ff5323bea1e3b78a9cf04e", null ],
                [ "Folder", "classPackuru_1_1Core_1_1ExtractionTypeMismatchPromptCore.html#aae24373b6c0bbb21fcd8a1cf051e29afab0f2b97dc5d2b76b26e040408bb1d8af", null ],
                [ "ItemsTypeMismatch", "classPackuru_1_1Core_1_1ExtractionTypeMismatchPromptCore.html#aae24373b6c0bbb21fcd8a1cf051e29afae753cc4a20b69abd4eb4a81de811c918", null ],
                [ "Link", "classPackuru_1_1Core_1_1ExtractionTypeMismatchPromptCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa97e7c9a7d06eac006a28bf05467fcc8b", null ],
                [ "Location", "classPackuru_1_1Core_1_1ExtractionTypeMismatchPromptCore.html#aae24373b6c0bbb21fcd8a1cf051e29aface5bf551379459c1c61d2a204061c455", null ],
                [ "Retry", "classPackuru_1_1Core_1_1ExtractionTypeMismatchPromptCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa6327b4e59f58137083214a1fec358855", null ],
                [ "Skip", "classPackuru_1_1Core_1_1ExtractionTypeMismatchPromptCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa72ef2b9b6965d078e3c7f95487a82d1c", null ],
                [ "Source", "classPackuru_1_1Core_1_1ExtractionTypeMismatchPromptCore.html#aae24373b6c0bbb21fcd8a1cf051e29afaf31bbdd1b3e85bccd652680e16935819", null ]
              ] ],
              [ "ExtractionTypeMismatchPromptCore", "classPackuru_1_1Core_1_1ExtractionTypeMismatchPromptCore.html#aa8db73f6dcc00a05ab4498ffe16fcac1", null ],
              [ "~ExtractionTypeMismatchPromptCore", "classPackuru_1_1Core_1_1ExtractionTypeMismatchPromptCore.html#a247b2849268a25807c7dd9f7c1fa33e7", null ],
              [ "abort", "classPackuru_1_1Core_1_1ExtractionTypeMismatchPromptCore.html#a220fbc82bcea18cde1d84500470b38d1", null ],
              [ "deleted", "classPackuru_1_1Core_1_1ExtractionTypeMismatchPromptCore.html#ad98350fa0941ca1e625dac12428429fa", null ],
              [ "destinationFilePath", "classPackuru_1_1Core_1_1ExtractionTypeMismatchPromptCore.html#a7139775b1887bc6be55fd737d3c1bd14", null ],
              [ "destinationFileType", "classPackuru_1_1Core_1_1ExtractionTypeMismatchPromptCore.html#aeac8d9d7dc206efa54ec8fb6b31dd3a7", null ],
              [ "destinationPath", "classPackuru_1_1Core_1_1ExtractionTypeMismatchPromptCore.html#a4faa3a09310d6a7702574b55f0eb67ec", null ],
              [ "itemFileName", "classPackuru_1_1Core_1_1ExtractionTypeMismatchPromptCore.html#a9755baf30956b04e0764f2c50713fd03", null ],
              [ "retry", "classPackuru_1_1Core_1_1ExtractionTypeMismatchPromptCore.html#a5619747c1e923b27c794f221811ef88a", null ],
              [ "skip", "classPackuru_1_1Core_1_1ExtractionTypeMismatchPromptCore.html#a57aea69559b0ab43d059bedd678ed26a", null ],
              [ "sourceFilePath", "classPackuru_1_1Core_1_1ExtractionTypeMismatchPromptCore.html#a3e6b6370313cc08e202e103bba9357a2", null ],
              [ "sourceFileType", "classPackuru_1_1Core_1_1ExtractionTypeMismatchPromptCore.html#a1cdef08eff49325f3268909210362032", null ],
              [ "sourcePath", "classPackuru_1_1Core_1_1ExtractionTypeMismatchPromptCore.html#a5e714656f589fd04db6f11530d2a17d2", null ]
            ] ],
            [ "GlobalSettingsConverter", "classPackuru_1_1Core_1_1GlobalSettingsConverter.html", [
              [ "GlobalSettingsConverter", "classPackuru_1_1Core_1_1GlobalSettingsConverter.html#a08c7b8f45fb749fd273524c2d39c6588", null ],
              [ "~GlobalSettingsConverter", "classPackuru_1_1Core_1_1GlobalSettingsConverter.html#a84bb9d94ff372ee3a21c2c145f01480d", null ],
              [ "fromSettingsType", "classPackuru_1_1Core_1_1GlobalSettingsConverter.html#a91c6840423b56a5ed616710b0f933635", null ],
              [ "getKeyName", "classPackuru_1_1Core_1_1GlobalSettingsConverter.html#a048f67dae819e29ab9198c5d8a0cfa2c", null ],
              [ "getSupportedKeys", "classPackuru_1_1Core_1_1GlobalSettingsConverter.html#a9a602c59929fd1ad450e6e0bbed494d8", null ],
              [ "toSettingsType", "classPackuru_1_1Core_1_1GlobalSettingsConverter.html#acbb02dabb0996c149e2e8447956ec8b3", null ]
            ] ],
            [ "GlobalSettingsManager", "classPackuru_1_1Core_1_1GlobalSettingsManager.html", [
              [ "~GlobalSettingsManager", "classPackuru_1_1Core_1_1GlobalSettingsManager.html#a0b3258e1ca1bb19a9411f1e03d21b518", null ],
              [ "addCustomSettingsConverter", "classPackuru_1_1Core_1_1GlobalSettingsManager.html#a221d9bce49ce589938093f8b07f2ef33", null ],
              [ "getValue", "classPackuru_1_1Core_1_1GlobalSettingsManager.html#a0eeca80f1bf05739eab14b721e8802a5", null ],
              [ "getValueAs", "classPackuru_1_1Core_1_1GlobalSettingsManager.html#a19f3c39e32e7ba49f6cb88c71e35690e", null ],
              [ "setValue", "classPackuru_1_1Core_1_1GlobalSettingsManager.html#a306783bf351c920367fdddd834d6a9c6", null ],
              [ "setValue", "classPackuru_1_1Core_1_1GlobalSettingsManager.html#a9616971e5e9e9e4668bee6960646fb84", null ],
              [ "valueChanged", "classPackuru_1_1Core_1_1GlobalSettingsManager.html#a73b379fc8bad5c0041331b65c850cd50", null ]
            ] ],
            [ "PasswordPromptCore", "classPackuru_1_1Core_1_1PasswordPromptCore.html", [
              [ "UserString", "classPackuru_1_1Core_1_1PasswordPromptCore.html#aae24373b6c0bbb21fcd8a1cf051e29af", [
                [ "___INVALID", "classPackuru_1_1Core_1_1PasswordPromptCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa6e545caba6d4293f6c360bc2e183bd64", null ],
                [ "Abort", "classPackuru_1_1Core_1_1PasswordPromptCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa727b63583e01fa2b3952dab580c84dc2", null ],
                [ "DialogTitle", "classPackuru_1_1Core_1_1PasswordPromptCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa59e6edcf06044686b74fc871f4098d0e", null ]
              ] ],
              [ "PasswordPromptCore", "classPackuru_1_1Core_1_1PasswordPromptCore.html#a82fd99ab1a2649aaa29bc071e541a70b", null ],
              [ "~PasswordPromptCore", "classPackuru_1_1Core_1_1PasswordPromptCore.html#af6393b88faebdf1440ace7150a5057f0", null ],
              [ "abort", "classPackuru_1_1Core_1_1PasswordPromptCore.html#a220fbc82bcea18cde1d84500470b38d1", null ],
              [ "deleted", "classPackuru_1_1Core_1_1PasswordPromptCore.html#ad98350fa0941ca1e625dac12428429fa", null ],
              [ "enterPassword", "classPackuru_1_1Core_1_1PasswordPromptCore.html#a1860e648a165050eb0146aab4c49846b", null ],
              [ "getArchiveName", "classPackuru_1_1Core_1_1PasswordPromptCore.html#ac021d52a3bc4b9c507ffad1345b91526", null ]
            ] ],
            [ "PluginDataModel", "classPackuru_1_1Core_1_1PluginDataModel.html", [
              [ "PluginDataModel", "classPackuru_1_1Core_1_1PluginDataModel.html#a7878d60bbb3606f8d79e3a1f14e4537d", null ],
              [ "~PluginDataModel", "classPackuru_1_1Core_1_1PluginDataModel.html#ac89a75a36aa427ae459b4589ab46389f", null ],
              [ "data", "classPackuru_1_1Core_1_1PluginDataModel.html#ae93b915598cdbe5ae59dddf41d917a53", null ],
              [ "getDescription", "classPackuru_1_1Core_1_1PluginDataModel.html#a51c934f06ef2b903e43e8e48f4f56948", null ],
              [ "getHomepage", "classPackuru_1_1Core_1_1PluginDataModel.html#aaf5163bb163077b79b0de0219bc809b3", null ],
              [ "getPath", "classPackuru_1_1Core_1_1PluginDataModel.html#ad1377ba27eb47d8bc6c4718304a01629", null ],
              [ "rowCount", "classPackuru_1_1Core_1_1PluginDataModel.html#a01847fb0c363a027bfc99678c63c400c", null ],
              [ "supportedReadArchives", "classPackuru_1_1Core_1_1PluginDataModel.html#a0ceca5a70b6ff0841923c1e48a21c2c8", null ],
              [ "supportedWriteArchives", "classPackuru_1_1Core_1_1PluginDataModel.html#a42f672ccda01350691b5ecd96ae15c99", null ]
            ] ],
            [ "TestDialogControllerType", "classPackuru_1_1Core_1_1TestDialogControllerType.html", [
              [ "Type", "classPackuru_1_1Core_1_1TestDialogControllerType.html#a1d1cfd8ffb84e947f82999c682b666a7", [
                [ "AcceptButton", "classPackuru_1_1Core_1_1TestDialogControllerType.html#a1d1cfd8ffb84e947f82999c682b666a7a54d40f47d348115168307bd4c1cdada0", null ],
                [ "DialogErrors", "classPackuru_1_1Core_1_1TestDialogControllerType.html#a1d1cfd8ffb84e947f82999c682b666a7a8445140d1b04381ede8375dc9e157ed1", null ],
                [ "Password", "classPackuru_1_1Core_1_1TestDialogControllerType.html#a1d1cfd8ffb84e947f82999c682b666a7adc647eb65e6711e155375218212b3964", null ],
                [ "RunInQueue", "classPackuru_1_1Core_1_1TestDialogControllerType.html#a1d1cfd8ffb84e947f82999c682b666a7a3e2ffba79e6d032b1c075e396fc70e1b", null ]
              ] ]
            ] ],
            [ "TestDialogCore", "classPackuru_1_1Core_1_1TestDialogCore.html", [
              [ "DialogMode", "classPackuru_1_1Core_1_1TestDialogCore.html#a8417b7c0317799c868a2a7408772a902", [
                [ "SingleArchive", "classPackuru_1_1Core_1_1TestDialogCore.html#a8417b7c0317799c868a2a7408772a902a2e030f3064798bd2bb442ee4bb0fe350", null ],
                [ "MultiArchive", "classPackuru_1_1Core_1_1TestDialogCore.html#a8417b7c0317799c868a2a7408772a902a1320a01fdbb9d966b73060c49796e01c", null ]
              ] ],
              [ "UserString", "classPackuru_1_1Core_1_1TestDialogCore.html#aae24373b6c0bbb21fcd8a1cf051e29af", [
                [ "___INVALID", "classPackuru_1_1Core_1_1TestDialogCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa6e545caba6d4293f6c360bc2e183bd64", null ],
                [ "Cancel", "classPackuru_1_1Core_1_1TestDialogCore.html#aae24373b6c0bbb21fcd8a1cf051e29afaea4788705e6873b424c65e91c2846b19", null ],
                [ "DialogTitleMulti", "classPackuru_1_1Core_1_1TestDialogCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa16506c7dcca0f1f0eea20e2cf5f85ad8", null ],
                [ "DialogTitleSingle", "classPackuru_1_1Core_1_1TestDialogCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa540a44eb092d329e7d257535929567cb", null ],
                [ "TabArchives", "classPackuru_1_1Core_1_1TestDialogCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa20aaffe14760557d9f0a961fafbf63fc", null ],
                [ "TabOptions", "classPackuru_1_1Core_1_1TestDialogCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa1fd2d79c4a6ec415bed13956dcddd7f1", null ]
              ] ],
              [ "TestDialogCore", "classPackuru_1_1Core_1_1TestDialogCore.html#a1431b55aa3d628d0546d7ce350276817", null ],
              [ "TestDialogCore", "classPackuru_1_1Core_1_1TestDialogCore.html#a00ee2bbfecbfdf39de21808765e17904", null ],
              [ "~TestDialogCore", "classPackuru_1_1Core_1_1TestDialogCore.html#ac9544a35738dc13de616ba88277d1bc2", null ],
              [ "accept", "classPackuru_1_1Core_1_1TestDialogCore.html#a74ddbe2ce21e10cdd5e4586e2e9f0607", null ],
              [ "addArchives", "classPackuru_1_1Core_1_1TestDialogCore.html#a756dcb1779d10a07294b22743d02aa99", null ],
              [ "destroyLater", "classPackuru_1_1Core_1_1TestDialogCore.html#aa5f97b4f6309ac39ce270d0ac0f92d50", null ],
              [ "getArchivesModel", "classPackuru_1_1Core_1_1TestDialogCore.html#aeb9784dda34852ba332ca3f01244dda5", null ],
              [ "getControllerEngine", "classPackuru_1_1Core_1_1TestDialogCore.html#a72d2d5dda34983ff5afcb6039a09bdbb", null ],
              [ "getMode", "classPackuru_1_1Core_1_1TestDialogCore.html#aa3e80e215041cf91f33eb042be7e3d64", null ],
              [ "removeArchives", "classPackuru_1_1Core_1_1TestDialogCore.html#a91e9284022ca279baa68d5784dbffee8", null ]
            ] ]
          ] ],
          [ "Utils", "namespacePackuru_1_1Utils.html", [
            [ "BoolFlag", "classPackuru_1_1Utils_1_1BoolFlag.html", [
              [ "BoolFlag", "classPackuru_1_1Utils_1_1BoolFlag.html#a8636cfb6b2bb04b050d5f03874336948", null ],
              [ "BoolFlag", "classPackuru_1_1Utils_1_1BoolFlag.html#a2973b4530ba874790f8eaaa537eaf430", null ],
              [ "BoolFlag", "classPackuru_1_1Utils_1_1BoolFlag.html#a2408a45308121507fb422fb1a774ba79", null ],
              [ "BoolFlag", "classPackuru_1_1Utils_1_1BoolFlag.html#ab0086fb9f00fafc86696a5506f4f51d1", null ],
              [ "BoolFlag", "classPackuru_1_1Utils_1_1BoolFlag.html#a09632ada73e1852cc6dfc8af857ed8e8", null ],
              [ "operator bool", "classPackuru_1_1Utils_1_1BoolFlag.html#a420e60e9788bfa3b52aa3aab72ea3a5e", null ],
              [ "operator=", "classPackuru_1_1Utils_1_1BoolFlag.html#a51dca6540360f0bc0992336149462508", null ],
              [ "operator=", "classPackuru_1_1Utils_1_1BoolFlag.html#a5ec88f9619a1e16f26fcf194583fb00d", null ],
              [ "operator=", "classPackuru_1_1Utils_1_1BoolFlag.html#a04a6621a4b62d0c632fc85b2a4af77d9", null ],
              [ "operator=", "classPackuru_1_1Utils_1_1BoolFlag.html#a4e733c323d08c6dfdccf7a195a37dc3b", null ],
              [ "operator=", "classPackuru_1_1Utils_1_1BoolFlag.html#af212a2f7a1fbce7c06d3ed5899a8584c", null ],
              [ "operator=", "classPackuru_1_1Utils_1_1BoolFlag.html#aa5de1b691733bc88c0e087ad146af605", null ],
              [ "reset", "classPackuru_1_1Utils_1_1BoolFlag.html#a20194325f0380eb3bbc6bc6c8b2697d9", null ],
              [ "set", "classPackuru_1_1Utils_1_1BoolFlag.html#a45b441bdebcee6e121e3b3198335b93a", null ],
              [ "value", "classPackuru_1_1Utils_1_1BoolFlag.html#ad7968d74283844a09d00ba0ad66e62e7", null ]
            ] ],
            [ "LocalHolder", "classPackuru_1_1Utils_1_1LocalHolder.html", [
              [ "LocalHolder", "classPackuru_1_1Utils_1_1LocalHolder.html#af4daebaf278a10aced73d8ea4bfba2f9", null ],
              [ "LocalHolder", "classPackuru_1_1Utils_1_1LocalHolder.html#a3197a2b9a7deecf628d2d76c602d3e47", null ],
              [ "~LocalHolder", "classPackuru_1_1Utils_1_1LocalHolder.html#a60aa04cc97acea53c7b4ba0e4aa7befb", null ],
              [ "operator=", "classPackuru_1_1Utils_1_1LocalHolder.html#a44907613a454f2ad55f19012980451e1", null ]
            ] ],
            [ "ScopeGuard", "classPackuru_1_1Utils_1_1ScopeGuard.html", [
              [ "ScopeGuard", "classPackuru_1_1Utils_1_1ScopeGuard.html#ad79d98631880e951f4dee8492db6916a", null ],
              [ "~ScopeGuard", "classPackuru_1_1Utils_1_1ScopeGuard.html#a9b2dd2382e7d9acf1de52a0b8b3cc31f", null ],
              [ "createScopeGuard", "classPackuru_1_1Utils_1_1ScopeGuard.html#a8cdf138e22cb0767044c09275e343ac7", null ]
            ] ]
          ] ]
        ] ],
        [ "qcs", null, [
          [ "qqc", "namespaceqcs_1_1qqc.html", [
            [ "WidgetMapper", "classqcs_1_1qqc_1_1WidgetMapper.html", [
              [ "WidgetMapper", "classqcs_1_1qqc_1_1WidgetMapper.html#a9b0177f079b738b0f2aaba253eddd23d", null ],
              [ "~WidgetMapper", "classqcs_1_1qqc_1_1WidgetMapper.html#ad93e39b2d30a0cfc06cf80b121f7e5ab", null ],
              [ "addBuddies", "classqcs_1_1qqc_1_1WidgetMapper.html#a7d6b74e7969284f27cdec0ce6ae742ea", null ],
              [ "addBuddy", "classqcs_1_1qqc_1_1WidgetMapper.html#a37039bc8bc59f65dd5a2ef2d6fb4302d", null ],
              [ "addLabelAndWidget", "classqcs_1_1qqc_1_1WidgetMapper.html#aa87f9c30335f1fc97aceec4c50ff3581", null ],
              [ "addLabelsAndWidgets", "classqcs_1_1qqc_1_1WidgetMapper.html#a14075d051bff67ab5e48836f4a2bc3ea", null ],
              [ "addWidget", "classqcs_1_1qqc_1_1WidgetMapper.html#afe6c0382962275f2e0e13e2e8629e514", null ],
              [ "addWidgets", "classqcs_1_1qqc_1_1WidgetMapper.html#a1e58fdc0752d665640a2e8a35ba06227", null ],
              [ "engineChanged", "classqcs_1_1qqc_1_1WidgetMapper.html#abaa2d1b32c38e73e0475d17aa1a8598f", null ],
              [ "getEngine", "classqcs_1_1qqc_1_1WidgetMapper.html#a3c8502b9a1868108b52ecc8219d7e21c", null ],
              [ "isSetEnabled", "classqcs_1_1qqc_1_1WidgetMapper.html#aff83ec3d43c867715a80fd504e2fae47", null ],
              [ "isSetVisible", "classqcs_1_1qqc_1_1WidgetMapper.html#a4cfe5dca429f87b4a4c0a508250abd70", null ],
              [ "setEngine", "classqcs_1_1qqc_1_1WidgetMapper.html#a84d834f8316894826925a81e079c71fb", null ],
              [ "updateWidgets", "classqcs_1_1qqc_1_1WidgetMapper.html#a30d5b448354c8c816ad12daa62e2e235", null ],
              [ "engine", "classqcs_1_1qqc_1_1WidgetMapper.html#aa27c5fe3624f4c6704d3b61f8eb7ee5f", null ]
            ] ]
          ] ],
          [ "qtw", "namespaceqcs_1_1qtw.html", [
            [ "GeneratedPage", "classqcs_1_1qtw_1_1GeneratedPage.html", [
              [ "GeneratedPage", "classqcs_1_1qtw_1_1GeneratedPage.html#a1d6df641d2efce68c402b96e5a8c0bec", null ],
              [ "~GeneratedPage", "classqcs_1_1qtw_1_1GeneratedPage.html#a82197093b819166f96f186590bbf6bd4", null ]
            ] ],
            [ "PageStack", "classqcs_1_1qtw_1_1PageStack.html", [
              [ "PageStack", "classqcs_1_1qtw_1_1PageStack.html#a478c559255779f27eac7bd342fb41887", null ],
              [ "~PageStack", "classqcs_1_1qtw_1_1PageStack.html#a65a4fd1db8d23ac2c6e7aa4c04c1696c", null ],
              [ "setCurrentPage", "classqcs_1_1qtw_1_1PageStack.html#a6900138f0202107611abee493a632963", null ]
            ] ],
            [ "WidgetMapper", "classqcs_1_1qtw_1_1WidgetMapper.html", [
              [ "WidgetMapper", "classqcs_1_1qtw_1_1WidgetMapper.html#a9b0177f079b738b0f2aaba253eddd23d", null ],
              [ "~WidgetMapper", "classqcs_1_1qtw_1_1WidgetMapper.html#ad8a0c124635806902e445d7b07cddded", null ],
              [ "addBuddies", "classqcs_1_1qtw_1_1WidgetMapper.html#aff91a259bfd73482d7a9da8282f95869", null ],
              [ "addLabelAndWidget", "classqcs_1_1qtw_1_1WidgetMapper.html#a8297e9e73b01ed427f3650efc94dd12a", null ],
              [ "addLabelsAndWidgets", "classqcs_1_1qtw_1_1WidgetMapper.html#a0acc67ce76e581a795cd079418cf6f14", null ],
              [ "addWidgets", "classqcs_1_1qtw_1_1WidgetMapper.html#ab449b05688414cbf1b7a56f8fd51ad20", null ],
              [ "isSetEnabled", "classqcs_1_1qtw_1_1WidgetMapper.html#a7819c308e789d5e548ee131cd27009c9", null ],
              [ "isSetVisible", "classqcs_1_1qtw_1_1WidgetMapper.html#a4a1731cd0076b2f046ad8ab6c13a22e8", null ],
              [ "setEngine", "classqcs_1_1qtw_1_1WidgetMapper.html#a883d64c2e020cc95988fc08baa2821c5", null ],
              [ "updateWidgets", "classqcs_1_1qtw_1_1WidgetMapper.html#a6ad1c3af6faad7b7844789affa08e748", null ]
            ] ]
          ] ]
        ] ],
        [ "GeneratedPage", "classGeneratedPage.html", [
          [ "rowMapper", "classGeneratedPage.html#a8c62445461f31386b574d241547b2358", null ]
        ] ],
        [ "PageStack", "classPageStack.html", [
          [ "setCurrentPage", "classPageStack.html#ab9eac3ff7a171f180fd073e5f303dfc3", null ]
        ] ]
      ] ],
      [ "Class Index", "classes.html", null ],
      [ "Class Members", "functions_all.html", [
        [ "All", "functions_all_a.html", [
          [ "a", "functions_all_a.html", null ],
          [ "b", "functions_all_b.html", null ],
          [ "c", "functions_all_c.html", null ],
          [ "d", "functions_all_d.html", null ],
          [ "e", "functions_all_e.html", null ],
          [ "f", "functions_all_f.html", null ],
          [ "g", "functions_all_g.html", null ],
          [ "h", "functions_all_h.html", null ],
          [ "i", "functions_all_i.html", null ],
          [ "l", "functions_all_l.html", null ],
          [ "m", "functions_all_m.html", null ],
          [ "n", "functions_all_n.html", null ],
          [ "o", "functions_all_o.html", null ],
          [ "p", "functions_all_p.html", null ],
          [ "q", "functions_all_q.html", null ],
          [ "r", "functions_all_r.html", null ],
          [ "s", "functions_all_s.html", null ],
          [ "t", "functions_all_t.html", null ],
          [ "u", "functions_all_u.html", null ],
          [ "v", "functions_all_v.html", null ],
          [ "~", "functions_all_~.html", null ]
        ] ],
        [ "Functions", "functions_func_a.html", [
          [ "a", "functions_func_a.html", null ],
          [ "b", "functions_func_b.html", null ],
          [ "c", "functions_func_c.html", null ],
          [ "d", "functions_func_d.html", null ],
          [ "e", "functions_func_e.html", null ],
          [ "f", "functions_func_f.html", null ],
          [ "g", "functions_func_g.html", null ],
          [ "h", "functions_func_h.html", null ],
          [ "i", "functions_func_i.html", null ],
          [ "l", "functions_func_l.html", null ],
          [ "m", "functions_func_m.html", null ],
          [ "n", "functions_func_n.html", null ],
          [ "o", "functions_func_o.html", null ],
          [ "p", "functions_func_p.html", null ],
          [ "q", "functions_func_q.html", null ],
          [ "r", "functions_func_r.html", null ],
          [ "s", "functions_func_s.html", null ],
          [ "t", "functions_func_t.html", null ],
          [ "u", "functions_func_u.html", null ],
          [ "v", "functions_func_v.html", null ],
          [ "~", "functions_func_~.html", null ]
        ] ],
        [ "Enums", "functions_enum.html", null ],
        [ "Properties", "functions_prop.html", null ],
        [ "Related Functions", "functions_related.html", null ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", [
        [ "packuru", "dir_d41d8cd98f00b204e9800998ecf8427e.html", [
          [ "src", "dir_68267d1309a1af8e8297ef4c3efbcdba.html", [
            [ "core", "dir_aebb8dcc11953d78e620bbef0b9e2183.html", [
              [ "aboutdialogcore.h", "aboutdialogcore_8h.html", [
                [ "AboutDialogCore", "classPackuru_1_1Core_1_1AboutDialogCore.html", [
                  [ "UserString", "classPackuru_1_1Core_1_1AboutDialogCore.html#aae24373b6c0bbb21fcd8a1cf051e29af", [
                    [ "___INVALID", "classPackuru_1_1Core_1_1AboutDialogCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa6e545caba6d4293f6c360bc2e183bd64", null ],
                    [ "AppInfo", "classPackuru_1_1Core_1_1AboutDialogCore.html#aae24373b6c0bbb21fcd8a1cf051e29afad6dfc8be95fcca215d2e5cd132f9780a", null ],
                    [ "DialogTitle", "classPackuru_1_1Core_1_1AboutDialogCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa59e6edcf06044686b74fc871f4098d0e", null ]
                  ] ],
                  [ "AboutDialogCore", "classPackuru_1_1Core_1_1AboutDialogCore.html#a2414d4cdb873d32202412795e3d4cc42", null ]
                ] ]
              ] ],
              [ "appclosingdialogcore.h", "appclosingdialogcore_8h.html", [
                [ "AppClosingDialogCore", "classPackuru_1_1Core_1_1AppClosingDialogCore.html", [
                  [ "UserString", "classPackuru_1_1Core_1_1AppClosingDialogCore.html#aae24373b6c0bbb21fcd8a1cf051e29af", [
                    [ "___INVALID", "classPackuru_1_1Core_1_1AppClosingDialogCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa6e545caba6d4293f6c360bc2e183bd64", null ],
                    [ "BrowserBusyBrowsersInfo", "classPackuru_1_1Core_1_1AppClosingDialogCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa2618e2ddfa9c945c73f76b08b48c8944", null ],
                    [ "Cancel", "classPackuru_1_1Core_1_1AppClosingDialogCore.html#aae24373b6c0bbb21fcd8a1cf051e29afaea4788705e6873b424c65e91c2846b19", null ],
                    [ "DialogTitle", "classPackuru_1_1Core_1_1AppClosingDialogCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa59e6edcf06044686b74fc871f4098d0e", null ],
                    [ "QueueOpenedDialogsInfo", "classPackuru_1_1Core_1_1AppClosingDialogCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa2d73907177fa0010c69448417b49bea5", null ],
                    [ "QueueUnfinishedTasksInfo", "classPackuru_1_1Core_1_1AppClosingDialogCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa9b2fabf6db9d0458429eeca5ffd11968", null ],
                    [ "Quit", "classPackuru_1_1Core_1_1AppClosingDialogCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa0d82790b0612935992bd564a17ce37d6", null ],
                    [ "QuitQuestion", "classPackuru_1_1Core_1_1AppClosingDialogCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa66a140e4aab9ef25be947ef056087be6", null ]
                  ] ],
                  [ "AppClosingDialogCore", "classPackuru_1_1Core_1_1AppClosingDialogCore.html#acf12ecc20178dda6894af8d721156220", null ]
                ] ]
              ] ],
              [ "appinfo.h", "appinfo_8h.html", [
                [ "AppInfo", "structPackuru_1_1Core_1_1AppInfo.html", null ],
                [ "PACKURU_VERSION_MAJOR", "appinfo_8h.html#ace5cc44279627127fbc44b92ac56d20c", null ],
                [ "PACKURU_VERSION_MINOR", "appinfo_8h.html#afafecdf25ea427531b18cbbba403b2c2", null ],
                [ "PACKURU_VERSION_PATCH", "appinfo_8h.html#a819dcde57aa15731e1288d8db26e5e15", null ]
              ] ],
              [ "appsettingscontrollertype.h", "appsettingscontrollertype_8h.html", [
                [ "AppSettingsControllerType", "classPackuru_1_1Core_1_1AppSettingsControllerType.html", [
                  [ "Type", "classPackuru_1_1Core_1_1AppSettingsControllerType.html#a1d1cfd8ffb84e947f82999c682b666a7", [
                    [ "CompressionRatioDisplayMode", "classPackuru_1_1Core_1_1AppSettingsControllerType.html#a1d1cfd8ffb84e947f82999c682b666a7a425b858601f3fb84e14a684d4e73a802", null ],
                    [ "SizeUnits", "classPackuru_1_1Core_1_1AppSettingsControllerType.html#a1d1cfd8ffb84e947f82999c682b666a7aee2f01136cdca8e061e3cb95e1b2442e", null ],
                    [ "SizeUnitsExample", "classPackuru_1_1Core_1_1AppSettingsControllerType.html#a1d1cfd8ffb84e947f82999c682b666a7af6e0ffe2e9ac82969bd684ab3b717971", null ]
                  ] ]
                ] ]
              ] ],
              [ "archivetypefiltermodel.h", "archivetypefiltermodel_8h.html", [
                [ "ArchiveTypeFilterModel", "classPackuru_1_1Core_1_1ArchiveTypeFilterModel.html", [
                  [ "ArchiveTypeFilterModel", "classPackuru_1_1Core_1_1ArchiveTypeFilterModel.html#a9ed1b0dedfb7a1ba46c1e9ce7931d343", null ],
                  [ "~ArchiveTypeFilterModel", "classPackuru_1_1Core_1_1ArchiveTypeFilterModel.html#a48cb6920e55f8c37a7cd5483a4918aff", null ],
                  [ "filterAcceptsRow", "classPackuru_1_1Core_1_1ArchiveTypeFilterModel.html#a5bdf08bac5a30438e2fa64e66b7fd6f5", null ],
                  [ "getFilterFixedString", "classPackuru_1_1Core_1_1ArchiveTypeFilterModel.html#acc2dd347af2cebf5c70f2435b130811a", null ],
                  [ "filterFixedString", "classPackuru_1_1Core_1_1ArchiveTypeFilterModel.html#a64e1407a3ce0ca5bd556677359352081", null ]
                ] ]
              ] ],
              [ "archivetypemodel.h", "archivetypemodel_8h.html", [
                [ "ArchiveTypeModel", "classPackuru_1_1Core_1_1ArchiveTypeModel.html", [
                  [ "ArchiveTypeModel", "classPackuru_1_1Core_1_1ArchiveTypeModel.html#a6d13596be27c85b5af4b7b95d22ba4c7", null ],
                  [ "~ArchiveTypeModel", "classPackuru_1_1Core_1_1ArchiveTypeModel.html#abeb879485beaabd0f1c3eb5500fa0ffe", null ],
                  [ "columnCount", "classPackuru_1_1Core_1_1ArchiveTypeModel.html#a65c9695e4a2778f358da46947cac09d0", null ],
                  [ "data", "classPackuru_1_1Core_1_1ArchiveTypeModel.html#aa06564528ca791fce5d559b298edbb18", null ],
                  [ "flags", "classPackuru_1_1Core_1_1ArchiveTypeModel.html#a58221c89c3d72d2c781fa6a57c483eba", null ],
                  [ "getCurrentReadPlugin", "classPackuru_1_1Core_1_1ArchiveTypeModel.html#a4a393024b1e4c164832510e8d8b8732b", null ],
                  [ "getCurrentWritePlugin", "classPackuru_1_1Core_1_1ArchiveTypeModel.html#ab0f2344dd69c7206ba8f927ab3db5c5e", null ],
                  [ "getReadPlugins", "classPackuru_1_1Core_1_1ArchiveTypeModel.html#aaf8a722ee0dec747c55c51fa484911f2", null ],
                  [ "getValueSet", "classPackuru_1_1Core_1_1ArchiveTypeModel.html#a2fde80eaf8172522531d4da720bbaf59", null ],
                  [ "getWritePlugins", "classPackuru_1_1Core_1_1ArchiveTypeModel.html#ae3b622b2e46f875731096cca72c57677", null ],
                  [ "headerData", "classPackuru_1_1Core_1_1ArchiveTypeModel.html#a3dcf81ea724475d324f9fdd1481492ea", null ],
                  [ "rowCount", "classPackuru_1_1Core_1_1ArchiveTypeModel.html#abb3de2795579c97b3ceea401e9c9677d", null ],
                  [ "setData", "classPackuru_1_1Core_1_1ArchiveTypeModel.html#a893d1b7c0d8fc61472f7dcd4d3efa055", null ],
                  [ "setReadPlugin", "classPackuru_1_1Core_1_1ArchiveTypeModel.html#a166916df15734a476f1c34237707887b", null ],
                  [ "setWritePlugin", "classPackuru_1_1Core_1_1ArchiveTypeModel.html#a76e91012f02e9b545692d342bdc01cdd", null ]
                ] ]
              ] ],
              [ "archivingdialogcontrollertype.h", "archivingdialogcontrollertype_8h.html", [
                [ "ArchivingDialogControllerType", "classPackuru_1_1Core_1_1ArchivingDialogControllerType.html", [
                  [ "Type", "classPackuru_1_1Core_1_1ArchivingDialogControllerType.html#a1d1cfd8ffb84e947f82999c682b666a7", [
                    [ "AcceptButton", "classPackuru_1_1Core_1_1ArchivingDialogControllerType.html#a1d1cfd8ffb84e947f82999c682b666a7a54d40f47d348115168307bd4c1cdada0", null ],
                    [ "ArchiveName", "classPackuru_1_1Core_1_1ArchivingDialogControllerType.html#a1d1cfd8ffb84e947f82999c682b666a7aa11d8253c0a8329965f7d7e1a86d48e3", null ],
                    [ "ArchiveNameError", "classPackuru_1_1Core_1_1ArchivingDialogControllerType.html#a1d1cfd8ffb84e947f82999c682b666a7a405cb0a3e8ad6138b049cea9c59a8d6f", null ],
                    [ "ArchiveType", "classPackuru_1_1Core_1_1ArchivingDialogControllerType.html#a1d1cfd8ffb84e947f82999c682b666a7ad77f4250386da13d83f709b54bac6012", null ],
                    [ "ArchivingMode", "classPackuru_1_1Core_1_1ArchivingDialogControllerType.html#a1d1cfd8ffb84e947f82999c682b666a7a0ab41235d375ac2f1f4c30c3f11c2424", null ],
                    [ "CompressionLevel", "classPackuru_1_1Core_1_1ArchivingDialogControllerType.html#a1d1cfd8ffb84e947f82999c682b666a7adc2e159f049625277b8a33d51074e5bf", null ],
                    [ "CompressionState", "classPackuru_1_1Core_1_1ArchivingDialogControllerType.html#a1d1cfd8ffb84e947f82999c682b666a7ae8d3cee45bdbd55c37e9be5119368151", null ],
                    [ "DestinationMode", "classPackuru_1_1Core_1_1ArchivingDialogControllerType.html#a1d1cfd8ffb84e947f82999c682b666a7a74e856f5a8e178cf38ae4e7c58bc838d", null ],
                    [ "DestinationPath", "classPackuru_1_1Core_1_1ArchivingDialogControllerType.html#a1d1cfd8ffb84e947f82999c682b666a7aed14a1948af1b4acdadbdaefd50722a6", null ],
                    [ "DialogErrors", "classPackuru_1_1Core_1_1ArchivingDialogControllerType.html#a1d1cfd8ffb84e947f82999c682b666a7a8445140d1b04381ede8375dc9e157ed1", null ],
                    [ "EncryptionState", "classPackuru_1_1Core_1_1ArchivingDialogControllerType.html#a1d1cfd8ffb84e947f82999c682b666a7a28893619a707715e34dcdee9df69e59d", null ],
                    [ "DirectoryError", "classPackuru_1_1Core_1_1ArchivingDialogControllerType.html#a1d1cfd8ffb84e947f82999c682b666a7a6b4b1a499a48685f442efc9e5c21b5d8", null ],
                    [ "EncryptionMode", "classPackuru_1_1Core_1_1ArchivingDialogControllerType.html#a1d1cfd8ffb84e947f82999c682b666a7a4b438bc1796e5bde74cd10b3895f265d", null ],
                    [ "FilePaths", "classPackuru_1_1Core_1_1ArchivingDialogControllerType.html#a1d1cfd8ffb84e947f82999c682b666a7a98b2f3064b727468dedc00c5b68aca6e", null ],
                    [ "InternalPath", "classPackuru_1_1Core_1_1ArchivingDialogControllerType.html#a1d1cfd8ffb84e947f82999c682b666a7a2fdb8a5feb2bdffba4f35a5b601670e7", null ],
                    [ "OpenDestinationPath", "classPackuru_1_1Core_1_1ArchivingDialogControllerType.html#a1d1cfd8ffb84e947f82999c682b666a7aea14bd4df8a5944d84b7a791925ba657", null ],
                    [ "OpenNewArchive", "classPackuru_1_1Core_1_1ArchivingDialogControllerType.html#a1d1cfd8ffb84e947f82999c682b666a7a2de74dbd1043cb443f61be98d4f68738", null ],
                    [ "Password", "classPackuru_1_1Core_1_1ArchivingDialogControllerType.html#a1d1cfd8ffb84e947f82999c682b666a7adc647eb65e6711e155375218212b3964", null ],
                    [ "PasswordRepeat", "classPackuru_1_1Core_1_1ArchivingDialogControllerType.html#a1d1cfd8ffb84e947f82999c682b666a7a093f1396f280ae5793bc81532f825091", null ],
                    [ "PasswordMatch", "classPackuru_1_1Core_1_1ArchivingDialogControllerType.html#a1d1cfd8ffb84e947f82999c682b666a7af5110490888aa1dff5e89a6769fdb47d", null ],
                    [ "RunInQueue", "classPackuru_1_1Core_1_1ArchivingDialogControllerType.html#a1d1cfd8ffb84e947f82999c682b666a7a3e2ffba79e6d032b1c075e396fc70e1b", null ],
                    [ "SplitPreset", "classPackuru_1_1Core_1_1ArchivingDialogControllerType.html#a1d1cfd8ffb84e947f82999c682b666a7af0eaac979794a786afcf830dbeb4f086", null ],
                    [ "SplitSize", "classPackuru_1_1Core_1_1ArchivingDialogControllerType.html#a1d1cfd8ffb84e947f82999c682b666a7af1ac652d05668b4a383f400f16b5eaa3", null ]
                  ] ]
                ] ],
                [ "qHash", "archivingdialogcontrollertype_8h.html#a701172786fd90e6a4bf25b540371227e", null ]
              ] ],
              [ "archivingdialogcore.h", "archivingdialogcore_8h.html", [
                [ "ArchivingDialogCore", "classPackuru_1_1Core_1_1ArchivingDialogCore.html", [
                  [ "DialogMode", "classPackuru_1_1Core_1_1ArchivingDialogCore.html#a8417b7c0317799c868a2a7408772a902", [
                    [ "AddToArchive", "classPackuru_1_1Core_1_1ArchivingDialogCore.html#a8417b7c0317799c868a2a7408772a902abd948d4b5876db7c52d0c20f223066c4", null ],
                    [ "CreateArchive", "classPackuru_1_1Core_1_1ArchivingDialogCore.html#a8417b7c0317799c868a2a7408772a902ab29548b720c9f8e251b601e5ccd410e0", null ]
                  ] ],
                  [ "UserString", "classPackuru_1_1Core_1_1ArchivingDialogCore.html#aae24373b6c0bbb21fcd8a1cf051e29af", [
                    [ "___INVALID", "classPackuru_1_1Core_1_1ArchivingDialogCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa6e545caba6d4293f6c360bc2e183bd64", null ],
                    [ "AddFiles", "classPackuru_1_1Core_1_1ArchivingDialogCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa0131f74030ee83e5eb048bc98acf9766", null ],
                    [ "AddFolder", "classPackuru_1_1Core_1_1ArchivingDialogCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa95dd7e29ef3db1c28d8243a3e162aa05", null ],
                    [ "Cancel", "classPackuru_1_1Core_1_1ArchivingDialogCore.html#aae24373b6c0bbb21fcd8a1cf051e29afaea4788705e6873b424c65e91c2846b19", null ],
                    [ "Compression", "classPackuru_1_1Core_1_1ArchivingDialogCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa82af841589057aa8922b1ac3bb4a28a4", null ],
                    [ "DialogTitleAdd", "classPackuru_1_1Core_1_1ArchivingDialogCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa05f50e62b13812a19e1f48b24ff04477", null ],
                    [ "DialogTitleCreate", "classPackuru_1_1Core_1_1ArchivingDialogCore.html#aae24373b6c0bbb21fcd8a1cf051e29afaab8d9182f5d50b1563e6a38add044fb4", null ],
                    [ "Encryption", "classPackuru_1_1Core_1_1ArchivingDialogCore.html#aae24373b6c0bbb21fcd8a1cf051e29afad7f2615c71a1567cc13cf3a7f7de0aea", null ],
                    [ "FilePaths", "classPackuru_1_1Core_1_1ArchivingDialogCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa98b2f3064b727468dedc00c5b68aca6e", null ],
                    [ "Miscellaneous", "classPackuru_1_1Core_1_1ArchivingDialogCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa387baf0199e7c9cc944fae94e96448fa", null ],
                    [ "OnCompletion", "classPackuru_1_1Core_1_1ArchivingDialogCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa36471358c6eea276df595fadd18bfd73", null ],
                    [ "PluginSettings", "classPackuru_1_1Core_1_1ArchivingDialogCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa5760bbc99f5c7588181e204c9c048bca", null ],
                    [ "RemoveItems", "classPackuru_1_1Core_1_1ArchivingDialogCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa8379408817affe864aa65a4d49abe17a", null ],
                    [ "TabAdvanced", "classPackuru_1_1Core_1_1ArchivingDialogCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa8d8d56a5b410e2157c6807a3ce614268", null ],
                    [ "TabArchive", "classPackuru_1_1Core_1_1ArchivingDialogCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa6833c6998e53b722728c4ae3093ce44c", null ],
                    [ "TabFiles", "classPackuru_1_1Core_1_1ArchivingDialogCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa018d63b95f1dea27eb8fc788bf598c28", null ],
                    [ "TabOptions", "classPackuru_1_1Core_1_1ArchivingDialogCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa1fd2d79c4a6ec415bed13956dcddd7f1", null ]
                  ] ],
                  [ "ArchivingDialogCore", "classPackuru_1_1Core_1_1ArchivingDialogCore.html#a4979ee466d3da022c93e11f47097b2a1", null ],
                  [ "ArchivingDialogCore", "classPackuru_1_1Core_1_1ArchivingDialogCore.html#a460a9fd0c2a4ed030194589742a1a52c", null ],
                  [ "~ArchivingDialogCore", "classPackuru_1_1Core_1_1ArchivingDialogCore.html#a7e8b848e74a089601d8a80d5a35c1247", null ],
                  [ "accept", "classPackuru_1_1Core_1_1ArchivingDialogCore.html#a74ddbe2ce21e10cdd5e4586e2e9f0607", null ],
                  [ "addItems", "classPackuru_1_1Core_1_1ArchivingDialogCore.html#aa269edce7d5c65601ea9bd10a5fc7780", null ],
                  [ "destroyLater", "classPackuru_1_1Core_1_1ArchivingDialogCore.html#aa5f97b4f6309ac39ce270d0ac0f92d50", null ],
                  [ "getControllerEngine", "classPackuru_1_1Core_1_1ArchivingDialogCore.html#a5bf5d17149c5959300fb3105bb540ef2", null ],
                  [ "getFileModel", "classPackuru_1_1Core_1_1ArchivingDialogCore.html#a10b8d378b527f8fdc06e4398728cabe0", null ],
                  [ "getMode", "classPackuru_1_1Core_1_1ArchivingDialogCore.html#aa3e80e215041cf91f33eb042be7e3d64", null ],
                  [ "getPluginRowMapper", "classPackuru_1_1Core_1_1ArchivingDialogCore.html#a9f1f14e291131acf28efa5585916cd01", null ],
                  [ "pluginRowMapperChanged", "classPackuru_1_1Core_1_1ArchivingDialogCore.html#aa80e5ab2fac436340c88844ebdb1077e", null ],
                  [ "removeItems", "classPackuru_1_1Core_1_1ArchivingDialogCore.html#ac88c91d05f2085b82deb3d551cbc119a", null ]
                ] ]
              ] ],
              [ "commonstrings.h", "commonstrings_8h.html", [
                [ "CommonStrings", "classPackuru_1_1Core_1_1CommonStrings.html", [
                  [ "UserString", "classPackuru_1_1Core_1_1CommonStrings.html#aae24373b6c0bbb21fcd8a1cf051e29af", [
                    [ "___INVALID", "classPackuru_1_1Core_1_1CommonStrings.html#aae24373b6c0bbb21fcd8a1cf051e29afa6e545caba6d4293f6c360bc2e183bd64", null ],
                    [ "Add", "classPackuru_1_1Core_1_1CommonStrings.html#aae24373b6c0bbb21fcd8a1cf051e29afaec211f7c20af43e742bf2570c3cb84f9", null ],
                    [ "ArchivePropertiesDialogTitle", "classPackuru_1_1Core_1_1CommonStrings.html#aae24373b6c0bbb21fcd8a1cf051e29afa46d98332f48f582d19701549cf715132", null ],
                    [ "Back", "classPackuru_1_1Core_1_1CommonStrings.html#aae24373b6c0bbb21fcd8a1cf051e29afa0557fa923dcee4d0f86b1409f5c2167f", null ],
                    [ "Cancel", "classPackuru_1_1Core_1_1CommonStrings.html#aae24373b6c0bbb21fcd8a1cf051e29afaea4788705e6873b424c65e91c2846b19", null ],
                    [ "Close", "classPackuru_1_1Core_1_1CommonStrings.html#aae24373b6c0bbb21fcd8a1cf051e29afad3d2e617335f08df83599665eef8a418", null ],
                    [ "CommandLineErrorDialogTitle", "classPackuru_1_1Core_1_1CommonStrings.html#aae24373b6c0bbb21fcd8a1cf051e29afae875c1781065da1c75741f84510b8f25", null ],
                    [ "CommandLineHelpDialogTitle", "classPackuru_1_1Core_1_1CommonStrings.html#aae24373b6c0bbb21fcd8a1cf051e29afa81a693742e6ac80f0bb7c7b58f04042e", null ],
                    [ "Comment", "classPackuru_1_1Core_1_1CommonStrings.html#aae24373b6c0bbb21fcd8a1cf051e29afa0be8406951cdfda82f00f79328cf4efc", null ],
                    [ "ErrorLogDialogTitle", "classPackuru_1_1Core_1_1CommonStrings.html#aae24373b6c0bbb21fcd8a1cf051e29afa2cd7ba6a0f23a822c4d22bacd7c72f14", null ],
                    [ "Remove", "classPackuru_1_1Core_1_1CommonStrings.html#aae24373b6c0bbb21fcd8a1cf051e29afa1063e38cb53d94d386f21227fcd84717", null ]
                  ] ],
                  [ "CommonStrings", "classPackuru_1_1Core_1_1CommonStrings.html#aaf16f2e31ccf3d0f2dd9c21550020394", null ]
                ] ]
              ] ],
              [ "creationnameerrorpromptcore.h", "creationnameerrorpromptcore_8h.html", [
                [ "CreationNameErrorPromptCore", "classPackuru_1_1Core_1_1CreationNameErrorPromptCore.html", [
                  [ "UserString", "classPackuru_1_1Core_1_1CreationNameErrorPromptCore.html#aae24373b6c0bbb21fcd8a1cf051e29af", [
                    [ "___INVALID", "classPackuru_1_1Core_1_1CreationNameErrorPromptCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa6e545caba6d4293f6c360bc2e183bd64", null ],
                    [ "Abort", "classPackuru_1_1Core_1_1CreationNameErrorPromptCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa727b63583e01fa2b3952dab580c84dc2", null ],
                    [ "CannotCreateArchive", "classPackuru_1_1Core_1_1CreationNameErrorPromptCore.html#aae24373b6c0bbb21fcd8a1cf051e29afad05ec4ff2ec6fd1e990da25ae935fb14", null ],
                    [ "DialogTitle", "classPackuru_1_1Core_1_1CreationNameErrorPromptCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa59e6edcf06044686b74fc871f4098d0e", null ],
                    [ "Name", "classPackuru_1_1Core_1_1CreationNameErrorPromptCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa49ee3087348e8d44e1feda1917443987", null ],
                    [ "Retry", "classPackuru_1_1Core_1_1CreationNameErrorPromptCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa6327b4e59f58137083214a1fec358855", null ],
                    [ "YouCanChangeArchiveName", "classPackuru_1_1Core_1_1CreationNameErrorPromptCore.html#aae24373b6c0bbb21fcd8a1cf051e29afad5a52b8ec2eb9a3abc6214ab1c28f6e6", null ]
                  ] ],
                  [ "CreationNameErrorPromptCore", "classPackuru_1_1Core_1_1CreationNameErrorPromptCore.html#a499b8da30e79b8dcf0cccff0da2e870f", null ],
                  [ "~CreationNameErrorPromptCore", "classPackuru_1_1Core_1_1CreationNameErrorPromptCore.html#af45337ab9fe72d2d9595e5b6c3f89371", null ],
                  [ "abort", "classPackuru_1_1Core_1_1CreationNameErrorPromptCore.html#a220fbc82bcea18cde1d84500470b38d1", null ],
                  [ "archiveName", "classPackuru_1_1Core_1_1CreationNameErrorPromptCore.html#a0bbca4ecbfc6610cb12632e7dc10af6f", null ],
                  [ "deleted", "classPackuru_1_1Core_1_1CreationNameErrorPromptCore.html#ad98350fa0941ca1e625dac12428429fa", null ],
                  [ "errorInfo", "classPackuru_1_1Core_1_1CreationNameErrorPromptCore.html#ad63200b3e696d3a2fb7d4c8724e5299b", null ],
                  [ "retry", "classPackuru_1_1Core_1_1CreationNameErrorPromptCore.html#aaed64d49cf1893b4d15f4011a498e99d", null ]
                ] ]
              ] ],
              [ "extractiondialogcontrollertype.h", "extractiondialogcontrollertype_8h.html", [
                [ "ExtractionDialogControllerType", "classPackuru_1_1Core_1_1ExtractionDialogControllerType.html", [
                  [ "Type", "classPackuru_1_1Core_1_1ExtractionDialogControllerType.html#a1d1cfd8ffb84e947f82999c682b666a7", [
                    [ "AcceptButton", "classPackuru_1_1Core_1_1ExtractionDialogControllerType.html#a1d1cfd8ffb84e947f82999c682b666a7a54d40f47d348115168307bd4c1cdada0", null ],
                    [ "DestinationError", "classPackuru_1_1Core_1_1ExtractionDialogControllerType.html#a1d1cfd8ffb84e947f82999c682b666a7ad1c9d421338dc8177762b1c34d180543", null ],
                    [ "DestinationMode", "classPackuru_1_1Core_1_1ExtractionDialogControllerType.html#a1d1cfd8ffb84e947f82999c682b666a7a74e856f5a8e178cf38ae4e7c58bc838d", null ],
                    [ "DestinationPath", "classPackuru_1_1Core_1_1ExtractionDialogControllerType.html#a1d1cfd8ffb84e947f82999c682b666a7aed14a1948af1b4acdadbdaefd50722a6", null ],
                    [ "DialogErrors", "classPackuru_1_1Core_1_1ExtractionDialogControllerType.html#a1d1cfd8ffb84e947f82999c682b666a7a8445140d1b04381ede8375dc9e157ed1", null ],
                    [ "FileExtractionMode", "classPackuru_1_1Core_1_1ExtractionDialogControllerType.html#a1d1cfd8ffb84e947f82999c682b666a7a8e39c58b8fbfa7bf0b4a43068a48629c", null ],
                    [ "FileOverwriteMode", "classPackuru_1_1Core_1_1ExtractionDialogControllerType.html#a1d1cfd8ffb84e947f82999c682b666a7a69fce6d24ca64a795dbdbc6045618d2b", null ],
                    [ "FolderOverwriteMode", "classPackuru_1_1Core_1_1ExtractionDialogControllerType.html#a1d1cfd8ffb84e947f82999c682b666a7a7e06140e991100abfa0b4d1cb3805466", null ],
                    [ "OpenDestinationOnCompletion", "classPackuru_1_1Core_1_1ExtractionDialogControllerType.html#a1d1cfd8ffb84e947f82999c682b666a7a1d522bbe436073c08e2febff71551110", null ],
                    [ "Password", "classPackuru_1_1Core_1_1ExtractionDialogControllerType.html#a1d1cfd8ffb84e947f82999c682b666a7adc647eb65e6711e155375218212b3964", null ],
                    [ "RunInQueue", "classPackuru_1_1Core_1_1ExtractionDialogControllerType.html#a1d1cfd8ffb84e947f82999c682b666a7a3e2ffba79e6d032b1c075e396fc70e1b", null ],
                    [ "TopFolderMode", "classPackuru_1_1Core_1_1ExtractionDialogControllerType.html#a1d1cfd8ffb84e947f82999c682b666a7a584a7f7e54fbbcd91de2e8e5b74f65ed", null ]
                  ] ]
                ] ],
                [ "qHash", "extractiondialogcontrollertype_8h.html#ac6b02e1d5212c7fab527c9289ee2f3b2", null ]
              ] ],
              [ "extractiondialogcore.h", "extractiondialogcore_8h.html", [
                [ "ExtractionDialogCore", "classPackuru_1_1Core_1_1ExtractionDialogCore.html", [
                  [ "DialogMode", "classPackuru_1_1Core_1_1ExtractionDialogCore.html#a8417b7c0317799c868a2a7408772a902", [
                    [ "SingleArchive", "classPackuru_1_1Core_1_1ExtractionDialogCore.html#a8417b7c0317799c868a2a7408772a902a2e030f3064798bd2bb442ee4bb0fe350", null ],
                    [ "MultiArchive", "classPackuru_1_1Core_1_1ExtractionDialogCore.html#a8417b7c0317799c868a2a7408772a902a1320a01fdbb9d966b73060c49796e01c", null ]
                  ] ],
                  [ "UserString", "classPackuru_1_1Core_1_1ExtractionDialogCore.html#aae24373b6c0bbb21fcd8a1cf051e29af", [
                    [ "___INVALID", "classPackuru_1_1Core_1_1ExtractionDialogCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa6e545caba6d4293f6c360bc2e183bd64", null ],
                    [ "Cancel", "classPackuru_1_1Core_1_1ExtractionDialogCore.html#aae24373b6c0bbb21fcd8a1cf051e29afaea4788705e6873b424c65e91c2846b19", null ],
                    [ "DialogTitleMulti", "classPackuru_1_1Core_1_1ExtractionDialogCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa16506c7dcca0f1f0eea20e2cf5f85ad8", null ],
                    [ "DialogTitleSingle", "classPackuru_1_1Core_1_1ExtractionDialogCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa540a44eb092d329e7d257535929567cb", null ],
                    [ "TabArchives", "classPackuru_1_1Core_1_1ExtractionDialogCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa20aaffe14760557d9f0a961fafbf63fc", null ],
                    [ "TabOptions", "classPackuru_1_1Core_1_1ExtractionDialogCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa1fd2d79c4a6ec415bed13956dcddd7f1", null ]
                  ] ],
                  [ "ExtractionDialogCore", "classPackuru_1_1Core_1_1ExtractionDialogCore.html#a4aaac96c705525ec330f6daa3e5b92c0", null ],
                  [ "ExtractionDialogCore", "classPackuru_1_1Core_1_1ExtractionDialogCore.html#a7cb16aed93f0e340cbdcb0909f04570b", null ],
                  [ "~ExtractionDialogCore", "classPackuru_1_1Core_1_1ExtractionDialogCore.html#a0273454246008ee64c540bae1e421b29", null ],
                  [ "accept", "classPackuru_1_1Core_1_1ExtractionDialogCore.html#a74ddbe2ce21e10cdd5e4586e2e9f0607", null ],
                  [ "addArchives", "classPackuru_1_1Core_1_1ExtractionDialogCore.html#a756dcb1779d10a07294b22743d02aa99", null ],
                  [ "destroyLater", "classPackuru_1_1Core_1_1ExtractionDialogCore.html#aa5f97b4f6309ac39ce270d0ac0f92d50", null ],
                  [ "getArchivesModel", "classPackuru_1_1Core_1_1ExtractionDialogCore.html#aeb9784dda34852ba332ca3f01244dda5", null ],
                  [ "getControllerEngine", "classPackuru_1_1Core_1_1ExtractionDialogCore.html#a72d2d5dda34983ff5afcb6039a09bdbb", null ],
                  [ "getMode", "classPackuru_1_1Core_1_1ExtractionDialogCore.html#aa3e80e215041cf91f33eb042be7e3d64", null ],
                  [ "removeArchives", "classPackuru_1_1Core_1_1ExtractionDialogCore.html#a91e9284022ca279baa68d5784dbffee8", null ]
                ] ]
              ] ],
              [ "extractionfileoverwritepromptcore.h", "extractionfileoverwritepromptcore_8h.html", [
                [ "ExtractionFileOverwritePromptCore", "classPackuru_1_1Core_1_1ExtractionFileOverwritePromptCore.html", [
                  [ "FileType", "classPackuru_1_1Core_1_1ExtractionFileOverwritePromptCore.html#a2c794c5c13ab4dd7e65bad031dbe41c3", [
                    [ "File", "classPackuru_1_1Core_1_1ExtractionFileOverwritePromptCore.html#a2c794c5c13ab4dd7e65bad031dbe41c3a0b27918290ff5323bea1e3b78a9cf04e", null ],
                    [ "Link", "classPackuru_1_1Core_1_1ExtractionFileOverwritePromptCore.html#a2c794c5c13ab4dd7e65bad031dbe41c3a97e7c9a7d06eac006a28bf05467fcc8b", null ]
                  ] ],
                  [ "UserString", "classPackuru_1_1Core_1_1ExtractionFileOverwritePromptCore.html#aae24373b6c0bbb21fcd8a1cf051e29af", [
                    [ "___INVALID", "classPackuru_1_1Core_1_1ExtractionFileOverwritePromptCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa6e545caba6d4293f6c360bc2e183bd64", null ],
                    [ "Abort", "classPackuru_1_1Core_1_1ExtractionFileOverwritePromptCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa727b63583e01fa2b3952dab580c84dc2", null ],
                    [ "ApplyToAll", "classPackuru_1_1Core_1_1ExtractionFileOverwritePromptCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa39d3ee11834e3e8833582218907a9cfe", null ],
                    [ "Destination", "classPackuru_1_1Core_1_1ExtractionFileOverwritePromptCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa12007e1d59f4d09c87dbe2c438256244", null ],
                    [ "File", "classPackuru_1_1Core_1_1ExtractionFileOverwritePromptCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa0b27918290ff5323bea1e3b78a9cf04e", null ],
                    [ "FileAlreadyExists", "classPackuru_1_1Core_1_1ExtractionFileOverwritePromptCore.html#aae24373b6c0bbb21fcd8a1cf051e29afacf25d5cc7db64ddc7b38564b7704cca2", null ],
                    [ "Location", "classPackuru_1_1Core_1_1ExtractionFileOverwritePromptCore.html#aae24373b6c0bbb21fcd8a1cf051e29aface5bf551379459c1c61d2a204061c455", null ],
                    [ "Modified", "classPackuru_1_1Core_1_1ExtractionFileOverwritePromptCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa35e0c8c0b180c95d4e122e55ed62cc64", null ],
                    [ "Overwrite", "classPackuru_1_1Core_1_1ExtractionFileOverwritePromptCore.html#aae24373b6c0bbb21fcd8a1cf051e29afada364eb37e143f6b2b5559aa03f5913a", null ],
                    [ "Retry", "classPackuru_1_1Core_1_1ExtractionFileOverwritePromptCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa6327b4e59f58137083214a1fec358855", null ],
                    [ "Size", "classPackuru_1_1Core_1_1ExtractionFileOverwritePromptCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa6f6cb72d544962fa333e2e34ce64f719", null ],
                    [ "Skip", "classPackuru_1_1Core_1_1ExtractionFileOverwritePromptCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa72ef2b9b6965d078e3c7f95487a82d1c", null ],
                    [ "Source", "classPackuru_1_1Core_1_1ExtractionFileOverwritePromptCore.html#aae24373b6c0bbb21fcd8a1cf051e29afaf31bbdd1b3e85bccd652680e16935819", null ]
                  ] ],
                  [ "ExtractionFileOverwritePromptCore", "classPackuru_1_1Core_1_1ExtractionFileOverwritePromptCore.html#a1790e7849ed188369f6b05de3a062e54", null ],
                  [ "~ExtractionFileOverwritePromptCore", "classPackuru_1_1Core_1_1ExtractionFileOverwritePromptCore.html#af151c2366dd6a539ca9b4178d5cc94d8", null ],
                  [ "abort", "classPackuru_1_1Core_1_1ExtractionFileOverwritePromptCore.html#a220fbc82bcea18cde1d84500470b38d1", null ],
                  [ "deleted", "classPackuru_1_1Core_1_1ExtractionFileOverwritePromptCore.html#ad98350fa0941ca1e625dac12428429fa", null ],
                  [ "destinationFilePath", "classPackuru_1_1Core_1_1ExtractionFileOverwritePromptCore.html#a7139775b1887bc6be55fd737d3c1bd14", null ],
                  [ "destinationLocation", "classPackuru_1_1Core_1_1ExtractionFileOverwritePromptCore.html#a8483b0cda34e6bde49ab3cf55d6d3d29", null ],
                  [ "destinationModifiedDate", "classPackuru_1_1Core_1_1ExtractionFileOverwritePromptCore.html#ada58e924774230265164e8db55301f3e", null ],
                  [ "destinationName", "classPackuru_1_1Core_1_1ExtractionFileOverwritePromptCore.html#afdb5c8bb11e7b8b05c5a2c78504b51cc", null ],
                  [ "destinationSize", "classPackuru_1_1Core_1_1ExtractionFileOverwritePromptCore.html#ad1e0c444698b4f47f6a96610985ea7f2", null ],
                  [ "getFileType", "classPackuru_1_1Core_1_1ExtractionFileOverwritePromptCore.html#aa08b9aefd026655793c9a03bd71a0168", null ],
                  [ "overwrite", "classPackuru_1_1Core_1_1ExtractionFileOverwritePromptCore.html#ab6407f13a2e84ea02d58eb2f994b43be", null ],
                  [ "retry", "classPackuru_1_1Core_1_1ExtractionFileOverwritePromptCore.html#a5619747c1e923b27c794f221811ef88a", null ],
                  [ "skip", "classPackuru_1_1Core_1_1ExtractionFileOverwritePromptCore.html#a57aea69559b0ab43d059bedd678ed26a", null ],
                  [ "sourceFilePath", "classPackuru_1_1Core_1_1ExtractionFileOverwritePromptCore.html#a3e6b6370313cc08e202e103bba9357a2", null ],
                  [ "sourceLocation", "classPackuru_1_1Core_1_1ExtractionFileOverwritePromptCore.html#a38e20f4a8dd200f913e18e817cfd2055", null ],
                  [ "sourceModifiedDate", "classPackuru_1_1Core_1_1ExtractionFileOverwritePromptCore.html#a65b3c5873b1d423a8693309718001652", null ],
                  [ "sourceName", "classPackuru_1_1Core_1_1ExtractionFileOverwritePromptCore.html#a288a4c87c7f419c4ea75b628c99889b6", null ],
                  [ "sourceSize", "classPackuru_1_1Core_1_1ExtractionFileOverwritePromptCore.html#ad4a75a8748312944d1651ffffaab9c6f", null ]
                ] ]
              ] ],
              [ "extractionfolderoverwritepromptcore.h", "extractionfolderoverwritepromptcore_8h.html", [
                [ "ExtractionFolderOverwritePromptCore", "classPackuru_1_1Core_1_1ExtractionFolderOverwritePromptCore.html", [
                  [ "UserString", "classPackuru_1_1Core_1_1ExtractionFolderOverwritePromptCore.html#aae24373b6c0bbb21fcd8a1cf051e29af", [
                    [ "___INVALID", "classPackuru_1_1Core_1_1ExtractionFolderOverwritePromptCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa6e545caba6d4293f6c360bc2e183bd64", null ],
                    [ "Abort", "classPackuru_1_1Core_1_1ExtractionFolderOverwritePromptCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa727b63583e01fa2b3952dab580c84dc2", null ],
                    [ "ApplyToAll", "classPackuru_1_1Core_1_1ExtractionFolderOverwritePromptCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa39d3ee11834e3e8833582218907a9cfe", null ],
                    [ "Content", "classPackuru_1_1Core_1_1ExtractionFolderOverwritePromptCore.html#aae24373b6c0bbb21fcd8a1cf051e29afaf15c1cae7882448b3fb0404682e17e61", null ],
                    [ "Destination", "classPackuru_1_1Core_1_1ExtractionFolderOverwritePromptCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa12007e1d59f4d09c87dbe2c438256244", null ],
                    [ "Folder", "classPackuru_1_1Core_1_1ExtractionFolderOverwritePromptCore.html#aae24373b6c0bbb21fcd8a1cf051e29afab0f2b97dc5d2b76b26e040408bb1d8af", null ],
                    [ "FolderAlreadyExists", "classPackuru_1_1Core_1_1ExtractionFolderOverwritePromptCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa91cede70800e35c65360d0013e073a07", null ],
                    [ "Modified", "classPackuru_1_1Core_1_1ExtractionFolderOverwritePromptCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa35e0c8c0b180c95d4e122e55ed62cc64", null ],
                    [ "Retry", "classPackuru_1_1Core_1_1ExtractionFolderOverwritePromptCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa6327b4e59f58137083214a1fec358855", null ],
                    [ "Skip", "classPackuru_1_1Core_1_1ExtractionFolderOverwritePromptCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa72ef2b9b6965d078e3c7f95487a82d1c", null ],
                    [ "Source", "classPackuru_1_1Core_1_1ExtractionFolderOverwritePromptCore.html#aae24373b6c0bbb21fcd8a1cf051e29afaf31bbdd1b3e85bccd652680e16935819", null ],
                    [ "WriteInto", "classPackuru_1_1Core_1_1ExtractionFolderOverwritePromptCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa3ca35dd9911b3ef691b5b34c0b8c769e", null ]
                  ] ],
                  [ "ExtractionFolderOverwritePromptCore", "classPackuru_1_1Core_1_1ExtractionFolderOverwritePromptCore.html#aa0308cc82612b72972e5cada4ed1d1e0", null ],
                  [ "~ExtractionFolderOverwritePromptCore", "classPackuru_1_1Core_1_1ExtractionFolderOverwritePromptCore.html#aaaf4fb049d41d417ba21231a7206061a", null ],
                  [ "abort", "classPackuru_1_1Core_1_1ExtractionFolderOverwritePromptCore.html#a220fbc82bcea18cde1d84500470b38d1", null ],
                  [ "deleted", "classPackuru_1_1Core_1_1ExtractionFolderOverwritePromptCore.html#ad98350fa0941ca1e625dac12428429fa", null ],
                  [ "destinationContent", "classPackuru_1_1Core_1_1ExtractionFolderOverwritePromptCore.html#af1c33facd36b5a14027d7e5487762850", null ],
                  [ "destinationModifiedDate", "classPackuru_1_1Core_1_1ExtractionFolderOverwritePromptCore.html#ada58e924774230265164e8db55301f3e", null ],
                  [ "destinationPath", "classPackuru_1_1Core_1_1ExtractionFolderOverwritePromptCore.html#a4faa3a09310d6a7702574b55f0eb67ec", null ],
                  [ "overwrite", "classPackuru_1_1Core_1_1ExtractionFolderOverwritePromptCore.html#ab6407f13a2e84ea02d58eb2f994b43be", null ],
                  [ "retry", "classPackuru_1_1Core_1_1ExtractionFolderOverwritePromptCore.html#a5619747c1e923b27c794f221811ef88a", null ],
                  [ "skip", "classPackuru_1_1Core_1_1ExtractionFolderOverwritePromptCore.html#a57aea69559b0ab43d059bedd678ed26a", null ],
                  [ "sourceContent", "classPackuru_1_1Core_1_1ExtractionFolderOverwritePromptCore.html#af11be36673bc61d1bfa5fbdd42a85178", null ],
                  [ "sourceModifiedDate", "classPackuru_1_1Core_1_1ExtractionFolderOverwritePromptCore.html#a65b3c5873b1d423a8693309718001652", null ],
                  [ "sourcePath", "classPackuru_1_1Core_1_1ExtractionFolderOverwritePromptCore.html#a5e714656f589fd04db6f11530d2a17d2", null ]
                ] ]
              ] ],
              [ "extractiontypemismatchpromptcore.h", "extractiontypemismatchpromptcore_8h.html", [
                [ "ExtractionTypeMismatchPromptCore", "classPackuru_1_1Core_1_1ExtractionTypeMismatchPromptCore.html", [
                  [ "FileType", "classPackuru_1_1Core_1_1ExtractionTypeMismatchPromptCore.html#a2c794c5c13ab4dd7e65bad031dbe41c3", [
                    [ "Dir", "classPackuru_1_1Core_1_1ExtractionTypeMismatchPromptCore.html#a2c794c5c13ab4dd7e65bad031dbe41c3a3c4e6dbf3e1df93a734d9959bac9ee94", null ],
                    [ "File", "classPackuru_1_1Core_1_1ExtractionTypeMismatchPromptCore.html#a2c794c5c13ab4dd7e65bad031dbe41c3a0b27918290ff5323bea1e3b78a9cf04e", null ],
                    [ "Link", "classPackuru_1_1Core_1_1ExtractionTypeMismatchPromptCore.html#a2c794c5c13ab4dd7e65bad031dbe41c3a97e7c9a7d06eac006a28bf05467fcc8b", null ],
                    [ "___INVALID", "classPackuru_1_1Core_1_1ExtractionTypeMismatchPromptCore.html#a2c794c5c13ab4dd7e65bad031dbe41c3a6e545caba6d4293f6c360bc2e183bd64", null ]
                  ] ],
                  [ "UserString", "classPackuru_1_1Core_1_1ExtractionTypeMismatchPromptCore.html#aae24373b6c0bbb21fcd8a1cf051e29af", [
                    [ "___INVALID", "classPackuru_1_1Core_1_1ExtractionTypeMismatchPromptCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa6e545caba6d4293f6c360bc2e183bd64", null ],
                    [ "Abort", "classPackuru_1_1Core_1_1ExtractionTypeMismatchPromptCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa727b63583e01fa2b3952dab580c84dc2", null ],
                    [ "ApplyToAll", "classPackuru_1_1Core_1_1ExtractionTypeMismatchPromptCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa39d3ee11834e3e8833582218907a9cfe", null ],
                    [ "Destination", "classPackuru_1_1Core_1_1ExtractionTypeMismatchPromptCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa12007e1d59f4d09c87dbe2c438256244", null ],
                    [ "File", "classPackuru_1_1Core_1_1ExtractionTypeMismatchPromptCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa0b27918290ff5323bea1e3b78a9cf04e", null ],
                    [ "Folder", "classPackuru_1_1Core_1_1ExtractionTypeMismatchPromptCore.html#aae24373b6c0bbb21fcd8a1cf051e29afab0f2b97dc5d2b76b26e040408bb1d8af", null ],
                    [ "ItemsTypeMismatch", "classPackuru_1_1Core_1_1ExtractionTypeMismatchPromptCore.html#aae24373b6c0bbb21fcd8a1cf051e29afae753cc4a20b69abd4eb4a81de811c918", null ],
                    [ "Link", "classPackuru_1_1Core_1_1ExtractionTypeMismatchPromptCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa97e7c9a7d06eac006a28bf05467fcc8b", null ],
                    [ "Location", "classPackuru_1_1Core_1_1ExtractionTypeMismatchPromptCore.html#aae24373b6c0bbb21fcd8a1cf051e29aface5bf551379459c1c61d2a204061c455", null ],
                    [ "Retry", "classPackuru_1_1Core_1_1ExtractionTypeMismatchPromptCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa6327b4e59f58137083214a1fec358855", null ],
                    [ "Skip", "classPackuru_1_1Core_1_1ExtractionTypeMismatchPromptCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa72ef2b9b6965d078e3c7f95487a82d1c", null ],
                    [ "Source", "classPackuru_1_1Core_1_1ExtractionTypeMismatchPromptCore.html#aae24373b6c0bbb21fcd8a1cf051e29afaf31bbdd1b3e85bccd652680e16935819", null ]
                  ] ],
                  [ "ExtractionTypeMismatchPromptCore", "classPackuru_1_1Core_1_1ExtractionTypeMismatchPromptCore.html#aa8db73f6dcc00a05ab4498ffe16fcac1", null ],
                  [ "~ExtractionTypeMismatchPromptCore", "classPackuru_1_1Core_1_1ExtractionTypeMismatchPromptCore.html#a247b2849268a25807c7dd9f7c1fa33e7", null ],
                  [ "abort", "classPackuru_1_1Core_1_1ExtractionTypeMismatchPromptCore.html#a220fbc82bcea18cde1d84500470b38d1", null ],
                  [ "deleted", "classPackuru_1_1Core_1_1ExtractionTypeMismatchPromptCore.html#ad98350fa0941ca1e625dac12428429fa", null ],
                  [ "destinationFilePath", "classPackuru_1_1Core_1_1ExtractionTypeMismatchPromptCore.html#a7139775b1887bc6be55fd737d3c1bd14", null ],
                  [ "destinationFileType", "classPackuru_1_1Core_1_1ExtractionTypeMismatchPromptCore.html#aeac8d9d7dc206efa54ec8fb6b31dd3a7", null ],
                  [ "destinationPath", "classPackuru_1_1Core_1_1ExtractionTypeMismatchPromptCore.html#a4faa3a09310d6a7702574b55f0eb67ec", null ],
                  [ "itemFileName", "classPackuru_1_1Core_1_1ExtractionTypeMismatchPromptCore.html#a9755baf30956b04e0764f2c50713fd03", null ],
                  [ "retry", "classPackuru_1_1Core_1_1ExtractionTypeMismatchPromptCore.html#a5619747c1e923b27c794f221811ef88a", null ],
                  [ "skip", "classPackuru_1_1Core_1_1ExtractionTypeMismatchPromptCore.html#a57aea69559b0ab43d059bedd678ed26a", null ],
                  [ "sourceFilePath", "classPackuru_1_1Core_1_1ExtractionTypeMismatchPromptCore.html#a3e6b6370313cc08e202e103bba9357a2", null ],
                  [ "sourceFileType", "classPackuru_1_1Core_1_1ExtractionTypeMismatchPromptCore.html#a1cdef08eff49325f3268909210362032", null ],
                  [ "sourcePath", "classPackuru_1_1Core_1_1ExtractionTypeMismatchPromptCore.html#a5e714656f589fd04db6f11530d2a17d2", null ]
                ] ]
              ] ],
              [ "globalsettingsconverter.h", "globalsettingsconverter_8h.html", [
                [ "GlobalSettingsConverter", "classPackuru_1_1Core_1_1GlobalSettingsConverter.html", [
                  [ "GlobalSettingsConverter", "classPackuru_1_1Core_1_1GlobalSettingsConverter.html#a08c7b8f45fb749fd273524c2d39c6588", null ],
                  [ "~GlobalSettingsConverter", "classPackuru_1_1Core_1_1GlobalSettingsConverter.html#a84bb9d94ff372ee3a21c2c145f01480d", null ],
                  [ "fromSettingsType", "classPackuru_1_1Core_1_1GlobalSettingsConverter.html#a91c6840423b56a5ed616710b0f933635", null ],
                  [ "getKeyName", "classPackuru_1_1Core_1_1GlobalSettingsConverter.html#a048f67dae819e29ab9198c5d8a0cfa2c", null ],
                  [ "getSupportedKeys", "classPackuru_1_1Core_1_1GlobalSettingsConverter.html#a9a602c59929fd1ad450e6e0bbed494d8", null ],
                  [ "toSettingsType", "classPackuru_1_1Core_1_1GlobalSettingsConverter.html#acbb02dabb0996c149e2e8447956ec8b3", null ]
                ] ]
              ] ],
              [ "globalsettingsmanager.h", "globalsettingsmanager_8h.html", [
                [ "GlobalSettingsManager", "classPackuru_1_1Core_1_1GlobalSettingsManager.html", [
                  [ "~GlobalSettingsManager", "classPackuru_1_1Core_1_1GlobalSettingsManager.html#a0b3258e1ca1bb19a9411f1e03d21b518", null ],
                  [ "addCustomSettingsConverter", "classPackuru_1_1Core_1_1GlobalSettingsManager.html#a221d9bce49ce589938093f8b07f2ef33", null ],
                  [ "getValue", "classPackuru_1_1Core_1_1GlobalSettingsManager.html#a0eeca80f1bf05739eab14b721e8802a5", null ],
                  [ "getValueAs", "classPackuru_1_1Core_1_1GlobalSettingsManager.html#a19f3c39e32e7ba49f6cb88c71e35690e", null ],
                  [ "setValue", "classPackuru_1_1Core_1_1GlobalSettingsManager.html#a306783bf351c920367fdddd834d6a9c6", null ],
                  [ "setValue", "classPackuru_1_1Core_1_1GlobalSettingsManager.html#a9616971e5e9e9e4668bee6960646fb84", null ],
                  [ "valueChanged", "classPackuru_1_1Core_1_1GlobalSettingsManager.html#a73b379fc8bad5c0041331b65c850cd50", null ]
                ] ]
              ] ],
              [ "globalsettingsuservalue.h", "globalsettingsuservalue_8h.html", [
                [ "globalSettingsUserValue", "globalsettingsuservalue_8h.html#a375804c835b93e91ac30ff3a16f4136e", null ]
              ] ],
              [ "htmllink.h", "htmllink_8h.html", [
                [ "htmlLink", "htmllink_8h.html#ade020852b42df1a5f985742ba23f010c", null ]
              ] ],
              [ "passwordpromptcore.h", "passwordpromptcore_8h.html", [
                [ "PasswordPromptCore", "classPackuru_1_1Core_1_1PasswordPromptCore.html", [
                  [ "UserString", "classPackuru_1_1Core_1_1PasswordPromptCore.html#aae24373b6c0bbb21fcd8a1cf051e29af", [
                    [ "___INVALID", "classPackuru_1_1Core_1_1PasswordPromptCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa6e545caba6d4293f6c360bc2e183bd64", null ],
                    [ "Abort", "classPackuru_1_1Core_1_1PasswordPromptCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa727b63583e01fa2b3952dab580c84dc2", null ],
                    [ "DialogTitle", "classPackuru_1_1Core_1_1PasswordPromptCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa59e6edcf06044686b74fc871f4098d0e", null ]
                  ] ],
                  [ "PasswordPromptCore", "classPackuru_1_1Core_1_1PasswordPromptCore.html#a82fd99ab1a2649aaa29bc071e541a70b", null ],
                  [ "~PasswordPromptCore", "classPackuru_1_1Core_1_1PasswordPromptCore.html#af6393b88faebdf1440ace7150a5057f0", null ],
                  [ "abort", "classPackuru_1_1Core_1_1PasswordPromptCore.html#a220fbc82bcea18cde1d84500470b38d1", null ],
                  [ "deleted", "classPackuru_1_1Core_1_1PasswordPromptCore.html#ad98350fa0941ca1e625dac12428429fa", null ],
                  [ "enterPassword", "classPackuru_1_1Core_1_1PasswordPromptCore.html#a1860e648a165050eb0146aab4c49846b", null ],
                  [ "getArchiveName", "classPackuru_1_1Core_1_1PasswordPromptCore.html#ac021d52a3bc4b9c507ffad1345b91526", null ]
                ] ]
              ] ],
              [ "plugindatamodel.h", "plugindatamodel_8h.html", [
                [ "PluginDataModel", "classPackuru_1_1Core_1_1PluginDataModel.html", [
                  [ "PluginDataModel", "classPackuru_1_1Core_1_1PluginDataModel.html#a7878d60bbb3606f8d79e3a1f14e4537d", null ],
                  [ "~PluginDataModel", "classPackuru_1_1Core_1_1PluginDataModel.html#ac89a75a36aa427ae459b4589ab46389f", null ],
                  [ "data", "classPackuru_1_1Core_1_1PluginDataModel.html#ae93b915598cdbe5ae59dddf41d917a53", null ],
                  [ "getDescription", "classPackuru_1_1Core_1_1PluginDataModel.html#a51c934f06ef2b903e43e8e48f4f56948", null ],
                  [ "getHomepage", "classPackuru_1_1Core_1_1PluginDataModel.html#aaf5163bb163077b79b0de0219bc809b3", null ],
                  [ "getPath", "classPackuru_1_1Core_1_1PluginDataModel.html#ad1377ba27eb47d8bc6c4718304a01629", null ],
                  [ "rowCount", "classPackuru_1_1Core_1_1PluginDataModel.html#a01847fb0c363a027bfc99678c63c400c", null ],
                  [ "supportedReadArchives", "classPackuru_1_1Core_1_1PluginDataModel.html#a0ceca5a70b6ff0841923c1e48a21c2c8", null ],
                  [ "supportedWriteArchives", "classPackuru_1_1Core_1_1PluginDataModel.html#a42f672ccda01350691b5ecd96ae15c99", null ]
                ] ]
              ] ],
              [ "testdialogcontrollertype.h", "testdialogcontrollertype_8h.html", [
                [ "TestDialogControllerType", "classPackuru_1_1Core_1_1TestDialogControllerType.html", [
                  [ "Type", "classPackuru_1_1Core_1_1TestDialogControllerType.html#a1d1cfd8ffb84e947f82999c682b666a7", [
                    [ "AcceptButton", "classPackuru_1_1Core_1_1TestDialogControllerType.html#a1d1cfd8ffb84e947f82999c682b666a7a54d40f47d348115168307bd4c1cdada0", null ],
                    [ "DialogErrors", "classPackuru_1_1Core_1_1TestDialogControllerType.html#a1d1cfd8ffb84e947f82999c682b666a7a8445140d1b04381ede8375dc9e157ed1", null ],
                    [ "Password", "classPackuru_1_1Core_1_1TestDialogControllerType.html#a1d1cfd8ffb84e947f82999c682b666a7adc647eb65e6711e155375218212b3964", null ],
                    [ "RunInQueue", "classPackuru_1_1Core_1_1TestDialogControllerType.html#a1d1cfd8ffb84e947f82999c682b666a7a3e2ffba79e6d032b1c075e396fc70e1b", null ]
                  ] ]
                ] ],
                [ "qHash", "testdialogcontrollertype_8h.html#a3d0d71e82e0f93426ddd0153451dcbcb", null ]
              ] ],
              [ "testdialogcore.h", "testdialogcore_8h.html", [
                [ "TestDialogCore", "classPackuru_1_1Core_1_1TestDialogCore.html", [
                  [ "DialogMode", "classPackuru_1_1Core_1_1TestDialogCore.html#a8417b7c0317799c868a2a7408772a902", [
                    [ "SingleArchive", "classPackuru_1_1Core_1_1TestDialogCore.html#a8417b7c0317799c868a2a7408772a902a2e030f3064798bd2bb442ee4bb0fe350", null ],
                    [ "MultiArchive", "classPackuru_1_1Core_1_1TestDialogCore.html#a8417b7c0317799c868a2a7408772a902a1320a01fdbb9d966b73060c49796e01c", null ]
                  ] ],
                  [ "UserString", "classPackuru_1_1Core_1_1TestDialogCore.html#aae24373b6c0bbb21fcd8a1cf051e29af", [
                    [ "___INVALID", "classPackuru_1_1Core_1_1TestDialogCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa6e545caba6d4293f6c360bc2e183bd64", null ],
                    [ "Cancel", "classPackuru_1_1Core_1_1TestDialogCore.html#aae24373b6c0bbb21fcd8a1cf051e29afaea4788705e6873b424c65e91c2846b19", null ],
                    [ "DialogTitleMulti", "classPackuru_1_1Core_1_1TestDialogCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa16506c7dcca0f1f0eea20e2cf5f85ad8", null ],
                    [ "DialogTitleSingle", "classPackuru_1_1Core_1_1TestDialogCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa540a44eb092d329e7d257535929567cb", null ],
                    [ "TabArchives", "classPackuru_1_1Core_1_1TestDialogCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa20aaffe14760557d9f0a961fafbf63fc", null ],
                    [ "TabOptions", "classPackuru_1_1Core_1_1TestDialogCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa1fd2d79c4a6ec415bed13956dcddd7f1", null ]
                  ] ],
                  [ "TestDialogCore", "classPackuru_1_1Core_1_1TestDialogCore.html#a1431b55aa3d628d0546d7ce350276817", null ],
                  [ "TestDialogCore", "classPackuru_1_1Core_1_1TestDialogCore.html#a00ee2bbfecbfdf39de21808765e17904", null ],
                  [ "~TestDialogCore", "classPackuru_1_1Core_1_1TestDialogCore.html#ac9544a35738dc13de616ba88277d1bc2", null ],
                  [ "accept", "classPackuru_1_1Core_1_1TestDialogCore.html#a74ddbe2ce21e10cdd5e4586e2e9f0607", null ],
                  [ "addArchives", "classPackuru_1_1Core_1_1TestDialogCore.html#a756dcb1779d10a07294b22743d02aa99", null ],
                  [ "destroyLater", "classPackuru_1_1Core_1_1TestDialogCore.html#aa5f97b4f6309ac39ce270d0ac0f92d50", null ],
                  [ "getArchivesModel", "classPackuru_1_1Core_1_1TestDialogCore.html#aeb9784dda34852ba332ca3f01244dda5", null ],
                  [ "getControllerEngine", "classPackuru_1_1Core_1_1TestDialogCore.html#a72d2d5dda34983ff5afcb6039a09bdbb", null ],
                  [ "getMode", "classPackuru_1_1Core_1_1TestDialogCore.html#aa3e80e215041cf91f33eb042be7e3d64", null ],
                  [ "removeArchives", "classPackuru_1_1Core_1_1TestDialogCore.html#a91e9284022ca279baa68d5784dbffee8", null ]
                ] ]
              ] ]
            ] ],
            [ "core-browser", "dir_c1f580bc93b1491ad07caabbd4ed4f43.html", [
              [ "archivebrowsercore.h", "archivebrowsercore_8h.html", [
                [ "ArchiveBrowserCore", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html", [
                  [ "BrowserTaskState", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#a276d67ed03566f1cec934a98a78d23dd", [
                    [ "ReadyToRun", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#a276d67ed03566f1cec934a98a78d23dda506db5c85cf0fd10f93e5478013650b6", null ],
                    [ "StartingUp", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#a276d67ed03566f1cec934a98a78d23dda2454c0e7d149123452f860fcb976b65c", null ],
                    [ "InProgress", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#a276d67ed03566f1cec934a98a78d23dda12d868c18cb29bf58f02b504be9033fd", null ],
                    [ "WaitingForInput", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#a276d67ed03566f1cec934a98a78d23dda76f1e38106648a32dbed02d8898fe21c", null ],
                    [ "WaitingForPassword", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#a276d67ed03566f1cec934a98a78d23dda5fab9daa3c378d3f65ff689a5b950e76", null ],
                    [ "Paused", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#a276d67ed03566f1cec934a98a78d23ddae99180abf47a8b3a856e0bcb2656990a", null ],
                    [ "Aborted", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#a276d67ed03566f1cec934a98a78d23dda721c28f4c74928cc9e0bb3fef345e408", null ],
                    [ "Success", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#a276d67ed03566f1cec934a98a78d23dda505a83f220c02df2f85c3810cd9ceb38", null ],
                    [ "Warnings", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#a276d67ed03566f1cec934a98a78d23ddae17fdb615452f440c793b5ddad90dd5e", null ],
                    [ "Errors", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#a276d67ed03566f1cec934a98a78d23dda5ef0c737746fae2ca90e66c39333f8f6", null ],
                    [ "___INVALID", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#a276d67ed03566f1cec934a98a78d23dda6e545caba6d4293f6c360bc2e183bd64", null ]
                  ] ],
                  [ "UIState", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#a46982b6b3247b002a74dde6ca26b2344", [
                    [ "Navigation", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#a46982b6b3247b002a74dde6ca26b2344a846495f9ceed11accf8879f555936a7d", null ],
                    [ "StatusPage", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#a46982b6b3247b002a74dde6ca26b2344a903fb4f260e8fa3179d3a278430d0836", null ]
                  ] ],
                  [ "UserString", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#aae24373b6c0bbb21fcd8a1cf051e29af", [
                    [ "___INVALID", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa6e545caba6d4293f6c360bc2e183bd64", null ],
                    [ "ActionAdd", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#aae24373b6c0bbb21fcd8a1cf051e29afaded30500200846a9cdb7d18c51671196", null ],
                    [ "ActionDelete", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa36b4a85d379d8e72426e1a59a167b6af", null ],
                    [ "ActionExtract", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa7f2a064ae66cff8649053b333b33de7b", null ],
                    [ "ActionFilter", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa710ef25b897ef78bedcd99442ed4518a", null ],
                    [ "ActionPreview", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#aae24373b6c0bbb21fcd8a1cf051e29afaaba6df4a348123cf8adcf7694da06113", null ],
                    [ "ActionProperties", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa8fcdc6f191114223f0690bae910dc4a8", null ],
                    [ "ActionReload", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#aae24373b6c0bbb21fcd8a1cf051e29afaf2730324df06d72982c8528374ef4cf4", null ],
                    [ "ActionTest", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa107f1c4fc93e51f8d693fa4c9eabac77", null ]
                  ] ],
                  [ "ArchiveBrowserCore", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#ab0855321499dde496f660f0866fce24a", null ],
                  [ "~ArchiveBrowserCore", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#a798951178a20923c69f47b60e0758a62", null ],
                  [ "addAvailableChanged", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#abbe8f54545f276d88181b17183e6fd7b", null ],
                  [ "archivePropertiesAvailable", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#a39d3dc43a324ab2837b0ad7a4e2dcf88", null ],
                  [ "availableActionsChanged", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#a6b38fa62503821e2fcdee137d113b73e", null ],
                  [ "browserReset", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#af457caa9b318d69811d47414df3a6c0b", null ],
                  [ "createArchivingDialogCore", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#ab3a1074f83e0e613e8c8fa784bc0af34", null ],
                  [ "createDeletionDialogCore", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#a96c10c2e3e745113bd900d51b4e5eda2", null ],
                  [ "createExtractionDialogCore", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#a2d3d85a4f68cf01fc7973fc042b4e2e8", null ],
                  [ "createTestDialogCore", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#ac762aa21c5f6755b5d6e2bc9e9ec0c50", null ],
                  [ "deleteAvailableChanged", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#a0673efce544fcd65c98461e9e23493d5", null ],
                  [ "destroyLater", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#aa5f97b4f6309ac39ce270d0ac0f92d50", null ],
                  [ "extractAvailableChanged", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#a233f721e58fedee0040a3e3b27fdf3ab", null ],
                  [ "filesReadyForPreview", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#a5436e66ddf284c411a4420d17831d4ed", null ],
                  [ "filterAvailableChanged", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#aaa8b43335673fb4ee1464828a1b840fc", null ],
                  [ "getArchiveComment", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#a64e6a165c5412dd8e8759a35d4eb0470", null ],
                  [ "getArchiveName", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#ac021d52a3bc4b9c507ffad1345b91526", null ],
                  [ "getNavigator", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#aa788ab29c8f807dcfc7ec870502e6b21", null ],
                  [ "getPropertiesModel", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#a668ed7bf905a84ee2f48ae3c666278c4", null ],
                  [ "getStatusPageCore", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#a9ea4f2793a9f97a608488a8ed74c152f", null ],
                  [ "getTaskState", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#a2d15aad1047131f45c1c8239f2fde121", null ],
                  [ "getTitle", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#a9c99cda566ec653b87bfc34b0d76d999", null ],
                  [ "getUIState", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#a7947d79cf8e8f7dea95a76089d1b6884", null ],
                  [ "isAddAvailable", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#af874935e44c7482f7aacd6db322a48fc", null ],
                  [ "isDeleteAvailable", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#ad84fb9bb10574895982ee0bc02617947", null ],
                  [ "isExtractAvailable", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#a5712c1b578d156352419b21d4f701d7e", null ],
                  [ "isFilterAvailable", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#a56a255cd59a905dd29627d0772cdf01c", null ],
                  [ "isPreviewAvailable", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#ac9c78f19a63440fb87f758f3eada7f20", null ],
                  [ "isReloadAvailable", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#a14d7f859896bc364fd29da0782e354dd", null ],
                  [ "isTaskBusy", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#aa7367792d8ac5d62812c4ebf7659cdde", null ],
                  [ "isTaskFinished", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#a98309a2ecca96501e5687223c9b35664", null ],
                  [ "needsActivation", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#a64811fcfee1f0127433bf06df1cf22d8", null ],
                  [ "openExtractionDestinationFolder", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#aafe344417df8b86062073f2bba7d32d5", null ],
                  [ "previewAvailableChanged", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#a7ce563866f9b015b9a22d9760c5fe5c4", null ],
                  [ "previewSelectedFiles", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#a10126b1c3ffe3458761a0623fe9008c9", null ],
                  [ "propertiesAvailableChanged", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#ac64148204ccf75c55d26870387dbe566", null ],
                  [ "reloadArchive", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#a2a5622ae4776ea7b63f134af94bc8f52", null ],
                  [ "reloadAvailableChanged", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#ae1334f5b218dded4af222585d97a1716", null ],
                  [ "taskBusyChanged", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#a47a03038ef90eebecbd80e4aa3470cc0", null ],
                  [ "taskStateChanged", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#a1e898a1b9e36e125dba17103fbb3ed4b", null ],
                  [ "titleChanged", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#ab32e5e797b9f3e81016d6b703d7a4dd5", null ],
                  [ "uiStateChanged", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#a3b847815abda8981901aa0e241350c66", null ],
                  [ "addAvailable", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#ae19cc643d473717db458b8242ea9bd29", null ],
                  [ "archiveName", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#a896346e04ece5b9dde4808844c334979", null ],
                  [ "deleteAvailable", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#a9a85f454a6f9f07ce9de30cb482412af", null ],
                  [ "extractAvailable", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#ac2a6bb3a1325c27b063cf106600820e0", null ],
                  [ "filterAvailable", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#a6b70e0b50554fbd0ca0c6d017c1a9948", null ],
                  [ "navigator", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#a7da562c93d0a03e3bf67f3c0a8566c2f", null ],
                  [ "previewAvailable", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#a3ca912ab848e18f7ecf0efa747eab7bd", null ],
                  [ "propertiesAvailable", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#a0e13a66bb8d668e611bb912498e2efef", null ],
                  [ "reloadAvailable", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#ae46b9b270db5c73bd9dc9dc32331d4ac", null ],
                  [ "statusPageCore", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#a4c1963a115247ed627d75904e713c127", null ],
                  [ "taskBusy", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#a03f485fcaa8e97262e2a84ac94bb4176", null ],
                  [ "taskState", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#a12fd4dedcbae89ca6bddda34d9a1a7e5", null ],
                  [ "title", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#aa93d37c468bf6aea9c9823eb74bd1a48", null ],
                  [ "uiState", "classPackuru_1_1Core_1_1Browser_1_1ArchiveBrowserCore.html#a316e948d552200cb583868f3717fc940", null ]
                ] ]
              ] ],
              [ "archivenavigator.h", "archivenavigator_8h.html", [
                [ "ArchiveNavigator", "classPackuru_1_1Core_1_1Browser_1_1ArchiveNavigator.html", [
                  [ "PreviousPathIndexSelection", "classPackuru_1_1Core_1_1Browser_1_1ArchiveNavigator.html#a2bd4c31e8c3edcef188925e74455bcf0", [
                    [ "___INVALID", "classPackuru_1_1Core_1_1Browser_1_1ArchiveNavigator.html#a2bd4c31e8c3edcef188925e74455bcf0a6e545caba6d4293f6c360bc2e183bd64", null ],
                    [ "NoSelection", "classPackuru_1_1Core_1_1Browser_1_1ArchiveNavigator.html#a2bd4c31e8c3edcef188925e74455bcf0a5230c6f2cc0017a06cb7f3afd8f535cf", null ],
                    [ "SelectFirstIndex", "classPackuru_1_1Core_1_1Browser_1_1ArchiveNavigator.html#a2bd4c31e8c3edcef188925e74455bcf0a2d88e0eae49b226677a77f4844169414", null ],
                    [ "SelectRow", "classPackuru_1_1Core_1_1Browser_1_1ArchiveNavigator.html#a2bd4c31e8c3edcef188925e74455bcf0a5357abf3f205f710f010aa2be41cb224", null ]
                  ] ],
                  [ "UserString", "classPackuru_1_1Core_1_1Browser_1_1ArchiveNavigator.html#aae24373b6c0bbb21fcd8a1cf051e29af", [
                    [ "___INVALID", "classPackuru_1_1Core_1_1Browser_1_1ArchiveNavigator.html#aae24373b6c0bbb21fcd8a1cf051e29afa6e545caba6d4293f6c360bc2e183bd64", null ],
                    [ "Close", "classPackuru_1_1Core_1_1Browser_1_1ArchiveNavigator.html#aae24373b6c0bbb21fcd8a1cf051e29afad3d2e617335f08df83599665eef8a418", null ]
                  ] ],
                  [ "ArchiveNavigator", "classPackuru_1_1Core_1_1Browser_1_1ArchiveNavigator.html#ad875f733274a0fcd3c625dba926c9030", null ],
                  [ "~ArchiveNavigator", "classPackuru_1_1Core_1_1Browser_1_1ArchiveNavigator.html#aeef7f6b60e13c4c18c5247005c289c77", null ],
                  [ "canGoUpChanged", "classPackuru_1_1Core_1_1Browser_1_1ArchiveNavigator.html#ae0a26930608c6a7010f7a91550e95b1b", null ],
                  [ "changeDir", "classPackuru_1_1Core_1_1Browser_1_1ArchiveNavigator.html#aef103219effad1780c7d8b762ae9a8eb", null ],
                  [ "currentPathChanged", "classPackuru_1_1Core_1_1Browser_1_1ArchiveNavigator.html#ab7b83febf474b0a65ef1e7b3bf652c48", null ],
                  [ "currentPathIndexChanged", "classPackuru_1_1Core_1_1Browser_1_1ArchiveNavigator.html#aca4d7ad884680505f1f06876515b25ec", null ],
                  [ "getCanGoUp", "classPackuru_1_1Core_1_1Browser_1_1ArchiveNavigator.html#ac78e2bf71afb9ec69c8861acb5747164", null ],
                  [ "getCurrentPath", "classPackuru_1_1Core_1_1Browser_1_1ArchiveNavigator.html#a06afa8f380f7576e971c9d66469d5ad7", null ],
                  [ "getCurrentPathIndex", "classPackuru_1_1Core_1_1Browser_1_1ArchiveNavigator.html#a603a61670514c435aa7ea9dfcae294fd", null ],
                  [ "getFilterControllerEngine", "classPackuru_1_1Core_1_1Browser_1_1ArchiveNavigator.html#a9e6aee41e12d73a8ca6b86f68ec9969a", null ],
                  [ "getModel", "classPackuru_1_1Core_1_1Browser_1_1ArchiveNavigator.html#aebd364d75e9eac692769dd30679abb9e", null ],
                  [ "getModelDefaultSections", "classPackuru_1_1Core_1_1Browser_1_1ArchiveNavigator.html#a2174bdc6e3c4008ed25aefc0307c1575", null ],
                  [ "getPreviousPathIndexSelection", "classPackuru_1_1Core_1_1Browser_1_1ArchiveNavigator.html#a76431072975ab981b94dd84d53521e6f", null ],
                  [ "getSelectionModel", "classPackuru_1_1Core_1_1Browser_1_1ArchiveNavigator.html#a2f00a733f2ef3d5cd8a2aeba08daaf1e", null ],
                  [ "getString", "classPackuru_1_1Core_1_1Browser_1_1ArchiveNavigator.html#a572e58bd9d208f4fd37e800a18d11de0", null ],
                  [ "goUp", "classPackuru_1_1Core_1_1Browser_1_1ArchiveNavigator.html#a5a17d4b22bebd99951af4c2422022844", null ],
                  [ "openItem", "classPackuru_1_1Core_1_1Browser_1_1ArchiveNavigator.html#ab81a9ad4707dfc791a2f39befb9cc81d", null ],
                  [ "previousPathIndexSelectionChanged", "classPackuru_1_1Core_1_1Browser_1_1ArchiveNavigator.html#ad93fed6f71e48515b25665dd6eb8fc9d", null ],
                  [ "setPreviousPathIndexSelection", "classPackuru_1_1Core_1_1Browser_1_1ArchiveNavigator.html#aec1663f05a38958212435ad61dd3e7dc", null ],
                  [ "sortModel", "classPackuru_1_1Core_1_1Browser_1_1ArchiveNavigator.html#a961fe28fc060ed5cdc1e20524714e294", null ],
                  [ "canGoUp", "classPackuru_1_1Core_1_1Browser_1_1ArchiveNavigator.html#a988b3f61f0cc56085cf07e0f6c477f4c", null ],
                  [ "currentPath", "classPackuru_1_1Core_1_1Browser_1_1ArchiveNavigator.html#ab6189b2f022d5a2ecfcde6857c5b9bf8", null ],
                  [ "currentPathIndex", "classPackuru_1_1Core_1_1Browser_1_1ArchiveNavigator.html#aae9263b173ce7331b7218e844db5b1bc", null ],
                  [ "model", "classPackuru_1_1Core_1_1Browser_1_1ArchiveNavigator.html#ab565f00c2e1a51ba5511d0225bd1bab8", null ],
                  [ "previousPathIndexSelection", "classPackuru_1_1Core_1_1Browser_1_1ArchiveNavigator.html#a2874aa8d4ea5c846429d66dfd19e7865", null ],
                  [ "selectionModel", "classPackuru_1_1Core_1_1Browser_1_1ArchiveNavigator.html#add429d891c9324f479e3f8613048810a", null ]
                ] ]
              ] ],
              [ "basiccontrol.h", "basiccontrol_8h.html", [
                [ "BasicControl", "classPackuru_1_1Core_1_1Browser_1_1BasicControl.html", [
                  [ "DefaultButton", "classPackuru_1_1Core_1_1Browser_1_1BasicControl.html#a24a75c2caa7249dc6132ca82060f3ee8", [
                    [ "___INVALID", "classPackuru_1_1Core_1_1Browser_1_1BasicControl.html#a24a75c2caa7249dc6132ca82060f3ee8a6e545caba6d4293f6c360bc2e183bd64", null ],
                    [ "Abort", "classPackuru_1_1Core_1_1Browser_1_1BasicControl.html#a24a75c2caa7249dc6132ca82060f3ee8a727b63583e01fa2b3952dab580c84dc2", null ],
                    [ "BrowseArchive", "classPackuru_1_1Core_1_1Browser_1_1BasicControl.html#a24a75c2caa7249dc6132ca82060f3ee8aab49bb4d9d59db86b051a5428504492c", null ],
                    [ "Retry", "classPackuru_1_1Core_1_1Browser_1_1BasicControl.html#a24a75c2caa7249dc6132ca82060f3ee8a6327b4e59f58137083214a1fec358855", null ],
                    [ "ViewLog", "classPackuru_1_1Core_1_1Browser_1_1BasicControl.html#a24a75c2caa7249dc6132ca82060f3ee8a28dae093f6dd5798e896c2fbfe16836e", null ]
                  ] ],
                  [ "BasicControl", "classPackuru_1_1Core_1_1Browser_1_1BasicControl.html#a73a6429d8e8683abcaf307edce68a409", null ],
                  [ "~BasicControl", "classPackuru_1_1Core_1_1Browser_1_1BasicControl.html#acad4fbc56cb600ecbf4e7564f7d0a0c2", null ],
                  [ "abort", "classPackuru_1_1Core_1_1Browser_1_1BasicControl.html#a220fbc82bcea18cde1d84500470b38d1", null ],
                  [ "browse", "classPackuru_1_1Core_1_1Browser_1_1BasicControl.html#ad02d74b0df11febdc696172cf6a22b83", null ],
                  [ "defaultButtonChanged", "classPackuru_1_1Core_1_1Browser_1_1BasicControl.html#a052cb602fff0f71b514a0f8eb9285d4a", null ],
                  [ "getControllerEngine", "classPackuru_1_1Core_1_1Browser_1_1BasicControl.html#a72d2d5dda34983ff5afcb6039a09bdbb", null ],
                  [ "getDefaultButton", "classPackuru_1_1Core_1_1Browser_1_1BasicControl.html#aa33cbcbcb61969da84acde9c785a2cb2", null ],
                  [ "getLog", "classPackuru_1_1Core_1_1Browser_1_1BasicControl.html#a9a878fed611a5ca9e06a6e5974d11387", null ],
                  [ "retry", "classPackuru_1_1Core_1_1Browser_1_1BasicControl.html#a5619747c1e923b27c794f221811ef88a", null ],
                  [ "defaultButton", "classPackuru_1_1Core_1_1Browser_1_1BasicControl.html#aaba7d939f104fde296c49db2f3be9c91", null ]
                ] ]
              ] ],
              [ "basiccontrolcontrollertype.h", "basiccontrolcontrollertype_8h.html", [
                [ "BasicControlControllerType", "classPackuru_1_1Core_1_1Browser_1_1BasicControlControllerType.html", [
                  [ "Type", "classPackuru_1_1Core_1_1Browser_1_1BasicControlControllerType.html#a1d1cfd8ffb84e947f82999c682b666a7", [
                    [ "AbortButton", "classPackuru_1_1Core_1_1Browser_1_1BasicControlControllerType.html#a1d1cfd8ffb84e947f82999c682b666a7a41096bc836b5e9230aa0ccc09ab5b6ea", null ],
                    [ "BrowserButton", "classPackuru_1_1Core_1_1Browser_1_1BasicControlControllerType.html#a1d1cfd8ffb84e947f82999c682b666a7ae0a571867df971422ade5d99985da31d", null ],
                    [ "ErrorsWarningsLabel", "classPackuru_1_1Core_1_1Browser_1_1BasicControlControllerType.html#a1d1cfd8ffb84e947f82999c682b666a7a58e9643867b2fae9213efe075c304794", null ],
                    [ "RetryButton", "classPackuru_1_1Core_1_1Browser_1_1BasicControlControllerType.html#a1d1cfd8ffb84e947f82999c682b666a7a8dec4d6f30ae94ff4f300af9013b2351", null ],
                    [ "ViewLogButton", "classPackuru_1_1Core_1_1Browser_1_1BasicControlControllerType.html#a1d1cfd8ffb84e947f82999c682b666a7ab013339acd6f8385dc10652aafe4699d", null ]
                  ] ]
                ] ]
              ] ],
              [ "deletiondialogcontrollertype.h", "deletiondialogcontrollertype_8h.html", [
                [ "DeletionDialogControllerType", "classPackuru_1_1Core_1_1Browser_1_1DeletionDialogControllerType.html", [
                  [ "Type", "classPackuru_1_1Core_1_1Browser_1_1DeletionDialogControllerType.html#a1d1cfd8ffb84e947f82999c682b666a7", [
                    [ "Password", "classPackuru_1_1Core_1_1Browser_1_1DeletionDialogControllerType.html#a1d1cfd8ffb84e947f82999c682b666a7adc647eb65e6711e155375218212b3964", null ],
                    [ "RunInQueue", "classPackuru_1_1Core_1_1Browser_1_1DeletionDialogControllerType.html#a1d1cfd8ffb84e947f82999c682b666a7a3e2ffba79e6d032b1c075e396fc70e1b", null ]
                  ] ]
                ] ],
                [ "qHash", "deletiondialogcontrollertype_8h.html#acdb04b063306ea6592ad0be303667ce6", null ]
              ] ],
              [ "deletiondialogcore.h", "deletiondialogcore_8h.html", [
                [ "DeletionDialogCore", "classPackuru_1_1Core_1_1Browser_1_1DeletionDialogCore.html", [
                  [ "UserString", "classPackuru_1_1Core_1_1Browser_1_1DeletionDialogCore.html#aae24373b6c0bbb21fcd8a1cf051e29af", [
                    [ "___INVALID", "classPackuru_1_1Core_1_1Browser_1_1DeletionDialogCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa6e545caba6d4293f6c360bc2e183bd64", null ],
                    [ "Cancel", "classPackuru_1_1Core_1_1Browser_1_1DeletionDialogCore.html#aae24373b6c0bbb21fcd8a1cf051e29afaea4788705e6873b424c65e91c2846b19", null ],
                    [ "DialogQuestion", "classPackuru_1_1Core_1_1Browser_1_1DeletionDialogCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa999d248c759f533003da12d4d8a7a5db", null ],
                    [ "DialogTitle", "classPackuru_1_1Core_1_1Browser_1_1DeletionDialogCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa59e6edcf06044686b74fc871f4098d0e", null ]
                  ] ],
                  [ "DeletionDialogCore", "classPackuru_1_1Core_1_1Browser_1_1DeletionDialogCore.html#a76ff4a3c669bdffb94074d3c67ac3d8f", null ],
                  [ "~DeletionDialogCore", "classPackuru_1_1Core_1_1Browser_1_1DeletionDialogCore.html#a9683652e1738277ded62c17bd6d8cf11", null ],
                  [ "accept", "classPackuru_1_1Core_1_1Browser_1_1DeletionDialogCore.html#a74ddbe2ce21e10cdd5e4586e2e9f0607", null ],
                  [ "destroyLater", "classPackuru_1_1Core_1_1Browser_1_1DeletionDialogCore.html#aa5f97b4f6309ac39ce270d0ac0f92d50", null ],
                  [ "getControllerEngine", "classPackuru_1_1Core_1_1Browser_1_1DeletionDialogCore.html#a72d2d5dda34983ff5afcb6039a09bdbb", null ],
                  [ "getFileModel", "classPackuru_1_1Core_1_1Browser_1_1DeletionDialogCore.html#ac009248b1e2c5fd0df6ac227f4aa45d8", null ]
                ] ]
              ] ],
              [ "init.h", "core-browser_2init_8h.html", [
                [ "Init", "classPackuru_1_1Core_1_1Browser_1_1Init.html", [
                  [ "Init", "classPackuru_1_1Core_1_1Browser_1_1Init.html#ab7ab6b6595f2fe7563a2484e7e9a92f1", null ],
                  [ "~Init", "classPackuru_1_1Core_1_1Browser_1_1Init.html#a451d43761b3b3eeae21dd5559eeb8fb5", null ],
                  [ "getMainWindowCore", "classPackuru_1_1Core_1_1Browser_1_1Init.html#abfa4078ed385965e314a1e0ae61e34be", null ],
                  [ "messagePrimaryInstance", "classPackuru_1_1Core_1_1Browser_1_1Init.html#aea87a1bde153a87fcac831856fc448d8", null ],
                  [ "processArguments", "classPackuru_1_1Core_1_1Browser_1_1Init.html#af1694f156f54fda65cf39d1a62a14bb5", null ]
                ] ]
              ] ],
              [ "mainwindowcore.h", "core-browser_2mainwindowcore_8h.html", [
                [ "MainWindowCore", "classPackuru_1_1Core_1_1Browser_1_1MainWindowCore.html", [
                  [ "UserString", "classPackuru_1_1Core_1_1Browser_1_1MainWindowCore.html#aae24373b6c0bbb21fcd8a1cf051e29af", [
                    [ "___INVALID", "classPackuru_1_1Core_1_1Browser_1_1MainWindowCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa6e545caba6d4293f6c360bc2e183bd64", null ],
                    [ "ActionAbout", "classPackuru_1_1Core_1_1Browser_1_1MainWindowCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa223138955c9c0ef19f7b9eecd2f9bfc4", null ],
                    [ "ActionClose", "classPackuru_1_1Core_1_1Browser_1_1MainWindowCore.html#aae24373b6c0bbb21fcd8a1cf051e29afabcbb21e925d5eb7d17a949db6a852b5d", null ],
                    [ "ActionNew", "classPackuru_1_1Core_1_1Browser_1_1MainWindowCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa98ff5ebe2ae053cf1e008e791a4cf56e", null ],
                    [ "ActionOpen", "classPackuru_1_1Core_1_1Browser_1_1MainWindowCore.html#aae24373b6c0bbb21fcd8a1cf051e29afafc311944a280b5ce38182b7fb4237f4c", null ],
                    [ "ActionQuit", "classPackuru_1_1Core_1_1Browser_1_1MainWindowCore.html#aae24373b6c0bbb21fcd8a1cf051e29afae2edcf3a4e74ed1dbd7dab13ee58c7ab", null ],
                    [ "ActionSettings", "classPackuru_1_1Core_1_1Browser_1_1MainWindowCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa8a2f945561854bddbd40ca2b4b07cb33", null ],
                    [ "WindowTitle", "classPackuru_1_1Core_1_1Browser_1_1MainWindowCore.html#aae24373b6c0bbb21fcd8a1cf051e29afabfeec2b56d1c63a77acc8b2af0ef248e", null ]
                  ] ],
                  [ "MainWindowCore", "classPackuru_1_1Core_1_1Browser_1_1MainWindowCore.html#a8820fceb4da44fc9e86d7237025e3dac", null ],
                  [ "~MainWindowCore", "classPackuru_1_1Core_1_1Browser_1_1MainWindowCore.html#a86f5b1c3c4a89d19f46b58a7af224106", null ],
                  [ "commandLineError", "classPackuru_1_1Core_1_1Browser_1_1MainWindowCore.html#a7ecc760fd11ca5ea8fda293fc82b9c0c", null ],
                  [ "commandLineHelpRequested", "classPackuru_1_1Core_1_1Browser_1_1MainWindowCore.html#aa206621758a6fd0fb7954e27d00ed3f8", null ],
                  [ "createAppSettingsDialogCore", "classPackuru_1_1Core_1_1Browser_1_1MainWindowCore.html#a429ed735e31b974d11632492c54ff600", null ],
                  [ "createArchive", "classPackuru_1_1Core_1_1Browser_1_1MainWindowCore.html#adf462c90bda9e42b34e92b032bb46847", null ],
                  [ "createBrowserSettingsDialogCore", "classPackuru_1_1Core_1_1Browser_1_1MainWindowCore.html#a7a6edb296afed0b25a4778c4cb947d53", null ],
                  [ "getBusyBrowserCount", "classPackuru_1_1Core_1_1Browser_1_1MainWindowCore.html#aac2c1c0581dac85c3ed8e5de17625af8", null ],
                  [ "getQueueMessenger", "classPackuru_1_1Core_1_1Browser_1_1MainWindowCore.html#aa2039b6bb36fbf538b6a3197d3a53bc1", null ],
                  [ "getString", "classPackuru_1_1Core_1_1Browser_1_1MainWindowCore.html#a803c0e47fdbaebbcca9c0f4e5f4ddd27", null ],
                  [ "newBrowserCoreCreated", "classPackuru_1_1Core_1_1Browser_1_1MainWindowCore.html#ad55277600e8f642959f528faf4ce474d", null ],
                  [ "newMessage", "classPackuru_1_1Core_1_1Browser_1_1MainWindowCore.html#a4d147bec8de4813d3aeb0d71bf159f68", null ],
                  [ "openArchives", "classPackuru_1_1Core_1_1Browser_1_1MainWindowCore.html#a89ce54a606ebad8f59e5f9ce54ea94d4", null ],
                  [ "openArchives", "classPackuru_1_1Core_1_1Browser_1_1MainWindowCore.html#a8285679a8a0fcc6210ec5c1010c0a45b", null ],
                  [ "openArchives", "classPackuru_1_1Core_1_1Browser_1_1MainWindowCore.html#a437875e971d239b7f0bc0c0365a63179", null ]
                ] ]
              ] ],
              [ "queuemessenger.h", "queuemessenger_8h.html", [
                [ "QueueMessenger", "classPackuru_1_1Core_1_1Browser_1_1QueueMessenger.html", [
                  [ "UserString", "classPackuru_1_1Core_1_1Browser_1_1QueueMessenger.html#aae24373b6c0bbb21fcd8a1cf051e29af", [
                    [ "___INVALID", "classPackuru_1_1Core_1_1Browser_1_1QueueMessenger.html#aae24373b6c0bbb21fcd8a1cf051e29afa6e545caba6d4293f6c360bc2e183bd64", null ],
                    [ "Abort", "classPackuru_1_1Core_1_1Browser_1_1QueueMessenger.html#aae24373b6c0bbb21fcd8a1cf051e29afa727b63583e01fa2b3952dab580c84dc2", null ],
                    [ "DialogTitle", "classPackuru_1_1Core_1_1Browser_1_1QueueMessenger.html#aae24373b6c0bbb21fcd8a1cf051e29afa59e6edcf06044686b74fc871f4098d0e", null ],
                    [ "RunInBrowser", "classPackuru_1_1Core_1_1Browser_1_1QueueMessenger.html#aae24373b6c0bbb21fcd8a1cf051e29afa1d989eaf8f6b9c606fa44d787f5deca5", null ]
                  ] ],
                  [ "QueueMessenger", "classPackuru_1_1Core_1_1Browser_1_1QueueMessenger.html#a4c02b54a2f1460394146a27abf0e05a0", null ],
                  [ "~QueueMessenger", "classPackuru_1_1Core_1_1Browser_1_1QueueMessenger.html#a44eb1db37279ede50e382b5c3bb0e83c", null ],
                  [ "abort", "classPackuru_1_1Core_1_1Browser_1_1QueueMessenger.html#a220fbc82bcea18cde1d84500470b38d1", null ],
                  [ "activated", "classPackuru_1_1Core_1_1Browser_1_1QueueMessenger.html#a244e631e4637625aaca7abf94c04d4d1", null ],
                  [ "activeChanged", "classPackuru_1_1Core_1_1Browser_1_1QueueMessenger.html#a56ce57451f645b6167547a0c8420fc54", null ],
                  [ "canRunInBrowserChanged", "classPackuru_1_1Core_1_1Browser_1_1QueueMessenger.html#a982a6a4359613f78f54ee60b46009acf", null ],
                  [ "deactivated", "classPackuru_1_1Core_1_1Browser_1_1QueueMessenger.html#acfd4ec43396d90d144a6b27bfe409465", null ],
                  [ "getCanRunInBrowser", "classPackuru_1_1Core_1_1Browser_1_1QueueMessenger.html#aea636c48fba10f150bad4ab78b9b4164", null ],
                  [ "getStatus", "classPackuru_1_1Core_1_1Browser_1_1QueueMessenger.html#ad0d914f1624c77ed8eb149f1ee698a40", null ],
                  [ "isActive", "classPackuru_1_1Core_1_1Browser_1_1QueueMessenger.html#adb80df6eeafd900dfefcf90c56efad51", null ],
                  [ "runInBrowser", "classPackuru_1_1Core_1_1Browser_1_1QueueMessenger.html#a0b21e846c084fc48ff34e24625fcab3b", null ],
                  [ "statusChanged", "classPackuru_1_1Core_1_1Browser_1_1QueueMessenger.html#ad6ae348da15f16797bd854667b67ceee", null ],
                  [ "active", "classPackuru_1_1Core_1_1Browser_1_1QueueMessenger.html#a03c996f9fcf0e10baeb3e700be0c409a", null ],
                  [ "canRunInBrowser", "classPackuru_1_1Core_1_1Browser_1_1QueueMessenger.html#a61caedf8a282ceffb57f96f7768d9f79", null ],
                  [ "status", "classPackuru_1_1Core_1_1Browser_1_1QueueMessenger.html#ac239e35c6fdaead7d9011c035268dadd", null ]
                ] ]
              ] ],
              [ "settingscontrollertype.h", "core-browser_2settingscontrollertype_8h.html", [
                [ "SettingsControllerType", "classPackuru_1_1Core_1_1Browser_1_1SettingsControllerType.html", [
                  [ "Type", "classPackuru_1_1Core_1_1Browser_1_1SettingsControllerType.html#a1d1cfd8ffb84e947f82999c682b666a7", [
                    [ "ForceSingleInstance", "classPackuru_1_1Core_1_1Browser_1_1SettingsControllerType.html#a1d1cfd8ffb84e947f82999c682b666a7ae64517427e61b70bd8aa5c6b3708d09a", null ],
                    [ "ShowSummaryOnTaskCompletion", "classPackuru_1_1Core_1_1Browser_1_1SettingsControllerType.html#a1d1cfd8ffb84e947f82999c682b666a7a134e324704f9dc8589617e54b75baccd", null ]
                  ] ]
                ] ]
              ] ],
              [ "settingsdialogcore.h", "core-browser_2settingsdialogcore_8h.html", [
                [ "SettingsDialogCore", "classPackuru_1_1Core_1_1Browser_1_1SettingsDialogCore.html", [
                  [ "UserString", "classPackuru_1_1Core_1_1Browser_1_1SettingsDialogCore.html#aae24373b6c0bbb21fcd8a1cf051e29af", [
                    [ "___INVALID", "classPackuru_1_1Core_1_1Browser_1_1SettingsDialogCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa6e545caba6d4293f6c360bc2e183bd64", null ],
                    [ "PageTitle", "classPackuru_1_1Core_1_1Browser_1_1SettingsDialogCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa57a08c88b9aff49612ca6b19fd8bea90", null ]
                  ] ],
                  [ "SettingsDialogCore", "classPackuru_1_1Core_1_1Browser_1_1SettingsDialogCore.html#ac5701d5ae1efc89d382b324e4ea2d3b8", null ],
                  [ "~SettingsDialogCore", "classPackuru_1_1Core_1_1Browser_1_1SettingsDialogCore.html#a0f841e3d6a6cdaf9837a251777d66af6", null ],
                  [ "accept", "classPackuru_1_1Core_1_1Browser_1_1SettingsDialogCore.html#a74ddbe2ce21e10cdd5e4586e2e9f0607", null ],
                  [ "destroyLater", "classPackuru_1_1Core_1_1Browser_1_1SettingsDialogCore.html#aa5f97b4f6309ac39ce270d0ac0f92d50", null ],
                  [ "getControllerEngine", "classPackuru_1_1Core_1_1Browser_1_1SettingsDialogCore.html#a72d2d5dda34983ff5afcb6039a09bdbb", null ]
                ] ]
              ] ],
              [ "statuspagecore.h", "statuspagecore_8h.html", [
                [ "StatusPageCore", "classPackuru_1_1Core_1_1Browser_1_1StatusPageCore.html", [
                  [ "StatusPageCore", "classPackuru_1_1Core_1_1Browser_1_1StatusPageCore.html#a40c513de3307d120d1aecdbfdd573b73", null ],
                  [ "~StatusPageCore", "classPackuru_1_1Core_1_1Browser_1_1StatusPageCore.html#af965d740caeb5995b2c73011d8904020", null ],
                  [ "getBasicControl", "classPackuru_1_1Core_1_1Browser_1_1StatusPageCore.html#a8a36ef4e747f1875b5a0787a831fb925", null ],
                  [ "getProgressBarVisible", "classPackuru_1_1Core_1_1Browser_1_1StatusPageCore.html#a40bcf1e12ce7db5587c08fdd87bb0ab2", null ],
                  [ "getTaskDescription", "classPackuru_1_1Core_1_1Browser_1_1StatusPageCore.html#a494c42a825c2ffd504449424d09cbc4b", null ],
                  [ "getTaskProgress", "classPackuru_1_1Core_1_1Browser_1_1StatusPageCore.html#a9a257b3532387654746c8f810e3bcdb9", null ],
                  [ "getTaskState", "classPackuru_1_1Core_1_1Browser_1_1StatusPageCore.html#ac5a1dc77e1abafd36428775d3dae8c36", null ],
                  [ "progressBarVisibleChanged", "classPackuru_1_1Core_1_1Browser_1_1StatusPageCore.html#a2974bc1f2b81bd41ebbe933a18c095ce", null ],
                  [ "showBasicControl", "classPackuru_1_1Core_1_1Browser_1_1StatusPageCore.html#aab7dda5ee791d12d2a371120ef66de68", null ],
                  [ "showFileExistsPrompt", "classPackuru_1_1Core_1_1Browser_1_1StatusPageCore.html#a10044f78671170cc06fcefb11a183089", null ],
                  [ "showFolderExistsPrompt", "classPackuru_1_1Core_1_1Browser_1_1StatusPageCore.html#a157adb894d6ff84e6b7c10a5bee55893", null ],
                  [ "showItemTypeMismatchPrompt", "classPackuru_1_1Core_1_1Browser_1_1StatusPageCore.html#a474c9e79b435e2cca3b96362dd417cf8", null ],
                  [ "showPasswordPrompt", "classPackuru_1_1Core_1_1Browser_1_1StatusPageCore.html#ab24dd3ded9c5283ea9fc6701dfa7633e", null ],
                  [ "taskDescriptionChanged", "classPackuru_1_1Core_1_1Browser_1_1StatusPageCore.html#a1aee599ef614703adaaba2c9e23233df", null ],
                  [ "taskProgressChanged", "classPackuru_1_1Core_1_1Browser_1_1StatusPageCore.html#aff8ed851abc98cad04e8c5143fd4c0f4", null ],
                  [ "taskStateChanged", "classPackuru_1_1Core_1_1Browser_1_1StatusPageCore.html#a9c210d2ce4ca42a7c0ff5c064a2b1574", null ],
                  [ "basicControl", "classPackuru_1_1Core_1_1Browser_1_1StatusPageCore.html#af0eed66356a41be25205e3fb32d60a28", null ],
                  [ "progressBarVisible", "classPackuru_1_1Core_1_1Browser_1_1StatusPageCore.html#ad39364f25c67a631b9e814fa92b31c2a", null ],
                  [ "taskDescription", "classPackuru_1_1Core_1_1Browser_1_1StatusPageCore.html#a7755def137e650ae374fba38ae0558ed", null ],
                  [ "taskProgress", "classPackuru_1_1Core_1_1Browser_1_1StatusPageCore.html#a1456590ec5da6a8cdd21a6e5085c27e2", null ],
                  [ "taskState", "classPackuru_1_1Core_1_1Browser_1_1StatusPageCore.html#adb9b324dde3d0328af7af7481da25176", null ]
                ] ]
              ] ],
              [ "tabclosingdialogcore.h", "tabclosingdialogcore_8h.html", [
                [ "TabClosingDialogCore", "classPackuru_1_1Core_1_1Browser_1_1TabClosingDialogCore.html", [
                  [ "UserString", "classPackuru_1_1Core_1_1Browser_1_1TabClosingDialogCore.html#aae24373b6c0bbb21fcd8a1cf051e29af", [
                    [ "___INVALID", "classPackuru_1_1Core_1_1Browser_1_1TabClosingDialogCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa6e545caba6d4293f6c360bc2e183bd64", null ],
                    [ "Cancel", "classPackuru_1_1Core_1_1Browser_1_1TabClosingDialogCore.html#aae24373b6c0bbb21fcd8a1cf051e29afaea4788705e6873b424c65e91c2846b19", null ],
                    [ "Close", "classPackuru_1_1Core_1_1Browser_1_1TabClosingDialogCore.html#aae24373b6c0bbb21fcd8a1cf051e29afad3d2e617335f08df83599665eef8a418", null ],
                    [ "DialogTitle", "classPackuru_1_1Core_1_1Browser_1_1TabClosingDialogCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa59e6edcf06044686b74fc871f4098d0e", null ],
                    [ "DoYouWantToCloseIt", "classPackuru_1_1Core_1_1Browser_1_1TabClosingDialogCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa6c2f589a66f5d1badac5ae0170cb0a49", null ],
                    [ "ThisTabIsBusy", "classPackuru_1_1Core_1_1Browser_1_1TabClosingDialogCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa996758d9a293210523bd070a48c476cb", null ]
                  ] ],
                  [ "TabClosingDialogCore", "classPackuru_1_1Core_1_1Browser_1_1TabClosingDialogCore.html#a96cfd2325d7f154bc5683ab602c08e69", null ]
                ] ]
              ] ],
              [ "viewfiltercontrollertype.h", "viewfiltercontrollertype_8h.html", [
                [ "ViewFilterControllerType", "classPackuru_1_1Core_1_1Browser_1_1ViewFilterControllerType.html", [
                  [ "Type", "classPackuru_1_1Core_1_1Browser_1_1ViewFilterControllerType.html#a1d1cfd8ffb84e947f82999c682b666a7", [
                    [ "FilterFiles", "classPackuru_1_1Core_1_1Browser_1_1ViewFilterControllerType.html#a1d1cfd8ffb84e947f82999c682b666a7a2981599b9df1f02d9e113e1c90d06eef", null ],
                    [ "FilterFolders", "classPackuru_1_1Core_1_1Browser_1_1ViewFilterControllerType.html#a1d1cfd8ffb84e947f82999c682b666a7a3b41c5b816c09fad2f5ed0eb29bf7875", null ],
                    [ "FilterScope", "classPackuru_1_1Core_1_1Browser_1_1ViewFilterControllerType.html#a1d1cfd8ffb84e947f82999c682b666a7a806fef84c314e95fdaf4cfce28f3ef5b", null ],
                    [ "FilterText", "classPackuru_1_1Core_1_1Browser_1_1ViewFilterControllerType.html#a1d1cfd8ffb84e947f82999c682b666a7aadc15cb731ea0e6ec61cd2a1f933dca5", null ]
                  ] ]
                ] ],
                [ "qHash", "viewfiltercontrollertype_8h.html#a8550dfdf3512d0158e7170321f4e28c6", null ]
              ] ]
            ] ],
            [ "core-queue", "dir_c113aa9f15df3532312d5a8a5b3c444c.html", [
              [ "init.h", "core-queue_2init_8h.html", [
                [ "Init", "classPackuru_1_1Core_1_1Queue_1_1Init.html", [
                  [ "Init", "classPackuru_1_1Core_1_1Queue_1_1Init.html#a1bdcf02cbfc67a99f726eec8c57fed44", null ],
                  [ "~Init", "classPackuru_1_1Core_1_1Queue_1_1Init.html#a970c1ac01b53341336e950772d76669d", null ],
                  [ "getQueueCore", "classPackuru_1_1Core_1_1Queue_1_1Init.html#a5de0743cb1f27466f8c63d5fa7dea147", null ],
                  [ "messagePrimaryInstance", "classPackuru_1_1Core_1_1Queue_1_1Init.html#aea87a1bde153a87fcac831856fc448d8", null ],
                  [ "processArguments", "classPackuru_1_1Core_1_1Queue_1_1Init.html#af1694f156f54fda65cf39d1a62a14bb5", null ]
                ] ]
              ] ],
              [ "mainwindowcore.h", "core-queue_2mainwindowcore_8h.html", [
                [ "MainWindowCore", "classPackuru_1_1Core_1_1Queue_1_1MainWindowCore.html", [
                  [ "UserString", "classPackuru_1_1Core_1_1Queue_1_1MainWindowCore.html#aae24373b6c0bbb21fcd8a1cf051e29af", [
                    [ "___INVALID", "classPackuru_1_1Core_1_1Queue_1_1MainWindowCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa6e545caba6d4293f6c360bc2e183bd64", null ],
                    [ "ActionAbout", "classPackuru_1_1Core_1_1Queue_1_1MainWindowCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa223138955c9c0ef19f7b9eecd2f9bfc4", null ],
                    [ "ActionExtract", "classPackuru_1_1Core_1_1Queue_1_1MainWindowCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa7f2a064ae66cff8649053b333b33de7b", null ],
                    [ "ActionNew", "classPackuru_1_1Core_1_1Queue_1_1MainWindowCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa98ff5ebe2ae053cf1e008e791a4cf56e", null ],
                    [ "ActionQuit", "classPackuru_1_1Core_1_1Queue_1_1MainWindowCore.html#aae24373b6c0bbb21fcd8a1cf051e29afae2edcf3a4e74ed1dbd7dab13ee58c7ab", null ],
                    [ "ActionSettings", "classPackuru_1_1Core_1_1Queue_1_1MainWindowCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa8a2f945561854bddbd40ca2b4b07cb33", null ],
                    [ "ActionTest", "classPackuru_1_1Core_1_1Queue_1_1MainWindowCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa107f1c4fc93e51f8d693fa4c9eabac77", null ],
                    [ "WindowTitle", "classPackuru_1_1Core_1_1Queue_1_1MainWindowCore.html#aae24373b6c0bbb21fcd8a1cf051e29afabfeec2b56d1c63a77acc8b2af0ef248e", null ]
                  ] ],
                  [ "MainWindowCore", "classPackuru_1_1Core_1_1Queue_1_1MainWindowCore.html#a1a864a61662d070daa97da45ccd35449", null ],
                  [ "~MainWindowCore", "classPackuru_1_1Core_1_1Queue_1_1MainWindowCore.html#a86f5b1c3c4a89d19f46b58a7af224106", null ],
                  [ "allTasksCompletedReadyToClose", "classPackuru_1_1Core_1_1Queue_1_1MainWindowCore.html#a284880af28e1c7081dca91e15eddbb47", null ],
                  [ "getTaskQueueModel", "classPackuru_1_1Core_1_1Queue_1_1MainWindowCore.html#a8879298f727d96d8f3d58ef568b43dc7", null ]
                ] ]
              ] ],
              [ "queuecore.h", "queuecore_8h.html", [
                [ "QueueCore", "classPackuru_1_1Core_1_1Queue_1_1QueueCore.html", [
                  [ "QueueCore", "classPackuru_1_1Core_1_1Queue_1_1QueueCore.html#a496e0a03e027358308d8e036ebfaf4b2", null ],
                  [ "~QueueCore", "classPackuru_1_1Core_1_1Queue_1_1QueueCore.html#ad0485dfd874c059db6b7eca001ae76b6", null ],
                  [ "archivingDialogCoreCreated", "classPackuru_1_1Core_1_1Queue_1_1QueueCore.html#ac56b60c0e2ffaca0a966f44e11e8f777", null ],
                  [ "commandLineError", "classPackuru_1_1Core_1_1Queue_1_1QueueCore.html#a90aa69260286b0d6341eb32a4a0273ce", null ],
                  [ "commandLineHelpRequested", "classPackuru_1_1Core_1_1Queue_1_1QueueCore.html#aa206621758a6fd0fb7954e27d00ed3f8", null ],
                  [ "createAppSettingsDialogCore", "classPackuru_1_1Core_1_1Queue_1_1QueueCore.html#a429ed735e31b974d11632492c54ff600", null ],
                  [ "createArchivingDialogCore", "classPackuru_1_1Core_1_1Queue_1_1QueueCore.html#aa469eb7c512584e02e802c6bb21e80b5", null ],
                  [ "createExtractionDialogCore", "classPackuru_1_1Core_1_1Queue_1_1QueueCore.html#a700149f85c2a574366744f64084192d4", null ],
                  [ "createQueueSettingsDialogCore", "classPackuru_1_1Core_1_1Queue_1_1QueueCore.html#a9452a1449ace31158d5815422cfd2995", null ],
                  [ "createTestDialogCore", "classPackuru_1_1Core_1_1Queue_1_1QueueCore.html#a1826e9d2b6810e1e466b7a11f2fc5f27", null ],
                  [ "defaultStartUp", "classPackuru_1_1Core_1_1Queue_1_1QueueCore.html#a84a3f5dcac1e9ba0e946b25363bb86cb", null ],
                  [ "extractionDialogCoreCreated", "classPackuru_1_1Core_1_1Queue_1_1QueueCore.html#a74fa4e7bd02b29ea2088927456badb16", null ],
                  [ "getMainWindowCore", "classPackuru_1_1Core_1_1Queue_1_1QueueCore.html#a3bc3780dfb5183815bd888fa90ed50c7", null ],
                  [ "newTasks", "classPackuru_1_1Core_1_1Queue_1_1QueueCore.html#aa8d0ddf089947ded76e04e74b3446167", null ],
                  [ "testDialogCoreCreated", "classPackuru_1_1Core_1_1Queue_1_1QueueCore.html#a48f6ea6c856f8ad345691276cff76633", null ]
                ] ]
              ] ],
              [ "settingscontrollertype.h", "core-queue_2settingscontrollertype_8h.html", [
                [ "SettingsControllerType", "classPackuru_1_1Core_1_1Queue_1_1SettingsControllerType.html", [
                  [ "Type", "classPackuru_1_1Core_1_1Queue_1_1SettingsControllerType.html#a1d1cfd8ffb84e947f82999c682b666a7", [
                    [ "AbortTasksRequiringPassword", "classPackuru_1_1Core_1_1Queue_1_1SettingsControllerType.html#a1d1cfd8ffb84e947f82999c682b666a7ab58f0d8290bec3b07316713d44c1477f", null ],
                    [ "AutoCloseOnAllTasksCompletion", "classPackuru_1_1Core_1_1Queue_1_1SettingsControllerType.html#a1d1cfd8ffb84e947f82999c682b666a7a8c25d0f5d9093f5325ddd52aea073e84", null ],
                    [ "AutoRemoveCompletedTasks", "classPackuru_1_1Core_1_1Queue_1_1SettingsControllerType.html#a1d1cfd8ffb84e947f82999c682b666a7a85f73d8d8a6f269f8e7ebfb4d51af6ff", null ],
                    [ "MaxErrorCount", "classPackuru_1_1Core_1_1Queue_1_1SettingsControllerType.html#a1d1cfd8ffb84e947f82999c682b666a7a36111345248439ea9576d1b46c8a84b9", null ]
                  ] ]
                ] ]
              ] ],
              [ "settingsdialogcore.h", "core-queue_2settingsdialogcore_8h.html", [
                [ "SettingsDialogCore", "classPackuru_1_1Core_1_1Queue_1_1SettingsDialogCore.html", [
                  [ "UserString", "classPackuru_1_1Core_1_1Queue_1_1SettingsDialogCore.html#aae24373b6c0bbb21fcd8a1cf051e29af", [
                    [ "___INVALID", "classPackuru_1_1Core_1_1Queue_1_1SettingsDialogCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa6e545caba6d4293f6c360bc2e183bd64", null ],
                    [ "PageTitle", "classPackuru_1_1Core_1_1Queue_1_1SettingsDialogCore.html#aae24373b6c0bbb21fcd8a1cf051e29afa57a08c88b9aff49612ca6b19fd8bea90", null ]
                  ] ],
                  [ "SettingsDialogCore", "classPackuru_1_1Core_1_1Queue_1_1SettingsDialogCore.html#ac5701d5ae1efc89d382b324e4ea2d3b8", null ],
                  [ "~SettingsDialogCore", "classPackuru_1_1Core_1_1Queue_1_1SettingsDialogCore.html#a0f841e3d6a6cdaf9837a251777d66af6", null ],
                  [ "accept", "classPackuru_1_1Core_1_1Queue_1_1SettingsDialogCore.html#a74ddbe2ce21e10cdd5e4586e2e9f0607", null ],
                  [ "destroyLater", "classPackuru_1_1Core_1_1Queue_1_1SettingsDialogCore.html#aa5f97b4f6309ac39ce270d0ac0f92d50", null ],
                  [ "getControllerEngine", "classPackuru_1_1Core_1_1Queue_1_1SettingsDialogCore.html#a72d2d5dda34983ff5afcb6039a09bdbb", null ]
                ] ]
              ] ],
              [ "taskqueuemodel.h", "taskqueuemodel_8h.html", [
                [ "TaskQueueModel", "classPackuru_1_1Core_1_1Queue_1_1TaskQueueModel.html", [
                  [ "UserString", "classPackuru_1_1Core_1_1Queue_1_1TaskQueueModel.html#aae24373b6c0bbb21fcd8a1cf051e29af", [
                    [ "___INVALID", "classPackuru_1_1Core_1_1Queue_1_1TaskQueueModel.html#aae24373b6c0bbb21fcd8a1cf051e29afa6e545caba6d4293f6c360bc2e183bd64", null ],
                    [ "ActionErrorLog", "classPackuru_1_1Core_1_1Queue_1_1TaskQueueModel.html#aae24373b6c0bbb21fcd8a1cf051e29afac0ea3d943f017ec563e153d87959ba59", null ],
                    [ "ActionRemoveCompleted", "classPackuru_1_1Core_1_1Queue_1_1TaskQueueModel.html#aae24373b6c0bbb21fcd8a1cf051e29afa604693d2bfbde294601d627ecce28186", null ],
                    [ "ActionRemoveTask", "classPackuru_1_1Core_1_1Queue_1_1TaskQueueModel.html#aae24373b6c0bbb21fcd8a1cf051e29afade6f403f3df6e66b540c8dd2983ceb64", null ],
                    [ "ActionResetTask", "classPackuru_1_1Core_1_1Queue_1_1TaskQueueModel.html#aae24373b6c0bbb21fcd8a1cf051e29afac1440498d690395b7ea2e4fbf6a9bd43", null ],
                    [ "ActionStartQueue", "classPackuru_1_1Core_1_1Queue_1_1TaskQueueModel.html#aae24373b6c0bbb21fcd8a1cf051e29afaf6999cfbb106d13e44f98bbb851b7fb9", null ],
                    [ "ActionStopHere", "classPackuru_1_1Core_1_1Queue_1_1TaskQueueModel.html#aae24373b6c0bbb21fcd8a1cf051e29afa2ce8f75eddbe22fd608894e631aa3ae6", null ],
                    [ "ActionStopQueue", "classPackuru_1_1Core_1_1Queue_1_1TaskQueueModel.html#aae24373b6c0bbb21fcd8a1cf051e29afa01825dd0ef7a527e038d89819f78942e", null ]
                  ] ],
                  [ "TaskQueueModel", "classPackuru_1_1Core_1_1Queue_1_1TaskQueueModel.html#a354c2271332e2e92dd77aba7a465df40", null ],
                  [ "~TaskQueueModel", "classPackuru_1_1Core_1_1Queue_1_1TaskQueueModel.html#a4502f11885f3d4b4a9dad35bc982f143", null ],
                  [ "allTasksCompleted", "classPackuru_1_1Core_1_1Queue_1_1TaskQueueModel.html#a1bdf4ce4342340d77d93d3db15c97408", null ],
                  [ "allTasksFinished", "classPackuru_1_1Core_1_1Queue_1_1TaskQueueModel.html#a64dc75a3b1ebd406468eafc21d701b2e", null ],
                  [ "archiveCreationNameError", "classPackuru_1_1Core_1_1Queue_1_1TaskQueueModel.html#a12e37d843735d202965d5426b6d31f71", null ],
                  [ "areAllTasksCompleted", "classPackuru_1_1Core_1_1Queue_1_1TaskQueueModel.html#a1a0cc0f1db5491875233788f08ea3e98", null ],
                  [ "columnCount", "classPackuru_1_1Core_1_1Queue_1_1TaskQueueModel.html#a772927c4f845c5d0efe36b0c51245ba0", null ],
                  [ "completedTaskCountChanged", "classPackuru_1_1Core_1_1Queue_1_1TaskQueueModel.html#a7e70fdaa174e829df0b2bfd051ced57c", null ],
                  [ "data", "classPackuru_1_1Core_1_1Queue_1_1TaskQueueModel.html#ae93b915598cdbe5ae59dddf41d917a53", null ],
                  [ "fileExists", "classPackuru_1_1Core_1_1Queue_1_1TaskQueueModel.html#a453edcd20ca0bf273682814dbaca10d1", null ],
                  [ "folderExists", "classPackuru_1_1Core_1_1Queue_1_1TaskQueueModel.html#ae45f8c8676c16fa2ebc15c5962c41d20", null ],
                  [ "getCompletedTaskCount", "classPackuru_1_1Core_1_1Queue_1_1TaskQueueModel.html#af4aa80740f7ad19613bf928323e8e95e", null ],
                  [ "getErrorLog", "classPackuru_1_1Core_1_1Queue_1_1TaskQueueModel.html#af6cd4da32460a6716bd7e2a374ce4080", null ],
                  [ "getQueueDescription", "classPackuru_1_1Core_1_1Queue_1_1TaskQueueModel.html#aed2bb6103938865d3ccdf9fa5563a021", null ],
                  [ "hasErrorLog", "classPackuru_1_1Core_1_1Queue_1_1TaskQueueModel.html#ada3aeaeecc6242fea5f606ba4f526541", null ],
                  [ "headerData", "classPackuru_1_1Core_1_1Queue_1_1TaskQueueModel.html#a0b82c5f78bc6bc150160c0b3f73e22ba", null ],
                  [ "isRunning", "classPackuru_1_1Core_1_1Queue_1_1TaskQueueModel.html#ad619b8b51adc7874204abd71080cd90e", null ],
                  [ "itemTypeMismatch", "classPackuru_1_1Core_1_1Queue_1_1TaskQueueModel.html#a09f61504055a608f95d3eba98725adfb", null ],
                  [ "openDestinationFolder", "classPackuru_1_1Core_1_1Queue_1_1TaskQueueModel.html#a21fc7c78ec174d02892c06b8c6b0450f", null ],
                  [ "openNewArchive", "classPackuru_1_1Core_1_1Queue_1_1TaskQueueModel.html#a08d3aae1a4ddf606dfcc8943c0a73aef", null ],
                  [ "passwordNeeded", "classPackuru_1_1Core_1_1Queue_1_1TaskQueueModel.html#ae0ffe92903ecc8a43f46f2e371e29930", null ],
                  [ "progressColumn", "classPackuru_1_1Core_1_1Queue_1_1TaskQueueModel.html#ad4e3df7b726eb543e20641a1ffa2c807", null ],
                  [ "queueDescriptionChanged", "classPackuru_1_1Core_1_1Queue_1_1TaskQueueModel.html#af7b06e142539f144b8ba3ba1e189d7bb", null ],
                  [ "removeCompletedTasks", "classPackuru_1_1Core_1_1Queue_1_1TaskQueueModel.html#a68a22aa4b60e3c8af137eb2fb174cba0", null ],
                  [ "removeRows", "classPackuru_1_1Core_1_1Queue_1_1TaskQueueModel.html#a52044751b750cfaf51b877a7970911c9", null ],
                  [ "resetTasks", "classPackuru_1_1Core_1_1Queue_1_1TaskQueueModel.html#a0ceed59a3c749f1a2e8e2b6ec4c91281", null ],
                  [ "roleNames", "classPackuru_1_1Core_1_1Queue_1_1TaskQueueModel.html#ad4bfae6b95ad33dca637414040872cc2", null ],
                  [ "rowCount", "classPackuru_1_1Core_1_1Queue_1_1TaskQueueModel.html#a63ff48c076d848a157ed82c970805ef7", null ],
                  [ "runningChanged", "classPackuru_1_1Core_1_1Queue_1_1TaskQueueModel.html#a9fce3135084d066c916bba956287061b", null ],
                  [ "startQueue", "classPackuru_1_1Core_1_1Queue_1_1TaskQueueModel.html#a74cae7abc2b49d92f6d2f82ffc8b3217", null ],
                  [ "stopAfterTask", "classPackuru_1_1Core_1_1Queue_1_1TaskQueueModel.html#a910eb219460c811016bfacfd693d3d14", null ],
                  [ "stopQueue", "classPackuru_1_1Core_1_1Queue_1_1TaskQueueModel.html#a9778c72d81d7f5aa8456e9e4379f406a", null ],
                  [ "taskStateChanged", "classPackuru_1_1Core_1_1Queue_1_1TaskQueueModel.html#a163bae033ae25d9104116cf8e1190557", null ],
                  [ "unfinishedTaskCount", "classPackuru_1_1Core_1_1Queue_1_1TaskQueueModel.html#a68f7c4af5c3a3853221e4f7a07695e08", null ],
                  [ "completedTaskCount", "classPackuru_1_1Core_1_1Queue_1_1TaskQueueModel.html#ae7327933071159f823999a049d9f6e04", null ],
                  [ "description", "classPackuru_1_1Core_1_1Queue_1_1TaskQueueModel.html#a867c710287b9ea1a0ab27c30886fb5c3", null ],
                  [ "running", "classPackuru_1_1Core_1_1Queue_1_1TaskQueueModel.html#a36f7b6be7108281af77939ceaec42fd6", null ]
                ] ]
              ] ]
            ] ],
            [ "qcs-qqcontrols", "dir_2cd901d9ab2a03719514050daa5a6806.html", [
              [ "private", "dir_5a3d22656a675c6425b415f11624a8c4.html", null ],
              [ "registertypes.h", "registertypes_8h.html", [
                [ "registerTypes", "registertypes_8h.html#a6f5a1b91062ebca7f1fa24c61c7a1a12", null ]
              ] ]
            ] ],
            [ "qcs-qtwidgets", "dir_b99e31e6e62cfd7a499b38d228c62406.html", [
              [ "generatedpage.h", "generatedpage_8h.html", [
                [ "GeneratedPage", "classqcs_1_1qtw_1_1GeneratedPage.html", [
                  [ "GeneratedPage", "classqcs_1_1qtw_1_1GeneratedPage.html#a1d6df641d2efce68c402b96e5a8c0bec", null ],
                  [ "~GeneratedPage", "classqcs_1_1qtw_1_1GeneratedPage.html#a82197093b819166f96f186590bbf6bd4", null ]
                ] ]
              ] ],
              [ "pagestack.h", "pagestack_8h.html", [
                [ "PageStack", "classqcs_1_1qtw_1_1PageStack.html", [
                  [ "PageStack", "classqcs_1_1qtw_1_1PageStack.html#a478c559255779f27eac7bd342fb41887", null ],
                  [ "~PageStack", "classqcs_1_1qtw_1_1PageStack.html#a65a4fd1db8d23ac2c6e7aa4c04c1696c", null ],
                  [ "setCurrentPage", "classqcs_1_1qtw_1_1PageStack.html#a6900138f0202107611abee493a632963", null ]
                ] ]
              ] ],
              [ "widgetmapper.h", "qcs-qtwidgets_2widgetmapper_8h.html", [
                [ "WidgetMapper", "classqcs_1_1qtw_1_1WidgetMapper.html", [
                  [ "WidgetMapper", "classqcs_1_1qtw_1_1WidgetMapper.html#a9b0177f079b738b0f2aaba253eddd23d", null ],
                  [ "~WidgetMapper", "classqcs_1_1qtw_1_1WidgetMapper.html#ad8a0c124635806902e445d7b07cddded", null ],
                  [ "addBuddies", "classqcs_1_1qtw_1_1WidgetMapper.html#aff91a259bfd73482d7a9da8282f95869", null ],
                  [ "addLabelAndWidget", "classqcs_1_1qtw_1_1WidgetMapper.html#a8297e9e73b01ed427f3650efc94dd12a", null ],
                  [ "addLabelsAndWidgets", "classqcs_1_1qtw_1_1WidgetMapper.html#a0acc67ce76e581a795cd079418cf6f14", null ],
                  [ "addWidgets", "classqcs_1_1qtw_1_1WidgetMapper.html#ab449b05688414cbf1b7a56f8fd51ad20", null ],
                  [ "isSetEnabled", "classqcs_1_1qtw_1_1WidgetMapper.html#a7819c308e789d5e548ee131cd27009c9", null ],
                  [ "isSetVisible", "classqcs_1_1qtw_1_1WidgetMapper.html#a4a1731cd0076b2f046ad8ab6c13a22e8", null ],
                  [ "setEngine", "classqcs_1_1qtw_1_1WidgetMapper.html#a883d64c2e020cc95988fc08baa2821c5", null ],
                  [ "updateWidgets", "classqcs_1_1qtw_1_1WidgetMapper.html#a6ad1c3af6faad7b7844789affa08e748", null ]
                ] ]
              ] ]
            ] ],
            [ "utils", "dir_313caf1132e152dd9b58bea13a4052ca.html", [
              [ "boolflag.h", "boolflag_8h.html", [
                [ "BoolFlag", "classPackuru_1_1Utils_1_1BoolFlag.html", [
                  [ "BoolFlag", "classPackuru_1_1Utils_1_1BoolFlag.html#a8636cfb6b2bb04b050d5f03874336948", null ],
                  [ "BoolFlag", "classPackuru_1_1Utils_1_1BoolFlag.html#a2973b4530ba874790f8eaaa537eaf430", null ],
                  [ "BoolFlag", "classPackuru_1_1Utils_1_1BoolFlag.html#a2408a45308121507fb422fb1a774ba79", null ],
                  [ "BoolFlag", "classPackuru_1_1Utils_1_1BoolFlag.html#ab0086fb9f00fafc86696a5506f4f51d1", null ],
                  [ "BoolFlag", "classPackuru_1_1Utils_1_1BoolFlag.html#a09632ada73e1852cc6dfc8af857ed8e8", null ],
                  [ "operator bool", "classPackuru_1_1Utils_1_1BoolFlag.html#a420e60e9788bfa3b52aa3aab72ea3a5e", null ],
                  [ "operator=", "classPackuru_1_1Utils_1_1BoolFlag.html#a51dca6540360f0bc0992336149462508", null ],
                  [ "operator=", "classPackuru_1_1Utils_1_1BoolFlag.html#a5ec88f9619a1e16f26fcf194583fb00d", null ],
                  [ "operator=", "classPackuru_1_1Utils_1_1BoolFlag.html#a04a6621a4b62d0c632fc85b2a4af77d9", null ],
                  [ "operator=", "classPackuru_1_1Utils_1_1BoolFlag.html#a4e733c323d08c6dfdccf7a195a37dc3b", null ],
                  [ "operator=", "classPackuru_1_1Utils_1_1BoolFlag.html#af212a2f7a1fbce7c06d3ed5899a8584c", null ],
                  [ "operator=", "classPackuru_1_1Utils_1_1BoolFlag.html#aa5de1b691733bc88c0e087ad146af605", null ],
                  [ "reset", "classPackuru_1_1Utils_1_1BoolFlag.html#a20194325f0380eb3bbc6bc6c8b2697d9", null ],
                  [ "set", "classPackuru_1_1Utils_1_1BoolFlag.html#a45b441bdebcee6e121e3b3198335b93a", null ],
                  [ "value", "classPackuru_1_1Utils_1_1BoolFlag.html#ad7968d74283844a09d00ba0ad66e62e7", null ]
                ] ]
              ] ],
              [ "localholder.h", "localholder_8h.html", [
                [ "LocalHolder", "classPackuru_1_1Utils_1_1LocalHolder.html", [
                  [ "LocalHolder", "classPackuru_1_1Utils_1_1LocalHolder.html#af4daebaf278a10aced73d8ea4bfba2f9", null ],
                  [ "LocalHolder", "classPackuru_1_1Utils_1_1LocalHolder.html#a3197a2b9a7deecf628d2d76c602d3e47", null ],
                  [ "~LocalHolder", "classPackuru_1_1Utils_1_1LocalHolder.html#a60aa04cc97acea53c7b4ba0e4aa7befb", null ],
                  [ "operator=", "classPackuru_1_1Utils_1_1LocalHolder.html#a44907613a454f2ad55f19012980451e1", null ]
                ] ]
              ] ],
              [ "makeqpointer.h", "makeqpointer_8h.html", [
                [ "makeQPointer", "makeqpointer_8h.html#a6febd7d75708805fe4e91a8c37d01be8", null ]
              ] ],
              [ "qvariant_utils.h", "qvariant__utils_8h.html", [
                [ "containsType", "qvariant__utils_8h.html#a548c18bed558077ed402fae23ee4d3f5", null ],
                [ "getValueAs", "qvariant__utils_8h.html#af4a238f9152e242b859d24998dcc67b5", null ]
              ] ],
              [ "scopeguard.h", "scopeguard_8h.html", [
                [ "ScopeGuard", "classPackuru_1_1Utils_1_1ScopeGuard.html", [
                  [ "ScopeGuard", "classPackuru_1_1Utils_1_1ScopeGuard.html#ad79d98631880e951f4dee8492db6916a", null ],
                  [ "~ScopeGuard", "classPackuru_1_1Utils_1_1ScopeGuard.html#a9b2dd2382e7d9acf1de52a0b8b3cc31f", null ],
                  [ "createScopeGuard", "classPackuru_1_1Utils_1_1ScopeGuard.html#a8cdf138e22cb0767044c09275e343ac7", null ]
                ] ],
                [ "createScopeGuard", "scopeguard_8h.html#a8cdf138e22cb0767044c09275e343ac7", null ]
              ] ]
            ] ]
          ] ]
        ] ]
      ] ]
    ] ],
    [ "File Source", "filesource.html", [
      [ "packuru", "dir_d41d8cd98f00b204e9800998ecf8427e.html", [
        [ "src", "dir_68267d1309a1af8e8297ef4c3efbcdba.html", [
          [ "core", "dir_aebb8dcc11953d78e620bbef0b9e2183.html", [
            [ "aboutdialogcore.h", "aboutdialogcore_8h_source.html", null ],
            [ "appclosingdialogcore.h", "appclosingdialogcore_8h_source.html", null ],
            [ "appinfo.h", "appinfo_8h_source.html", null ],
            [ "appsettingscontrollertype.h", "appsettingscontrollertype_8h_source.html", null ],
            [ "appsettingsdialogcore.h", "appsettingsdialogcore_8h_source.html", null ],
            [ "archivetypefiltermodel.h", "archivetypefiltermodel_8h_source.html", null ],
            [ "archivetypemodel.h", "archivetypemodel_8h_source.html", null ],
            [ "archivingdialogcontrollertype.h", "archivingdialogcontrollertype_8h_source.html", null ],
            [ "archivingdialogcore.h", "archivingdialogcore_8h_source.html", null ],
            [ "commonstrings.h", "commonstrings_8h_source.html", null ],
            [ "creationnameerrorpromptcore.h", "creationnameerrorpromptcore_8h_source.html", null ],
            [ "extractiondialogcontrollertype.h", "extractiondialogcontrollertype_8h_source.html", null ],
            [ "extractiondialogcore.h", "extractiondialogcore_8h_source.html", null ],
            [ "extractionfileoverwritepromptcore.h", "extractionfileoverwritepromptcore_8h_source.html", null ],
            [ "extractionfolderoverwritepromptcore.h", "extractionfolderoverwritepromptcore_8h_source.html", null ],
            [ "extractiontypemismatchpromptcore.h", "extractiontypemismatchpromptcore_8h_source.html", null ],
            [ "globalsettingsconverter.h", "globalsettingsconverter_8h_source.html", null ],
            [ "globalsettingsmanager.h", "globalsettingsmanager_8h_source.html", null ],
            [ "globalsettingsuservalue.h", "globalsettingsuservalue_8h_source.html", null ],
            [ "htmllink.h", "htmllink_8h_source.html", null ],
            [ "passwordpromptcore.h", "passwordpromptcore_8h_source.html", null ],
            [ "plugindatamodel.h", "plugindatamodel_8h_source.html", null ],
            [ "symbol_export.h", "core_2symbol__export_8h_source.html", null ],
            [ "testdialogcontrollertype.h", "testdialogcontrollertype_8h_source.html", null ],
            [ "testdialogcore.h", "testdialogcore_8h_source.html", null ]
          ] ],
          [ "core-browser", "dir_c1f580bc93b1491ad07caabbd4ed4f43.html", [
            [ "archivebrowsercore.h", "archivebrowsercore_8h_source.html", null ],
            [ "archivenavigator.h", "archivenavigator_8h_source.html", null ],
            [ "basiccontrol.h", "basiccontrol_8h_source.html", null ],
            [ "basiccontrolcontrollertype.h", "basiccontrolcontrollertype_8h_source.html", null ],
            [ "deletiondialogcontrollertype.h", "deletiondialogcontrollertype_8h_source.html", null ],
            [ "deletiondialogcore.h", "deletiondialogcore_8h_source.html", null ],
            [ "init.h", "core-browser_2init_8h_source.html", null ],
            [ "mainwindowcore.h", "core-browser_2mainwindowcore_8h_source.html", null ],
            [ "queuemessenger.h", "queuemessenger_8h_source.html", null ],
            [ "settingscontrollertype.h", "core-browser_2settingscontrollertype_8h_source.html", null ],
            [ "settingsdialogcore.h", "core-browser_2settingsdialogcore_8h_source.html", null ],
            [ "statuspagecore.h", "statuspagecore_8h_source.html", null ],
            [ "symbol_export.h", "core-browser_2symbol__export_8h_source.html", null ],
            [ "tabclosingdialogcore.h", "tabclosingdialogcore_8h_source.html", null ],
            [ "viewfiltercontrollertype.h", "viewfiltercontrollertype_8h_source.html", null ]
          ] ],
          [ "core-queue", "dir_c113aa9f15df3532312d5a8a5b3c444c.html", [
            [ "init.h", "core-queue_2init_8h_source.html", null ],
            [ "mainwindowcore.h", "core-queue_2mainwindowcore_8h_source.html", null ],
            [ "queuecore.h", "queuecore_8h_source.html", null ],
            [ "settingscontrollertype.h", "core-queue_2settingscontrollertype_8h_source.html", null ],
            [ "settingsdialogcore.h", "core-queue_2settingsdialogcore_8h_source.html", null ],
            [ "symbol_export.h", "core-queue_2symbol__export_8h_source.html", null ],
            [ "taskqueuemodel.h", "taskqueuemodel_8h_source.html", null ]
          ] ],
          [ "qcs-qqcontrols", "dir_2cd901d9ab2a03719514050daa5a6806.html", [
            [ "private", "dir_5a3d22656a675c6425b415f11624a8c4.html", [
              [ "widgetmapper.h", "qcs-qqcontrols_2private_2widgetmapper_8h_source.html", null ]
            ] ],
            [ "registertypes.h", "registertypes_8h_source.html", null ],
            [ "symbol_export.h", "qcs-qqcontrols_2symbol__export_8h_source.html", null ]
          ] ],
          [ "qcs-qtwidgets", "dir_b99e31e6e62cfd7a499b38d228c62406.html", [
            [ "generatedpage.h", "generatedpage_8h_source.html", null ],
            [ "pagestack.h", "pagestack_8h_source.html", null ],
            [ "symbol_export.h", "qcs-qtwidgets_2symbol__export_8h_source.html", null ],
            [ "widgetmapper.h", "qcs-qtwidgets_2widgetmapper_8h_source.html", null ]
          ] ],
          [ "utils", "dir_313caf1132e152dd9b58bea13a4052ca.html", [
            [ "boolflag.h", "boolflag_8h_source.html", null ],
            [ "localholder.h", "localholder_8h_source.html", null ],
            [ "makeqpointer.h", "makeqpointer_8h_source.html", null ],
            [ "qvariant_utils.h", "qvariant__utils_8h_source.html", null ],
            [ "scopeguard.h", "scopeguard_8h_source.html", null ],
            [ "typetraits.h", "typetraits_8h_source.html", null ]
          ] ]
        ] ]
      ] ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
".html",
"classPackuru_1_1Core_1_1Browser_1_1ArchiveNavigator.html#a2bd4c31e8c3edcef188925e74455bcf0a2d88e0eae49b226677a77f4844169414",
"classPackuru_1_1Core_1_1ExtractionFileOverwritePromptCore.html#a8483b0cda34e6bde49ab3cf55d6d3d29",
"classPackuru_1_1Utils_1_1BoolFlag.html#a45b441bdebcee6e121e3b3198335b93a"
];

var SYNCONMSG = 'click to disable panel synchronization';
var SYNCOFFMSG = 'click to enable panel synchronization';